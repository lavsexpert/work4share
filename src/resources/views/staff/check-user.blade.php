@extends('layouts.app')

@section('content')


<div class="container-fluid">
    @include('staff.parts.navbar',['route' => $route])
    {{--<form action="/staff/user-refusal/138" method="post">
        @csrf
        <input type="hidden" name="user_refusal_id" value="1">
        <input type="submit">
    </form>--}}
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Подтвердил E-mail</th>
                        <th scope="col">Номер телефона</th>
                        <th scope="col">Паспорт</th>
                        <th scope="col">Паспорт</th>
                        <th scope="col">Водительские права</th>
                        <th scope="col">Водительские права</th>
                        <th scope="col">Селфи</th>
                        <th scope="col">Возраст</th>
                        <th scope="col">Стаж вождения</th>
                        <th scope="col">День рождения</th>
                        <th scope="col">Статус</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $item)
                    <tr id="user_{{ $item->id }}">
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->NAME }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->email_verified_at }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>@if (!empty($item->passport_face)) <a href="{{$item->passport_face}}">Открыть</a> @else @endif</td>
                        <td>@if (!empty($item->passport_visa)) <a href="{{$item->passport_visa}}">Открыть</a> @else @endif</td>
                        <td>@if (!empty($item->driver_lisense_first)) <a href="{{$item->driver_lisense_first}}">Открыть</a> @else @endif</td>
                        <td>@if (!empty($item->driver_license_second)) <a href="{{$item->driver_license_second}}">Открыть</a> @else @endif</td>
                        <td>@if (!empty($item->selfie)) <a href="{{$item->selfie}}">Открыть</a> @else @endif</td>
                        <td>{{ \App\User::find($item->id)->age ?? 'Не указано'}}</td>
                        <td><input type="number" min="0" id="driving-experience-{{ $item->id }}" value="{{ $item->driving_experience }}"></td>
                        <td><input type="date" id="birthday-{{ $item->id }}" value="{{ $item->birthday }}"></td>
                        <td>
                                <select
                                        type="checkbox"
                                        class="form-cars__select"
                                        style="font-size: 14px;width: 195px !important;"
                                        name="refusal"
                                        id="refusal_select">
                                    <option value="">Ничего не выбрано</option>
                                    @isset ($users_refusal)
                                        @foreach ($users_refusal as $user_refusal)
                                            <option class="form-cars__option" value="{!! $user_refusal->id !!}"
                                            @isset($item->validate_refusal_id)
                                                @if($item->validate_refusal_id == $user_refusal->id) selected @endif
                                            @endisset
                                            >
                                                {!! $user_refusal->name !!}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                        </td>
                        <td>
                            <button class="btn" onclick="saveRefusal({!! $item->id !!})">Сохранить</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>

const loading = $('.loading');

function saveRefusal(user_id) {
    var user_refusal_id = $('#user_' + user_id).find('#refusal_select').val();
    var birthday = $('#birthday-' + user_id).val();
    var experience = $('#driving-experience-' + user_id).val();
    if ((user_refusal_id !== undefined && user_refusal_id != '') || experience || birthday) {
        loading.addClass('active');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '/staff/user-refusal/' + user_id,
            data: {
                'user_refusal_id': user_refusal_id,
                'birthday' : birthday,
                'experience' : experience

            }
        }).done(function(res) {
            console.log(res);
            
            loading.removeClass('active')
        }).fail(function(err) {   
            console.log(err);
            
            loading.removeClass('active')
            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        });
    }
}

</script>

@endsection