@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/staff.js') }}" defer></script>
@endpush
@section('content')



<div class="container-fluid staff-auto">
    @include('staff.parts.navbar',['route' => $route])
    @include('staff.parts.filter-auto',[ 'brandsCars' => $brandsCars,'models' => $models , 'selected_brand' => $brandId, 'selected_model' => $modelId,  'fromDate' => $fromDate, 'toDate' => $toDate ])
    <div class="row">
        <div class="col-sm-12">
            <div class="table table-striped">
                <div class="tr">
                    <div class="table_tr" scope="col">#</div>
                    <div class="table_tr" scope="col">Имя владельца</div>
                    <div class="table_tr" scope="col">Паспорт</div>
                    <div class="table_tr" scope="col">Паспорт</div>
                    <div class="table_tr" scope="col">Автомобиль</div>
                    <div class="table_tr" scope="col">VIN</div>
                    <div class="table_tr" scope="col">Заполнить</div>
                    <div class="table_tr" scope="col">Сохранить</div>
                </div>
                @foreach ($cars as $item)
                <form class="tr" method="POST" action="/staff/car-refusal/{{$item->id}}">
                    @csrf
                    <div class="th" scope="row">{{ $item->id }}</div>
                    <div class="td">{{ $item->userName }}</div>
                    <div class="td">@if (!empty($item->passport_face)) <a href="{{$item->passport_face}}">Открыть</a> @else @endif</div>
                    <div class="td">@if (!empty($item->passport_visa)) <a href="{{$item->passport_visa}}">Открыть</a> @else @endif</div>
                    <div class="td">{{ $item->brand }} {{ $item->model }}</div>
                    <div class="td">{{ $item->vin }}</div>
                    <div class="td">
                        <div class="form-cars__wrap">
                            <select
                            type="checkbox"
                            class="form-cars__select"
                            style="font-size: 14px"
                            name="car_refusal_id"
                            id="refusal_select">
                                <option value="">Ничего не выбрано</option>
                                @isset ($cars_refusal)
                                    @foreach ($cars_refusal as $car_refusal)
                                        <option class="form-cars__option" value="{!! $car_refusal->id !!}"
                                        @isset($item->validate_refusal_id)
                                            @if($item->validate_refusal_id == $car_refusal->id) selected @endif
                                        @endisset
                                        >
                                            {!! $car_refusal->name !!}
                                        </option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                    </div>
                    <div class="td">
                    
                    
                        
                        <button class="btn">Сохранить</button>
                    
                    </div>
                </form>
                @endforeach

            </div>
        </div>
    </div>
</div>


@endsection