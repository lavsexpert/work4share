@extends('layouts.app') 
@section('content')

<div class="container">
    @include('staff.parts.navbar',['route' => $route])
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Verified</th>
                        <th scope="col">Validate</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->NAME }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->email_verified_at }}</td>
                        <td>{{ $item->validate }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection