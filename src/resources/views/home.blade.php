@extends('layouts.home')

@section('content')
<section style="background-image: url('/image/intro/intro-bg-2.jpg')" class="intro">
	<div class="container">
		<div class="row">
			<h2>Найди особенный авто для путешествия</h2>
			@desktop
			<form autocomplete="off" method="POST" class="form" action="{{ route('car.catalog.filter') }}">
				@csrf
				<div class="form__search form__item"><i class="icon icon-mark"></i>
					<input class="search" readonly="true" autocomplete="nope" placeholder="г. Москва" type="text" name="location_delivery" value="г. Москва"  required>
					<input type="hidden" id="coordinates" name="coordinates">
					<div class="form__search__block">
						<ul id="search_ul">
							@foreach ($city as $item)
							<li><a>{{$item->name}}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="form__date form__item">
					<i class="icon icon-calendar"></i>
					<label>
						<div class="label">Введите период поездки</div>
						<p class="from"></p><span></span>
						<p class="to"></p>
						<input id="from" type="text" name="from">
						<input id="to" type="text" name="to">
						<input readonly="true" autocomplete="nope" id="date" placeholder="Введите период поездки" type="text" data-timepicker="true" data-range="true" data-multiple-dates-separator=" - " required class="datepicker-here">
					</label>
				</div>
				<button type="submit" class="btn btn-search"><i class="icon icon-search"></i>найти авто</button>
			</form>
			@enddesktop
		</div>
	</div>
</section>
@mobile
<section class="pt-4 pb-4">
	<div class="container">
			<div class="row">	
				<div class="col-12">
					<form autocomplete="off" method="POST" class="form" action="{{ route('car.catalog.filter') }}">
						@csrf
						<div class="form__search form__item"><i class="icon icon-mark"></i>
							<input class="search" readonly="true" autocomplete="nope" placeholder="Введите город, аэропорт или адрес" type="text" name="location_delivery" value="Москва"  required>
							<input type="hidden" id="coordinates" name="coordinates">
							<div class="form__search__block">
								<ul id="search_ul">
									<li>Москва</li>
									<li class="location"><i class="fas fa-location-arrow"></i>Мое местоположение</li>
									{{-- @foreach ($rentPlace as $item)
									<li><a>{{$item->name}}</a></li>
									@endforeach --}}
								</ul>
							</div>
						</div>
						<div class="form__date form__item">
							<i class="icon icon-calendar"></i>
							<label>
								<div class="label">Введите период поездки</div>
								<p class="from"></p><span></span>
								<p class="to"></p>
								<input id="from" type="text" name="from">
								<input id="to" type="text" name="to">
								<input readonly="true" autocomplete="nope" id="date" placeholder="Введите период поездки" type="text" data-timepicker="true" data-range="true" data-multiple-dates-separator=" - " required class="datepicker-here">
							</label>
						</div>
						<button type="submit" class="btn btn-search"><i class="icon icon-search"></i>найти авто</button>
					</form>
				</div>
			</div>
	</div>
</section>
@endmobile
<section class="catalog">
	<div class="container">
		<div class="row">
			<div class="col position-relative">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						@foreach ($cars as $car)
						<div class="swiper-slide">
							<a href="/carcard/{{$car->id}}">
								<div class="catalog-card">
									@php
										$src = null; 
										$class = null; 
									@endphp
									@isset($images)
										@foreach ($images as $image)
												@if ($image->car_id == $car->id && $image->avatar)
													@php
														$src = "https://hb.bizmrg.com/soautomedia/$image->name"
													@endphp
													@break
												@endif
										@endforeach
									@endisset

									@if ($src == null)
									@php
											$src = "/image/no_image.jpg";
											$class = 'no-image';
									@endphp
									@endif      
									<img class="{{$class}}" src="{{$src}}" alt="">
									<div class="catalog-card__block">
										<h2>{{$car->brand['name']}} {{$car->models_name}}
											<span>{{$car->cartype['name']}}</span>
										</h2>
										<h3>{{$car->cost_day}}₽
											<span>сутки</span>
										</h3>
									</div>
								</div>
							</a>
						</div>
						@endforeach
					</div>
					<div class="swiper-pagination"></div>
				</div>
				<div class="swiper-button-prev d-none d-sm-block"><i class="icon icon-arrow"></i></div>
				<div class="swiper-button-next d-none d-sm-block"><i class="icon icon-arrow"></i></div>
				<div class="d-flex justify-content-center">
					<a href="{{ route('car.catalog.index') }}" class="btn btn-catalog"> <img src="/image/car.svg" alt="">подобрать идеальный авто</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="info">
	<div class="container">
		<div class="info__block">
			<p>Выходя из дома открыть приложение и выбрать тот автомобиль, на котором сегодня поедешь: по делам на черном Mercedes E350, на свидание с ярко синим кабрио BMW или в веселое путешествие с друзьями на Mini Countryman!</p>
			<p class="yellow">Добро пожаловать в SOAUTO.<br>Мы превращаем автомобильные мечты в реальность!</p>
			@if (Auth::check())
				<a href="{{ route('profile.index') }}" class="btn btn-info">присоединиться</a>
			@else
				<a href="{{ route('register') }}" class="btn btn-info">присоединиться</a>
			@endif
		</div>
	</div>
</section>
<section class="social">
		<div class="container">
				<div class="row">
						<div class="col-lg-4">
							<a href="https://tglink.ru/joinchat/ATHw70_wp_0K9U4A7aJISQ" target="_blank" class="btn btn-primary btn-blue w-100">telegram</a>
						</div>
						<div class="col-lg-4">
							<a href="https://www.instagram.com/so.auto/" target="_blank" class="btn btn-primary btn-coral w-100">instagram</a>
						</div>
						<div class="col-lg-4">
							<a href="https://vk.com/soautoru" target="_blank" class="btn btn-primary btn-dark-blue w-100">вконтакте</a>
						</div>
				</div>
		</div>
	</section>
<section class="create">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Мы создаем сообщество, в котором каждый может арендовать или поделиться машиной, удобно, просто, безопасно.				</h2>
			</div>
		</div>
	</div>
	<div style="background-image: url('/image/create-bg.png')" class="create__bg">
		<div class="container">
			<div class="row align-items-center">
				<div class="offset-md-7 col-md-4 d-flex flex-column align-items-center">
					<h3>Делитесь автомобилем с родственниками, с друзьями, с коллегами или единомышленниками.</h3>
					@if (Auth::check())
						<a href="{{ route('car.create') }}" class="btn btn-create">разместить авто</a>
					@else
						<a href="{{ route('register') }}" class="btn btn-create">разместить авто</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
<section class="how">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="text-center">Не давай машине скучать</h2>
			</div>
			<div class="col-md-10"><iframe width="100%" height="500" src="https://www.youtube.com/embed/0UpgNj1FmZ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></div>
		</div>
	</div>
</section>
{{-- <section class="mobile">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5 col-md-12">
				<h2>Просто и удобно.</h2>
				<p>Скачай приложение. Зарегистрируйся и выбирай авто по своему вкусу и запросу.</p>
				<div class="d-flex">
					<a href="#">
						<img src="/image/app-store.svg" alt="">
					</a>
					<a href="#">
						<img src="/image/google-play.svg" alt="">
					</a>
				</div>
			</div>
			<div class="col-lg-7 col-md-12"><img src="/image/phone.png" alt="" class="d-none d-sm-block"></div>
		</div>
	</div>
</section> --}}

@endsection
