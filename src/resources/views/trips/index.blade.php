@extends('layouts.app') 
@section('content')
<section class="my-car trips">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="my-car-title">
					Мои поездки 
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<ul class="nav" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="tab_personal__link active" id="active-tab" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">Активные</a>
					</li>
					<li class="nav-item">
						<a class="tab_personal__link" id="booking-tab" data-toggle="tab" href="#booking" role="tab" aria-controls="booking" aria-selected="false">Бронирования</a>
					</li>
					<li class="nav-item">
						<a class="tab_personal__link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">История</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="tab-content">
					<div class="tab-pane fade all show active" id="active" role="tabpanel" aria-labelledby="active-tab">
						<div class="row">
							@foreach ($activeCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">Даты</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																	<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																		С: {{ $item->datefrom }} <br>По: {{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex align-items-center">
															<p class="car-wrapper__text_f">Статус</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex align-items-center">
															<p class="car-wrapper__text_f">Сумма аренды</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{ $item->totalCost}} руб</p>
															@endisset
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tab-pane fade show" id="booking" role="tabpanel" aria-labelledby="booking-tab">
						<div class="row">
							@foreach ($bookedCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">Даты</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																	С: {{ $item->datefrom }} <br>По: {{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">Статус</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">Сумма аренды</p>
															<p class="car-wrapper__text_s">{{ $item->totalCost}} руб</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tab-pane fade show" id="history" role="tabpanel" aria-labelledby="history-tab">
						<div class="row">
							@foreach ($historyCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">Даты</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																	С: {{ $item->datefrom }} <br>По: {{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">Статус</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">Сумма аренды</p>
															<p class="car-wrapper__text_s">{{ $item->totalCost }} руб</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@if (session('result') !== null)
	@if (session('result') === '1')
		<script>
			ym(53583991, 'reachGoal', 'success-pay');
		</script>
		<div class="modal fade" id="modal-trip" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ваша оплата прянята</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<p>Вы можете связаться с владельцем через сервис сообщений, чтобы обсудить условия передачи авто</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">Хорошо</button>
				</div>
				</div>
			</div>
		</div>
	@elseif(session('result') == '0')
		<div class="modal fade" id="modal-trip" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ошибка оплаты</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<p>Упс... Что то пошло не так! Но вы можете попробовать ещё раз</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">Хорошо</button>
				</div>
				</div>
			</div>
		</div>
	@endif
@endif

@endsection

