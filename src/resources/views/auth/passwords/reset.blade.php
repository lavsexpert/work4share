@extends('layouts.app')

@section('content')

<section class="login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 d-flex align-items-center flex-column">
				<h1 class="login-title">{{ __('Сброс пароля') }}</h1>

				<form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Сброс пароля') }}" style="width: 100%" class="login-form d-flex align-items-center flex-column">
					@csrf
					<input type="hidden" name="token" value="{{ $token }}">

					<div class="login-form__adress">
						<p class="login-form__text">{{ __('E-Mail адрес') }}</p>
						<input id="#" type="email" class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{ __('E-Mail адрес') }}" required value="{{ $email ?? old('email') }}">

						@if ($errors->has('email'))
							<p class="login-form__text login-form__text_red">{{ $errors->first('email') }}</p>
						@endif

					</div>

					<div class="login-form__adress">
						<p class="login-form__text">{{ __('Новый пароль') }}</p>
						<input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Новый пароль') }}" required >

						@if ($errors->has('password'))
							<p class="login-form__text login-form__text_red">{{ $errors->first('password') }}</p>
						@endif

					</div>

					<div class="login-form__adress">
						<p class="login-form__text">{{ __('Новый пароль') }}</p>
						<input id="#" type="password" class="login-form__input" name="password_confirmation" placeholder="{{ __('Новый пароль') }}" required>
					</div>

					<button type="submit" class="btn login-form__button">{{ __('Сбросить пароль') }}</button>
				</form>
			</div>
		</div>
	</div>
</section>

@endsection
