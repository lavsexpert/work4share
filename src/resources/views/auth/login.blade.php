@extends('layouts.app')

@section('content')
<section class="login">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
					<h1 class="login-title">Вход</h1>
					<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="login-form">
						@csrf
						<div class="login-form__adress">
							<p class="login-form__text">{{ __('Введите адрес эл. почты') }}</p>
							<input id="#" type="email" class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Введите E-mail" required 
							value="{{ old('email') }}">
			
							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
			
						</div>
						<div class="login-form__pass">
							<p class="login-form__text">{{ __('Введите пароль') }}</p>
							<input id="password" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" minlength="6" placeholder="Введите пароль" required>
							<div id="show-pass" onclick="showPass('password')"></div>
							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
			
							<div class="checkbox-login">
								<input id="check" type="checkbox" name="remember" value="check" {{ old('remember') ? 'checked' : '' }} class="check">
								<label for="check">{{ __('запомнить меня') }}</label>
			
								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
			
							</div>
						</div>
						<button type="submit" class="btn login-form__button">{{ __('Войти') }}</button>
						<br>
						<a href="{{ route('password.request') }}"  class="login-form__link ml-4">{{ __('Забыл пароль') }}</a>
					</form>
			</div>
			<div class="col-md-6">
				<h1 class="login-title">Еще нет аккаунта?</h1>
				<a href="/register" class="btn btn--register login-form__button">{{ __('Зарегистрироватся') }}</a>
			</div>
		</div>
	</div>
</section>
@endsection
