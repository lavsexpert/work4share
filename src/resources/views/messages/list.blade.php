@extends('layouts.app')
@section('content')
    <section class="my-car">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="my-car-title">
                        Мои сообщения
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="car-wrapper">
                        <img src="https://hb.bizmrg.com/soautomedia/181QxoNU5OAn0ICFXOePTRZ" alt="">
                        <a href="/carcard/126" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection