<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SoAuto</title>
    <style>
    /*======================global_style_for_all_browsers============================*/
    html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }
    ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }
    table {
        border-collapse: collapse;
        border-spacing: 0;
    }
    {
        outline: none!important;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    input {
        box-shadow: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        -o-box-shadow: none;
        -ms-box-shadow: none;
    }
    button, a {
        cursor: pointer;
        text-decoration: none!important;
        border: 0;
        display: inline-block;
    }
    a:hover {
        text-decoration: none!important;
        box-shadow: none;
    }
    a:hover, a:focus {
        box-shadow: none;
    }
    a {
        color: #007bff;
        transition: .5s;
        -webkit-transition: .5s;
        -moz-transition: .5s;
        -ms-transition: .5s;
        -o-transition: .5s;
    }
    img {
        max-width: 100%;
    }
    body {
        margin: 0;
        padding: 0;
        background: #fff;
        font-family: 'Circe-regular';
    }
    .main-container {
        max-width: 645px;
        padding: 0 15px;
        margin: 0 auto;
    }
    /*===================Custom Style=================*/
    .header-content {
        margin: 0 25px;
    }
    .menu-number {
        font-family: 'Druk-medium';
        font-size: 17px;
        color: #000;
    }
    .menu-number-text {
        font-family: 'Circe-light';
        font-size: 13px;
        color: #000;
    }
    .menu-num-block {
        display: inline-block;
    }
    /*=============Main-block==============*/
    .main-block {
        position: relative;
        background: #fff;
        border-radius: 18px;
        -webkit-border-radius: 18px;
        -moz-border-radius: 18px;
        -ms-border-radius: 18px;
        -o-border-radius: 18px;
        box-shadow: 0px 0px 26px 0px rgba(0, 0, 0, 0.1);
        width: 100%;
        padding: 57px 15px 62px;
    }
    .main-title {
        font-family: 'Druk-medium';
        font-size: 22px;
        color: #000;
        text-align: center;
    }
    .main-subtitle {
        font-family: 'Circe-regular';
        font-size: 20px;
        color: #000;
        text-align: center;
        width: 275px;
        line-height: 1.5;
        margin: 32px auto 0;
    }
    .main-subtitle.second {
        padding-bottom: 36px;
        position: relative;
    }
    .main-subtitle.second::after {
        content: '';
        width: 165px;
        height: 2px;
        background: #F6D200;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: 0 auto;
    }
    .verify-btn {
        font-family: 'Circe-bold';
        font-size: 18px;
        color: #000;
        background: #F6D200;
        width: 225px;
        height: 56px;
        border-radius: 30px;
        -webkit-border-radius: 30px;
        -moz-border-radius: 30px;
        -ms-border-radius: 30px;
        -o-border-radius: 30px;
        margin: 34px auto 0;
        cursor: pointer;
        display: block;
    }
    .main-create-account {
        font-family: 'Circe-light';
        font-size: 18px;
        color: #000;
        max-width: 420px;
        width: 100%;
        text-align: center;
        margin: 38px auto 0;
        line-height: 1.444;
    }
    .order-block {
        margin: 52px auto 0;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    .order-title {
        font-family: 'Circe-regular';
        font-size: 16px;
        color: #808080;
        margin-bottom: 9px;
    }
    .order-btn {
        border: 2px solid #F6D200;
        background: #fff;
        width: 191px;
        height: 38px;
        border-radius: 30px;
        -webkit-border-radius: 30px;
        -moz-border-radius: 30px;
        -ms-border-radius: 30px;
        -o-border-radius: 30px;
        font-size: 18px;
        color: #000;
    }
    .order-link {
        font-family: 'Circe-regular';
        font-size: 12px;
        color: #808080;
        width: 185px;
        text-align: center;
        margin-top: 9px;
    }
    .order-link span {
        text-decoration: underline;
    }
    .bottom-block {
        display: inline-flex;
        margin: 40px auto;
        text-align: center;
    }
    .community-text {
        font-family: 'Circe-light';
        font-size: 14px;
        color: #808080;
        width: 75px;
        line-height: 1.286;
    }
    .cummunity-img {
        width: 73px;
        height: 31px;
        vertical-align: middle;
    }
    /*=============Social===========*/
    .social ul {
        display: inline-flex;
        margin: auto;
    }
    .social ul li {
        margin-right: 10px;
    }
    /*========Footer=========*/
    .footer-text {
        font-family: 'Circe-light';
        font-size: 14px;
        color: #000;
        text-align: center;
        margin-bottom: 10px;
    }
    .footer-link {
        font-family: 'Circe-light';
        font-size: 14px;
        color: #000A8D;
        text-align: center;
        text-decoration: underline !important;
        word-break: break-all;
    }
    .copyright {
        font-family: 'Circe-light';
        font-size: 14px;
        color: #000;
        text-align: center;
        display: block;
        margin-top: 40px;
        padding-bottom: 60px;
    }
    @media (max-width: 1000px) {
        .gift-img, .auto-img {
            background-image: none
        }
        .gift-size {
            background-size: 250px;
        }
        .auto-img {
            background-size: 100px
        }
    }
    @media (max-width: 600px) {
        .gift-img, .auto-img {
            background-image: none
        }
    }
    @media (max-width: 576px) {
        .menu-num-block {
            align-items: center;
            margin-top: 25px;
        }
        .logo {
            display: block;
            text-align: center;
        }
    }
    </style>
</head>
<body>
    <table style="max-width:645px;margin:auto;">
        <tbody><tr><td>
            <!--========Header=========-->
            <table style="margin: 59px 0 0;width: 100%;">
                <tbody><tr><td>
                    <div class="main-container" style="position: relative;">
                        <div class="header-content">
                            <a href="http://soauto.ru" class="logo">
                                <img src="https://hb.bizmrg.com/soautomedia/media/Header/logo.png" alt="Logo">
                            </a>
                            <a href="tel:88005459988" class="menu-num-block" style="width: 156px;text-align: right;float: right;">
                                <span style="text-transform: uppercase" class="menu-number">+7 (800) 551 53 01</span>
                                <span class="menu-number-text">Бесплатная горячая линия</span>
                            </a>
                        </div>
                    </div>
                </td></tr></tbody>
            </table>
            <?php  if ($actionText !== 'Reset Password' && $actionText === 'Подтвердите регистрацию на SOAUTO') { $user = \Auth::user(); ?>
            <table style="text-align: center;width: 100%;">
                <tbody><tr><td>
                <div style="margin-top: 57px;">
                    <h1 class="main-title">Привет, {{ __($user['name']) }}</h1>
                    <h4 class="main-subtitle">Пожалуйста нажми на кнопку ниже, чтобы завершить регистрацию
                        своей учетной записи.</h4>
                    <div style="line-height:1.2em;">
                        <a href="{{ $url }}" class="verify-btn" style="vertical-align: middle;line-height: 25px;font-family: 'Circe-bold';font-size: 18px;color: #000;background: #F6D200;width: 225px;height: 56px;border-radius: 30px;-webkit-border-radius: 30px;-moz-border-radius: 30px;-ms-border-radius: 30px;-o-border-radius: 30px;margin: 34px auto 0;cursor: pointer;display: block;cursor: pointer;">
                            Подтвердить адрес электронной почты
                        </a>
                    </div>
                    <h4 class="main-subtitle second" style="padding-bottom: 36px;position: relative;">Если это письмо пришло тебе по 
                    ошибке, игнорируй его.</h4>
                    <hr style="width: 165px;height: 2px;background: #F6D200;margin: 0 auto; border:none;"> 
                    <h5 class="main-create-account">Ты же знаешь, что на SOAUTO можно делиться своим автомобилем? Преврати его в 
                        полезный инструмент для семьи, друзей и знакомых, отдавая в поездку на дачу или на море. Не давай авто 
                        простаивать и экономь на ТО и содержании!</h5>

                    {{-- <ul style="text-align:center;margin: 52px auto 0;">
                        <li><span class="order-title">Твой промокод</span></li>
                        <li><button class="order-btn"></button></li>
                        <li><a href="#" class="order-link">Активируя промокод, Вы
                            соглашаетесь с <span>условиями акции.</span></a></li>
                    </ul> --}}
                    
                    <div class="bottom-block">
                        <span class="community-text" style="text-align: left;width: 110px;">Сообщество автолюбителей</span>
                        <a href="#" class="cummunity-img">
                            <img src="https://hb.bizmrg.com/soautomedia/media/Main-block/Logo-grey.png" alt="Logo" class="cummunity-img">
                        </a>
                    </div>
                    <div class="social">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="https://hb.bizmrg.com/soautomedia/media/social/fb-icon.png" alt="img">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="https://hb.bizmrg.com/soautomedia/media/social/youtube.png" alt="img">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="https://hb.bizmrg.com/soautomedia/media/social/instagram.png" alt="img">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="https://hb.bizmrg.com/soautomedia/media/social/vk.png" alt="img">
                                </a>
                            </li>
                        </ul>
                    </div>
                    </td></tr></tbody>
                </table>
                <table style="margin-top: 70px;width: 100%;">
                    <tbody><tr><td>
                        <div class="main-container">
                            <p class="footer-text">
                                If you’re having trouble clicking the "Verify Email Address" button, copy and paste
                                the URL below into your web browser:
                            </p>
                            <a href="{{ $url }}" class="footer-link">
                                {{ $url }}
                            </a>
                            <span class="copyright">© 2019 SOAUTO. All rights reserved.</span>
                        </div>
                    </td></tr></tbody>
                </table>
                <?php } else { ?>
                <table style="text-align: center;width: 100%;">
                <tbody><tr><td>
                    <div style="margin-top: 57px;">
                        <h1 class="main-title">Привет</h1>
                        <h4 class="main-subtitle">Мы получили запрос на восстановление пароля твоей 
                            учетной записи. Чтобы сменить пароль нажми на кнопку ниже:
                        </h4>
                        <a href="{{ $url }}" class="verify-btn" style="vertical-align: middle;line-height: 50px;font-family: 'Circe-bold';font-size: 18px;color: #000;background: #F6D200;width: 225px;height: 56px;border-radius: 30px;-webkit-border-radius: 30px;-moz-border-radius: 30px;-ms-border-radius: 30px;-o-border-radius: 30px;margin: 34px auto 0;cursor: pointer;display: block;cursor: pointer;">
                            Восстановить пароль
                        </a>
                        <h4 class="main-subtitle second" style="padding-bottom: 36px;position: relative;">
                        Ссылка будет действительна в течение 60 минут.</h4>
                        <hr style="width: 165px;height: 2px;background: #F6D200;margin: 0 auto; border:none;"> 
                        <h5 class="main-create-account">Если это письмо пришло тебе по ошибке, игнорируй его.</h5>
                        <h5 class="main-create-account">С Уважением, SOAUTO</h5>
                        <div class="bottom-block">
                            <span class="community-text" style="text-align: left;width: 110px;">Сообщество автолюбителей</span>
                            <a href="#" class="cummunity-img">
                                <img src="https://hb.bizmrg.com/soautomedia/media/Main-block/Logo-grey.png" alt="Logo" class="cummunity-img">
                            </a>
                        </div>
                        <div class="social">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/fb-icon.png" alt="img">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/youtube.png" alt="img">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/instagram.png" alt="img">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/vk.png" alt="img">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td></tr></tbody>
            </table>
            <table style="margin-top:70px;width:100%;">
                <tbody><tr><td>
                    <div class="main-container">
                        <p class="footer-text">
                        <a href="{{ $url }}" class="footer-link">
                            {{ $url }}
                        </a>
                        <span class="copyright">© 2019 SOAUTO. All rights reserved.</span>
                    </div>
                </td></tr></tbody>
            </table>
            <?php } ?>
        </td></tr></tbody>
    </table>
    <!--============Script=============-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>