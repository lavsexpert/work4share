@extends('layouts.home')
@section('body_class', 'page')
@section('content')

<div class="booking__page">
  <section style="background-image: url('/image/booking/intro-bg.jpg')" class="booking__page__intro">
    <h1>SOAUTO — это место, <br>объединяющее людей, <br>для совместного использования авто</h1>
    <p>Ты можешь не знать этих людей, но они готовы поделиться с тобой своим автомобилем. Это настоящий каршеринг.</p>
    <div class="d-flex justify-content-center"><a href="{{route('car.catalog.index')}}" class="btn btn-primary"> <img src="/image/car.svg" alt="">мне нужен авто</a></div>
  </section>
  <section class="booking__page__why">
    <div class="container">
      <div class="row">
        {{-- <div class="col-12 d-none d-sm-none d-md-block">
          <div class="heading">
            <h2>Почему SOAUTO?</h2>
          </div>
          <div class="booking__page__why__icons"><i class="icon icon-car"></i><span>+ </span><i class="icon icon-car"></i><span>+</span><i class="icon icon-car"></i></div>
          <div class="booking__page__why__headings">
            <h3>Удобно<span>+</span>Выгодно<span>+</span>Просто</h3>
          </div>
          <div class="booking__page__why__texts">
            <p>Найди авто, подготовленый владельцем именно для твоей поездки</p>
            <p>Сопоставь свои желания и бюджет, чтобы выбрать лучший вариант</p>
            <p>Получи и позже верни авто за пять минут, в поездке мы и владелец на связи</p>
          </div>
        </div> --}}
        <div class="col-12 d-flex flex-wrap">
          <div class="heading w-100">
            <h2>Почему SOAUTO?</h2>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-easy"></i>
            <h3>Удобно</h3>
            <p>Найди авто, подготовленый владельцем именно для твоей поездки</p>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-profit"></i>
            <h3>Выгодно</h3>
            <p>Сопоставь свои желания и бюджет, чтобы выбрать лучший вариант</p>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-comfort"></i>
            <h3>Просто</h3>
            <p>Получи и позже верни авто за пять минут, в поездке мы и владелец на связи</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="booking__page__secure">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2>Ты защищен!</h2>
          </div>
          <div class="d-flex justify-content-center">
              <i class="icon icon-shield"></i>
          </div>
          <p class="yellow">Безопасность - наш главный приоритет</p>
          <p>В пути может случиться что угодно.<br> Но твоя ответственность перед владельцем автомобиля всегда ограничена. КАСКО поможет, в случае повреждения авто, а если случится неприятность - мы поможем решить ее.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="booking__page__how">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2>Как снять авто в аренду?</h2>
          </div>
        </div>
        <div class="col-12 d-flex justify-content-between booking__page__how__row">
          <div class="booking__page__how__block">
            <div>1</div>
            <h3>НАЙДИ АВТОМОБИЛЬ</h3>
            <p>Выбирай из авто, которыми поделились реальные люди</p>
          </div>
          <div class="booking__page__how__block">
            <div>2</div>
            <h3>ЗАБРОНИРУЙ НУЖНЫЕ ДАТЫ</h3>
            <p>Выбери доставку в аэропорт или предложи место встречи</p>
          </div>
          <div class="booking__page__how__block">
            <div>3</div>
            <h3>БЕРИ АВТО И В ПУТЬ</h3>
            <p>Встреться с хозяином, получи авто и отправляйся в путешествие</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>В SOAUTO аренда автомобилей не главное. Мы хотим чтобы авто друга, родственника или незнакомца стал и твоим. Внедорожник на рыбалку, минивен в отпуск, электрокар для поездок по городу.</p>
          <p class="small">Команда SOAUTO</p>
        </div>
      </div>
    </div>
  </section>
  <section class="category category--booking">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>Выбирай свое авто</h2>
        </div>
        <div class="col d-flex flex-wrap"><a href="{{route('car.catalog.index')}}" class="category__block black">
            <h3>#поделам</h3>
            <p>бизнес и премиум<br>авто</p><img src="/image/category/black.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block yellow">
            <h3>#просто</h3>
            <p>удобные<br>и экономичные</p><img src="/image/category/yellow.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block white">
            <h3>#длягруза</h3>
            <p>вместительные</p><img src="/image/category/white.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block red">
            <h3>#настиле</h3>
            <p>спорт кары <br>и лакшери авто</p><img src="/image/category/red.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block blue">
            <h3>#наотдых</h3>
            <p>отправиться<br>в путешествие</p><img src="/image/category/blue.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block grey">
            <h3>#сдрузьями</h3>
            <p>от 6 мест</p><img src="/image/category/grey.png" alt="">
          </a></div>
      </div>
    </div>
  </section>
</div>

@endsection