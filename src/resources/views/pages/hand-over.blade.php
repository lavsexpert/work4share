@extends('layouts.home')
@section('body_class', 'page')
@section('content')
<div class="handover__page">
  <section style="background-image: url('/image/hand-over/intro-bg.jpg')" class="handover__page__intro page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div>
            <h1>ПОДЕЛИСЬ СВОИМ <br>АВТОМОБИЛЕМ</h1>
            <p>Добавь автомобиль на платформу, выбирай кто может брать его в аренду. Регистрация  проста и занимает до 10 минут.</p>
            <div class="d-flex justify-content-center"><a href="{{route('car.create.show')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>делиться своим авто</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__earn">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="black">— Зачем мне это нужно?</h2>
            <p class="font-weight-bold">SOAUTO дает возможность рационально использовать автомобиль и окупать его содержание, делясь им с друзьями и коллегами, в виде аренды.</p>
            <p>Чтобы понять, нужно попробовать. Если ты на выходных планируешь остаться дома, отдай свой автомобиль другу для поездки выходного дня. А мы организуем весь процесс.</p>
            <div class="d-flex justify-content-center"><a href="{{route('how')}}" class="btn btn-primary">как это работает</a></div>
          </div>
        </div>
        <div class="col-lg-7">
          <div class="handover__page__earn__block">
            <h3>Ценообразование</h3>
            <p>Ты можешь самостоятельно установить цену за сутки поездки. И решать, кто будет пользоваться твоим автомобилем. А в скором времени ты сможешь управлять своим авто с нашей помощью, супруга будет бронировать автомобиль бесплатно, брат будет платить символическую стоимость, а коллега или незнакомец поможет тебе оплатить следующий ТО.</p>
          </div>
        </div>
        <div class="col-lg-4 offset-lg-1">
          <div class="handover__page__earn__block">
            <h3>Расходы</h3>
            <p>SOAUTO будет брать символическую стоимость за пользование платформой в зависимости от того, как часто ты будешь делиться автмообилем. Для новых пользователей мы обязательно подключаем КАСКО, чтобы защитить тебя SOAUTORA от неприятных ситуаций.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section style="background-image: url('/image/hand-over/connect-bg.jpg')" class="handover__page__connect">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="yellow">СКОРО<br> Дистанционая<br> передача авто</h2>
          </div>
          <div class="d-flex justify-content-center"><img src="/image/hand-over/connect-logo.png" alt=""></div>
          <p class="font-weight-bold">Уже этим летом советуем попробовать SOAUTO Connect — новую захватывающую функцию, которая позволит тебе дистанционно разблокировать свой автомобиль, отслеживать местоположение, пробег и многое другое.</p>
          <p>Оставь заявку сейчас, чтобы в числе первых подключить свой авто к системе дистанционного управления SOAUTO Connect.</p>
          <div class="d-flex justify-content-center"><a href="{{route('contact')}}" class="btn btn-primary">Оставить заявку</a></div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>Мы считаем что главное не владеть автомобилем, а правильно им распоряжаться. Автомобиль не должен простаивать в гараже или использоваться не по назначению. Лучше им поделиться. Это честно, это выгодно. С SOAUTO это просто.</p>
          <p class="small">Команда SOAUTO</p>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__spirit">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="white">Не о чем беспокоиться</h2>
          </div>
        </div>
        <div class="row">
          <div class="end d-flex flex-column align-items-end">
            <i class="icon icon-shield"></i>
            <h3>Ты в безопасности</h3>
            <p>При регистрации мы проверяем все юридические мелочи, чтобы не было проблем. Мы страхуем ответственность по КАСКО, чтобы ты не думал о возможных неприятностях на дороге.</p>
          </div>
          <div class="start d-flex flex-column">
            <i class="icon icon-users"></i>
            <h3>Ты не одинок</h3>
            <p>Позвони на  круглосуточную линию технической поддержки за советом или подсказкой. Мы будем рядом на на каждом шагу.</p>
          </div>
        </div>
        <div class="col-12">
          <h4>Зарегистрируй свой автомобиль на soauto совершенно бесплатно. <span>Нет ежемесячной платы, нет стартового взноса.</span></h4>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__registry">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>Присоединяйся к нам,<br> и используй авто по новому!</h2>
          <div class="d-flex justify-content-center"><a href="{{route('car.create.show')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>добавить свой автомобиль</a></div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection