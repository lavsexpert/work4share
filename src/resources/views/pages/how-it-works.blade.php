@extends('layouts.home')
@section('body_class', 'page')
@section('content')
<div class="how__page">
  <section style="background-image: url('/image/how/intro-bg.jpg')" class="how__page__intro page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div>
            <h1>Как работает<br> SOAUTO</h1>
            <p>Твоя поездка начинается здесь</p>
            <div class="d-flex justify-content-center flex-column flex-md-row">
              <a href="{{route('booking')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>найти авто</a>
              <a href="{{route('handOver')}}" class="btn btn-primary btn-white">поделиться своим авто</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="how__page__road" id="road">
    <div class="container">
      <div class="row">
        <div class="col-12 d-none d-xl-flex">
          <div class="left">
            <h2>Арендовать:</h2>
            <div class="how__page__road__block left__block">
              <h3>Зарегистрируйся</h3>
              <p>Отправь подтверждающие документы. Дождись результатов проверки личности и водительских прав. И теперь ты верифицированный пользователь.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Выбери подходящий авто</h3>
              <p>Ты можешь арендовать автомобиль, необходимый здесь и сейчас. Отправляешься ты в путешествие или едешь на дачу.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забронируй автомобиль</h3>
              <p>Выбери время и место встречи, и отправь заявку. Владелец подтвердит или отклонит бронь, что обычно занимает не больше часа.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забери автомобиль</h3>
              <p>Приезжай по адресу и встреться с владельцем. А может и владелец на автомобиле приедет на указанное место, если вы договорились на доставку.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Наслаждайся поездкой</h3>
              <p>Передвигайся по городу, стране, или за ее пределами, несколько дней или недель, комфортно, свободно.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Верни автомобиль владельцу</h3>
              <p>Приведи автомобиль в порядок перед тем, как вернуть владельцу. Вместе осмотрите автомобиль со всех сторон, убедитесь, что нет повреждений. Теперь можно вернуть ключи, поблагодарить друг-друга и начать планировать новую поездку!</p>
            </div>
          </div>
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>
          <div class="right">
            <h2>Поделиться:</h2>
            <div class="how__page__road__block right__block">
              <h3>Зарегистрируйся и добавь авто</h3>
              <p>Это займет не больше 10 минут. Опиши своё авто, добавь несколько хороших фото и вперед!</p>
              <p>Не забывай вовремя обновлять информацию о датах и времени, когда твой автомобиль доступен для бронирования.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Ответь на запрос</h3>
              <p>Если кто-то заинтересовался твоим автомобилем, он выбирает дату и место встречи и отправляет запрос на бронирование, ты мгновенно получишь об этом уведомление. Как можно быстрее подтверди или отклони предложение. Детали можно обсудить в переписке.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Передай автомобиль</h3>
              <p>Вместе осмотрите автомобиль со всех сторон и сделайте фото. Зафиксируйте в приложении уровень бензина и пробег. Теперь ты можешь передать ключи и расслабиться. Если что-то понадобится с тобой свяжутся.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Не беспокойся ни о чем</h3>
              <p>КАСКО избавляет от волнений за состояние авто, а связь с водителем через личные сообщения удовлетворяет любопытство.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Забери свой автомобиль</h3>
              <p>Встретьтесь на том-же месте в последний день поездки и проверь, всё ли в порядке. Можете поблагодарить друг друга и возвращаться домой. Не забудь оставить отзыв, это поможет нам стать надежнее и удобнее.</p>
            </div>
          </div>
        </div>

        <div class="col-12 d-none d-lg-flex d-xl-none medium">
          <div class="left">
            <h2>Арендовать:</h2>
            <div class="how__page__road__block left__block">
              <h3>Зарегистрируйся</h3>
              <p>Отправь подтверждающие документы. Дождись результатов проверки личности и водительских прав. И теперь ты верифицированный пользователь.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Выбери подходящий авто</h3>
              <p>Ты можешь арендовать автомобиль, необходимый здесь и сейчас. Отправляешься ты в путешествие или едешь на дачу.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забронируй автомобиль</h3>
              <p>Выбери время и место встречи, и отправь заявку. Владелец подтвердит или отклонит бронь, что обычно занимает не больше часа.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забери автомобиль</h3>
              <p>Приезжай по адресу и встреться с владельцем. А может и владелец на автомобиле приедет на указанное, если вы договорились на доставку.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Наслаждайся поездкой</h3>
              <p>Передвигайся по городу, стране, или за ее пределами, несколько дней или недель, комфортно, свободно.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Верни автомобиль владельцу</h3>
              <p>Приведи автомобиль в порядок перед тем, как вернуть владельцу. Вместе осмотрите автомобиль со всех сторон, убедитесь, что нет повреждений. Теперь можно вернуть ключи, поблагодарить друг-друга и начать планировать новую поездку!</p>
            </div>
          </div>
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>
          <div class="right">
            <h2>Поделиться:</h2>
            <div class="how__page__road__block right__block">
              <h3>Зарегистрируйся и добавь авто</h3>
              <p>Это займет не больше 10 минут. Опиши своё авто, добавь несколько хороших фото и вперед!</p>
              <p>Не забывай вовремя обновлять информацию о датах и времени, когда твой автомобиль доступен для бронирования.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Ответь на запрос</h3>
              <p>Если кто-то заинтересовался твоим автомобилем, он выбирает дату, место встречи и отправляет запрос на бронирование, ты мгновенно получишь об этом уведомление. Как можно быстрее подтверди или отклони предложение. Детали можно обсудить в переписке.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Передай автомобиль</h3>
              <p>Вместе осмотрите автомобиль со всех сторон и сделайте фото. Зафиксируйте в приложении уровень бензина и пробег. Теперь ты можешь передать ключи и расслабиться. Если что-то понадобится с тобой свяжутся.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Не беспокойся ни о чем</h3>
              <p>КАСКО избавляет от волнений за состояние авто, а связь с водителем через личные сообщения удовлетворяет любопытство.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Забери свой автомобиль</h3>
              <p>Встретьтесь на том-же месте в последний день поездки и проверь, всё ли в порядке. Можете поблагодарить друг друга и возвращаться домой. Не забудь оставить отзыв, это поможет нам стать надежнее и удобнее.</p>
            </div>
          </div>
        </div>

        <div class="col-12 justify-content-end d-flex d-lg-none d-xl-none small">
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>          
          <div class="right">
            <h2>Сдавать в аренду:</h2>
            <div class="how__page__road__block left__block">
              <h3>Зарегистрируйся</h3>
              <p>Отправь подтверждающие документы. Дождись результатов проверки личности и водительских прав. И теперь ты верифицированный пользователь.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Зарегистрируйся и добавь авто</h3>
              <p>Это займет не больше 10 минут. Опиши своё авто, добавь несколько хороших фото и вперед!</p>
              <p>Не забывай вовремя обновлять информацию о датах и времени, когда твой автомобиль доступен для бронирования.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Выбери подходящий авто</h3>
              <p>Ты можешь арендовать автомобиль, необходимый здесь и сейчас. Отправляешься ты в путешествие или едешь на дачу.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Ответь на запрос</h3>
              <p>Если кто-то заинтересовался твоим автомобилем, он выбирает дату и место встречи и отправляет запрос на бронирование, ты мгновенно получишь об этом уведомление. Как можно быстрее подтверди или отклони предложение. Детали можно обсудить в переписке.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забронируй автомобиль</h3>
              <p>Выбери время и место встречи, и отправь заявку. Владелец подтвердит или отклонит бронь, что обычно занимает не больше часа.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Передай автомобиль</h3>
              <p>Вместе осмотрите автомобиль со всех сторон и сделайте фото. Зафиксируйте в приложении уровень бензина и пробег. Теперь ты можешь передать ключи и расслабиться. Если что-то понадобится с тобой свяжутся.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Забери автомобиль</h3>
              <p>Приезжай по адресу и встреться с владельцем. А может и владелец на автомобиле приедет на указанное, если вы договорились на доставку.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Не беспокойся ни о чем</h3>
              <p>КАСКО избавляет от волнений за состояние авто, а связь с водителем через личные сообщения удовлетворяет любопытство.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Наслаждайся поездкой</h3>
              <p>Передвигайся по городу, стране, или за ее пределами, несколько дней или недель, комфортно, свободно.</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>Забери свой автомобиль</h3>
              <p>Встретьтесь на том-же месте в последний день поездки и проверь, всё ли в порядке. Можете поблагодарить друг друга и возвращаться домой. Не забудь оставить отзыв, это поможет нам стать надежне и удобнее.</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>Верни автомобиль владельцу</h3>
              <p>Приведи автомобиль в порядок перед тем, как вернуть владельцу. Вместе осмотрите автомобиль со всех сторон, убедитесь, что нет повреждений. Теперь можно вернуть ключи, поблагодарить друг-друга и начать планировать новую поездку!</p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>
  <section class="how__page__delivery">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="white">Доставка автомобиля</h2>
            <p class="yellow">Представь как приятно, когда автомобиль останавливается прямо у порога или ждет на выходе из гостиницы в центре города. Лишь раз получив ключи прямо из рук владельца, уже не захочешь идти в прокатную компанию.</p>
            <h3>Есть 3 способа встретиться:</h3>
            <div class="row">
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-point-on"></i>
                  <p>Владелец доставляет автомобиль по указанному адресу</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-plane"></i>
                  <p>Владелец доставляет<br> автомобиль в аэропорт</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-point"></i>
                  <p>Арендатор забирает автомобиль по адресу владельца</p>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-center flex-column flex-md-row"><a href="#" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>найти авто</a><a href="#" class="btn btn-primary">поделиться своим авто</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection