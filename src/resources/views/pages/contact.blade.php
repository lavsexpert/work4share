@extends('layouts.home')
@section('body_class', 'page')
@section('content')

<div class="contact__page">
  <section class="contact__page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1>— Если у тебя возникли вопросы, спрашивай!</h1>
          <p>Ты можешь связаться с нами удобным способом:</p>
        </div>
      </div>
    </div>
  </section>
  <section class="contact__page__form">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
          <div class="contact__page__form__block">
            <p>напиши в Whatsapp на номер:</p><a href="tel:+7 937 536-24-51">+7 937 536-24-51</a>
          </div>
        </div>
        <div class="col-lg-2 offset-lg-1">
          <div class="contact__page__form__block">
            <p>позвони на горячую линию:</p><a href="tel:8 800 551 53 01">8 800 551 53 01</a>
          </div>
        </div>
        <div class="col-lg-6 offset-lg-1">
          <div class="contact__page__form__block">
            <p>оставь свой вопрос и мы ответим на email.</p>
            <h3>Заполни форму:</h3>
            <form class="form__contact"><input type="text" name="name" placeholder="Тема*"><textarea name="message">Твое сообщение</textarea><input type="tel" name="phone" placeholder="Телефон*"><input type="email" name="email" placeholder="Email*"><span>* – обязательное поле </span>
              <div class="checkbox"><input id="ckeckbox" type="checkbox" name="checkbox" checked><label for="ckeckbox" class="label"></label><label for="ckeckbox" class="label-for">я не робот и соглашаюсь с обработкой данных.</label></div><button type="submit" class="btn btn-primary btn-purple">отправить вопрос</button></form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote black">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>Конструктивная обратная связь способна изменить мир к лучшему и сделать наше сообщество еще больше и полезнее каждому.</p>
          <p class="small">Иван Ершов, креативный директор SOAUTO</p>
        </div>
      </div>
    </div>
  </section>
  <section class="contact__page__info">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <h3>Реквизиты компании:</h3>
          <p class="font-weight-bold">ООО "СОАВТО"</p>
          <p>ОГРН 1197746281116 ИНН 7724473283 КПП 772401001</p>
          <p>115230, г. Москва, проезд Хлебозаводский, 7с9</p>
        </div>
        <div class="col-lg-2 offset-lg-2"><img src="/image/logo.svg" alt=""></div>
      </div>
    </div>
  </section>
  {{-- <section style="background-image: url('/image/map.jpg')" class="contact__page__map"></section> --}}
</div>

@endsection