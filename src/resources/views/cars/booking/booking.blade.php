@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{trans('cars.booking')}}</title>
</head>
<body>

    {{print_r($bookingDetals)}}

    <div class="container">
        <div class="row">
            <div class="col-sm-12">

            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                        <div class="card">
                                <div class="card-header">
                                  {{trans('cars.booking_details')}}
                                </div>
                                <div class="card-body">
                                  <p class="card-text">
                                    <p>{{trans('cars.booking_brand')}}: <span>{{$selectionCars[0]->brand}}</span></p>
                                    <p>{{trans('cars.booking_model')}}: <span>{{$selectionCars[0]->model}}</span></p>
                                    <p>{{trans('cars.booking_date')}}: <span>{{date('d-m-Y', $bookingDetals['from'])}} - {{date('d-m-Y', $bookingDetals['to'])}}</span></p>
                                    <p>{{trans('cars.booking_days')}}: <span>{{$bookingDetals['days']}}</span></p>
                                    <p>{{trans('cars.booking_cost')}}: <span>{{$bookingDetals['fullCostDays']}}</span></p>
                                    <p>{{trans('cars.booking_delivery')}}: <span>{{$bookingDetals['delivery']}}</span></p>
                                    <p>{{trans('cars.booking_full_cost')}}: <span>{{$bookingDetals['fullCost']}}</span></p>

                                  </p>
                                </div>
                              </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                    <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        {{trans('cars.payments')}}
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            <p>{{trans('cars.payments_card')}}</p>
                            <p>{{trans('cars.payments_card_number')}} <input type="tel" inputmode="numeric" name="cardnumber" id="cardnumber" autocomplete="cc-number" placeholder="" class="credit-card-form__input js-cc-input js-cc-number-input" pattern="[0-9]*"></p>
                            <p>{{trans('cars.payments_card_validity')}} <input type="tel" inputmode="numeric" name="expdate" id="expdate" autocomplete="cc-exp" placeholder="" class="credit-card-form__input js-cc-input js-cc-exp-input" pattern="[0-9]*"></p>
                            <p>{{trans('cars.payments_card_cvc')}} <input type="tel" name="cvc2" id="cvc2" placeholder="" class="credit-card-form__input js-cc-input js-cc-cvv-input" pattern="[0-9]*" autocomplete="off"></p>
                            <a href="/bookingcar/success?car_id={{$selectionCars[0]->id}}&from={{date('d-m-Y', $bookingDetals['from'])}}&to={{date('d-m-Y', $bookingDetals['to'])}}" class="btn btn-primary">Оплатить {{$bookingDetals['fullCost']}} руб.</a>
                        </p>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

@endsection
