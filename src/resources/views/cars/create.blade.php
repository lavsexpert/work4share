@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/image_push.js') }}"></script>
@endpush
@section('content')

<section class="creating-cars">
	<div class="container">
		<h2 class="my-car-title">{{trans('cars.create')}}</h2>
		<form method="POST" action="{{ route('car.create') }}" aria-label="{{ __('Create') }}" class="form-cars">
			@csrf
			<div class="row">
				<div class="col-12 col-sm-3">
					<div class="form-cars__wrap">
						<h2 class="form-cars__title">
							{{trans('cars.create_brand')}}
						</h2>

						<select id="brandid" name="brand_id" class="form-cars__select">
							<option disabled selected>{{trans('cars.nothing_selected')}}</option>
							@foreach ($data['brand'] as $item)
									<option value="{{ $item['id'] }}">
											{{ $item['name'] }}
									</option>
							@endforeach
						</select>

					</div>
				</div>
				<div class="col-12 col-sm-3">
					<div class="form-cars__wrap">
						<h2 class="form-cars__title">
							{{trans('cars.create_model')}}
						</h2>
						<select id="model" name="model_id" class="form-cars__select" required>
							<option disabled selected value="0">{{trans('cars.nothing_selected')}}</option>
							{{-- <option disabled selected class="form-cars__option">
								{{trans('cars.nothing_selected')}}
							</option> --}}
						</select>
					</div>
				</div>
				<div class="col-12 col-sm-3">
					<div class="form-cars__wrap">
						<h2 class="form-cars__title">
							{{trans('cars.create_type')}}
						</h2>
						<select id="carTypeid" name="car_type" class="form-cars__select">
							<option disabled selected>{{trans('cars.nothing_selected')}}</option>
							@foreach ($car_type as $item)
								<option value="{{ $item['id'] }}">
									{{ $item['name'] }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<button type="submit" class="btn creating-cars-btn">
				{{trans('cars.create_add')}}
			</button>
		</form>
	</div>
</section>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">{{trans('cars.create_modal')}}</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			</button>
		</div>
		<div class="modal-body">
      <h2>{{trans('cars.create_body_title')}}</h2>
      <h2>{{trans('cars.create_body_need')}}</h2>
      <ul>
        <li>{{trans('cars.create_body_document')}}</li>
        <li>{{trans('cars.create_body_photo')}}</li>
      </ul>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('cars.create_good')}}</button>
		</div>
		</div>
	</div>
</div>

<script>
$(function () {
	$('#modal').modal('show')
	$('.btn-close').click(function(){
		$('#modal').modal('toggle');
	});
});
</script>
@endsection
