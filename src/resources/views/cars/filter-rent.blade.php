@extends('layouts.app')
@section('content')

<section class="changing-card filterRent">
	<div class="container">
		<h1 class="changing-card-title">
			{{trans('cars.filter')}}
		</h1>
		<div class="row">
			<div class="col">
				<ul class="card-list">
          <li class="card-list__item d-flex justify-content-between">
              <form id="" method="GET" action="{{ route('car.search') }}" class="d-flex justify-content-between saveYearSeatsDoors" style="width: 100%;">
{{--                @csrf--}}
                <div class="form-production-date">
                  <h2 class="form-production-date__title">
                    {{trans('cars.filter_from')}}
                  </h2>
                  <input name="from" class="rent-form__select datepicker-here" data-timepicker="true" type="text" required autocomplete="off">
                </div>
                <div class="form-number-persons">
                  <h2 class="form-number-persons__title">
                    {{trans('cars.filter_to')}}
                  </h2>
                  <input name="to" class="rent-form__select datepicker-here" data-timepicker="true" type="text" required autocomplete="off">
                </div>
                <div class="number-dor">
                  <h2 class="number-dor__title">
                    {{trans('cars.filter_place')}}
                  </h2>
                  <select name="seat" class="form-number-persons__select" required>
                      <option class="form-number-persons__option" value="">
                        {{trans('cars.nothing_selected')}}
                      </option>
                      @foreach ($rent_place as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                      @endforeach
                    </select>
                </div>
                <button type="submit" class="btn card-list__btn">
                  {{trans('cars.filter_button')}}
                </button>
              </form>
            </li>
				</ul>
			</div>
		</div>
	</div>
</section>

@endsection
