@extends('layouts.app')

@section('content')

<section class="changing-card">
	<div class="container">
		@if ($register_step !== 2)
		<h1 class="changing-card-title">
			{{trans('cars.show')}}
		</h1>
		<div class="row">
			<div class="col">
				<ul class="card-list">
					<li class="card-list__item card-list__info  d-flex">
						<div class="card-list__wrap ">
							<h2 class="card-list__title">
								{{trans('cars.show_brand')}}
							</h2>
							<p class="card-list__text">
								{{ $data['brand']['name'] }}
							</p>
						</div>
						<div class="card-list__wrap">
							<h2 class="card-list__title">
								{{trans('cars.show_model')}}
							</h2>
							<p class="card-list__text">
									{{ $data['model']['name'] }}
							</p>
						</div>
						<div class="card-list__wrap">
							<h2 class="card-list__title">
								{{trans('cars.show_type')}}
							</h2>
							<p class="card-list__text">
								{{ $data['type']['name'] }}
							</p>
						</div>
					</li>
					<li class="card-list__item"></li>
					@if (!$step_1)
						<li class="card-list__item d-flex justify-content-between">
							<form id="saveYearSeatsDoors" method="POST" action="/car/{{$data['id']}}/update" class="d-flex justify-content-between saveYearSeatsDoors flex-wrap" style="width: 100%;">
								@csrf
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_birth')}}
									</h2>
									<input type="number" min="1930" max="2019" class="login-form__input" name="year_of_issue" placeholder="{{trans('cars.show_birth')}}" required>
								</div>
								<div class="form-number-persons">
									<h2 class="form-number-persons__title">
										{{trans('cars.show_children')}}
									</h2>
									<select name="seat" class="form-number-persons__select" required>
										<option class="form-number-persons__option" value="">
											{{trans('cars.nothing_selected')}}
										</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
									</select>
								</div>
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_experience')}}
									</h2>
									<input type="number" class="login-form__input" name="mileage" placeholder="{{trans('cars.show_experience')}}" required>
								</div>
								<div class="form-production-date">
									<h2 class="form-production-date__title">
										{{trans('cars.show_salary')}}
									</h2>
									<input type="number" class="login-form__input" name="assessed_value" placeholder="{{trans('cars.show_salary')}}" required>
								</div>
								<div class="form-production-date">
										<h2 class="form-production-date__title">
											{{trans('cars.show_color')}}
										</h2>
										<select id="color_id" name="color_id" class="form-production-date__select" required>
												<option class="form-production-date__option" value="">
													{{trans('cars.nothing_selected')}}
												</option>


												@foreach ($colors as $color)
												<option class="form-production-date__option" value="{{$color->id}}">
														{{$color->name}}
													</option>
												@endforeach
										</select>
									</div>
								<button type="submit" class="btn card-list__btn mt-4">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li>
					@endif
					@if (!$step_2 && $step_1)
					<li class="card-list__item card__cost card-list__item__cost d-flex justify-content-between">
						<form id="saveTypeCost" method="POST" action="/car/{{$data['id']}}/unlock_type_cost" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
							@csrf
							<div class="form-production-date">
								<h2 class="form-production-date__title">
									{{trans('cars.show_salary_per_day')}}
								</h2>
								<input type="number" class="login-form__input" name="cost_day" placeholder="{{trans('cars.show_input_salary_per_day')}}" required>
							</div>
							<div class="number-dor">
								<h2 class="number-dor__title">
									{{trans('cars.show_booking_type')}}
								</h2>
								<div class="group-ridio" id="connection_type">
										@foreach ($unlock_type as $type)
											<input name="type" class="form-check-label" type="radio" value="{{ $type->id }}"
											@if($type->check) checked @endif
											id="{{ $type->id }}">
											<label for="{{ $type->id }}">{{ $type->name }}</label>
										@endforeach
								</div>
							</div>
							<div class="number-dor d-none">
								<h2 class="form-production-dor__title">
									{{trans('cars.show_time_before')}}
								</h2>
								<select name="time_alert" class="form-number-persons__select">
									<option class="form-number-persons__option" value="">
										{{trans('cars.nothing_selected')}}
									</option>
									@foreach($timeAlertList as $id => $name)
										<option value="{{$id}}" {{ ($data['time_alert'] ?? null) == $id ? "selected" : "" }}> {{$name}}</option>
									@endforeach
								</select>
							</div>
							<button type="submit" class="btn card-list__btn">
								{{trans('cars.show_save')}}
							</button>
						</form>
					</li>
					@endif
					{{-- <li class="card-list__item card__mileage card-list__item__cost d-none justify-content-between">
						<form id="mileage_inclusive" method="POST" action="/car/{{$data['id']}}/mileage_inclusive" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
							@csrf
							<input type="hidden" name="car_id" value="{{$data['id']}}">
							<div class="number-dor">
								<h2 class="form-production-date__title">
									{{trans('cars.show_daily_load')}}
								</h2>
								<input type="number" class="login-form__input" name="day" placeholder="{{trans('cars.show_load_unit')}}" required>
							</div>
							<div class="number-dor">
								<h2 class="form-production-date__title">
									{{trans('cars.show_weekly_load')}}
								</h2>
								<input type="number" class="login-form__input" name="week" placeholder="{{trans('cars.show_load_unit')}}" required>
							</div>
							<div class="number-dor">
								<h2 class="form-production-date__title">
									{{trans('cars.show_monthly_load')}}
								</h2>
								<input type="number" class="login-form__input" name="month" placeholder="{{trans('cars.show_load_unit')}}" required>
							</div>
							<button type="submit" class="btn card-list__btn">
								{{trans('cars.show_save')}}
							</button>
						</form>
					</li> --}}

				{{-- @if (!$step_3 && $step_2)
					<li class="card-list__item card__rent card-list__item__cost d-flex justify-content-between flex-wrap">
						<div class="w-100">
							<h2 class="card-list__title card__photo__title mb-5">{{trans('cars.show_duration')}}</h2>
						</div>
						<form id="rent_time" method="POST" action="/car/{{$data['id']}}/rent_time" class="saveTypeCost d-flex justify-content-between" style="width: 100%;">
							@csrf
							<input type="hidden" name="car_id" value="{{$data['id']}}">
							<div class="number-dor">
								<h2 class="form-production-date__title">
									{{trans('cars.show_duration_min')}}
								</h2>
								<input type="number" class="login-form__input" name="min_rent_time" placeholder="{{trans('cars.show_duration_hint')}}" required>
							</div>
							<div class="number-dor">
								<h2 class="form-production-date__title">
									{{trans('cars.show_duration_max')}}
								</h2>
								<input type="number" class="login-form__input" name="max_rent_time" placeholder="{{trans('cars.show_duration_hint')}}" required>
							</div>
							<button type="submit" class="btn card-list__btn">
								{{trans('cars.show_save')}}
							</button>
						</form>
					</li>
				@endif --}}
				@if (!$step_4 && $step_2)
					<li class="card__photo card__photo__passport card-list__item justify-content-between align-items-center flex-wrap d-flex">
						<div class="w-100">
							<h2 class="card-list__title card__photo__title mt-4">{{trans('cars.show_identify')}}</h2>
						</div>
						<h2 class="card-list__title card-list__title_small">
							{{trans('cars.show_identify_first')}}
						</h2>
						<div class="card-list__Upload">
							<input type="file" id="passport_face" name="photo">
						</div>
					</li>
					<li class="card__photo card__photo__passport card-list__item justify-content-between align-items-center d-flex">
						<h2 class="card-list__title card-list__title_small">
							{{trans('cars.show_identify_registration')}}
						</h2>
						<div class="card-list__Upload">
							<input type="file" id="passport_visa" name="photo">
						</div>
					</li>
					<li class="card__photo card__photo__passport card-list__item justify-content-between align-items-center flex-wrap d-flex">
						<h2 class="card-list__title card-list__title_small">
								{{trans('cars.show_identify_with')}}
						</h2>
						<div class="card-list__Upload">
							<input type="file" id="selfie" name="photo">
						</div>
						{{--<form method="POST" id="passportData" action="{{ route('car.passportData', $data['id']) }}" class="d-flex justify-content-between w-100 flex-wrap">--}}
								{{--@csrf--}}
								{{--<input id="#" type="text" size="4" name="passport_seria" class="login-form__input seria" placeholder="{{trans('cars.show_identify_series')}}" required="">--}}
								{{--<input id="#" type="text" size="6" name="passport_nomer" class="login-form__input nomer" placeholder="{{trans('cars.show_identify_number')}}" required="">--}}
								{{--<input id="#" type="text" name="passport_issued_by" class="loFgin-form__input" placeholder="{{trans('cars.show_identify_issued')}}" required="">--}}
								{{--<input id="#" type="date" name="passport_issued" class="login-form__input" placeholder="{{trans('cars.show_identify_date')}}" required="">--}}
								{{--<input id="#" type="number" name="passport_issued" class="login-form__input" placeholder="{{trans('cars.show_identify_age')}}" required="">--}}
								{{--<div class="w-100 mt-3">--}}
									{{--<button type="submit" class="btn card-list__btn ">--}}
										{{--{{trans('cars.show_save')}}--}}
									{{--</button>--}}
								{{--</div>--}}
							{{--</form>--}}
							<hr class="w-100">
					</li>
				@endif

					@if (!$step_5 && $step_4 && $step_2)
						<li class="card__photo card__photo__sts card-list__item">
							<form method="POST" action="{{route('vin', $data['id'])}}" class="justify-content-between flex-wrap d-flex align-items-center">
								@csrf
								<h2 class="card-list__title card-list__title_small">
										{{trans('cars.show_number')}}
								</h2>
								<input type="text" class="login-form__input" name="vin" placeholder="Введите VIN" required>
								<button type="submit" class="btn card-list__btn">
									{{trans('cars.show_save')}}
								</button>
							</form>
						</li>
						@endif

						{{-- <li class="card__photo card__photo__sts card-list__item justify-content-between flex-wrap d-flex">
							<div class="w-100">
									<h2 class="card-list__title card__photo__title mt-4">{{trans('cars.show_document1')}}</h2>
							</div>

							<h2 class="card-list__title card-list__title_small">
								{{trans('cars.show_document1_first')}}
							</h2>
							<div class="card-list__Upload">
									<input type="file" id="sts1" name="photo">
							</div>
						</li> --}}
						{{-- <li class="card__photo card__photo__sts card-list__item justify-content-between align-items-center flex-wrap d-flex" >
							<h2 class="card-list__title card-list__title_small">
									{{trans('cars.show_document1_second')}}
							</h2>
							<div class="card-list__Upload">
								<input type="file" id="sts2" name="photo">
							</div>
							<hr class="w-100">
						</li>	 --}}



					{{-- @if (!$step_6 && $step_5)

						<li class="card__photo card__photo__pts card-list__item justify-content-between flex-wrap d-flex">
							<div class="w-100">
									<h2 class="card-list__title card__photo__title mt-4">{{trans('cars.show_document2')}}</h2>
							</div>

							<h2 class="card-list__title card-list__title_small">
								{{trans('cars.show_document2_first')}}
							</h2>
							<div class="card-list__Upload">
									<input type="file" id="pts1" name="photo">
							</div>
						</li>
						<li class="card__photo card__photo__pts card-list__item justify-content-between align-items-center flex-wrap d-flex" >
							<h2 class="card-list__title card-list__title_small">
									{{trans('cars.show_document2_second')}}
							</h2>
							<div class="card-list__Upload">
								<input type="file" id="pts2" name="photo">
							</div>
							<hr class="w-100">
						</li>
					@endif --}}
					@if ($step_7 == null && $step_5)
						<li class="card__photo card__photo__auto card-list__item justify-content-between align-items-center d-flex">
							<h2 class="card-list__title card-list__title_small">
									{{trans('cars.show_photo')}}
							</h2>
							<div class="card-list__Upload">
								<input multiple type="file" id="photo" name="photo">
							</div>
							<button id="savePhotoCar" class="btn card-list__btn">
								{{trans('cars.show_save')}}
							</button>
						</li>
					@endif
					{{-- @if ($step_8 === null && $step_7)
						<li class="card__osago card__osago_id card-list__item d-flex">
							<h2 class="changing-card-title">{{trans('cars.show_osago')}}</h2>
							<div class="card__osago__block">
								<div class="number-dor">
										<h2 class="number-dor__title">
											{{trans('cars.show_osago_count')}}
										</h2>
										<div class="group-ridio">
											<form method="POST" action="{{route('car.osago', $data['id'])}}">
												@csrf
												<input type="radio" id="osago_has_1" name="osago_has" value="0" required="">
												<label for="osago_has_1">{{trans('cars.show_osago_limited')}}</label>
												<input type="radio" id="osago_has_2" name="osago_has" value="1">
												<label for="osago_has_2">{{trans('cars.show_osago_not_limited')}}</label>
												<button type="submit" class="btn card-list__btn d-none">
													{{trans('cars.show_save')}}
												</button>
											</form>
										</div>
								</div>
							</div>
							<div class="card__osago__block osago__need" style="opacity: 0">
								<div class="card-list__Upload d-none">
									<input type="file" id="osago" name="photo">
								</div>
							</div>
						</li>
					@endif
					@if ($step_9 === null && $step_8 !== null)
						<li class="card__kasko card__osago card-list__item d-flex">
								<h2 class="changing-card-title">{{trans('cars.show_kasko')}}</h2>
								<div class="card__osago__block">
									<div class="number-dor">
											<h2 class="number-dor__title">
													{{trans('cars.show_kasko_has')}}
											</h2>
											<div class="group-ridio">
												<form method="POST" action="{{route('car.kasko', $data['id'])}}">
													@csrf
													<input type="radio" id="kasko_has_1" name="kasko_has" value="1">
													<label for="kasko_has_1">{{trans('cars.show_kasko_has_1')}}Да</label>
													<input type="radio" id="kasko_has_2" name="kasko_has" value="0">
													<label for="kasko_has_2">{{trans('cars.show_kasko_has_2')}}Нет</label>
													<button type="submit" class="btn card-list__btn d-none">
														{{trans('cars.show_save')}}
													</button>
												</form>
											</div>
									</div>
								</div>
								<div class="card__osago__block kasko__need" style="opacity: 0">
									<div class="card-list__Upload">
										<input type="file" id="kasko" name="photo">
									</div>
								</div>
						</li>
					@endif --}}


					{{-- @if (!$step_10 && $step_9 !== null)
						<li class="card__featured card-list__item d-flex">
							<form id="saveFeatured" class="w-100" method="POST" action="{{$data['id']}}/feature">
								@csrf
								<input type="hidden" name="redirect" value="1">
								<div class="row">
									<div class="col-12">
										<ul class="extra-wrap">
											@foreach ($data['feature'] as $title => $check)
												@isset($dataFeature[$title])
												<li class="extra-wrap__item">
													<div class="checkbox">
														<input id="{{$title}}" type="checkbox" name="{{$title}}" value="1" class="check"
														@if ($check)
															checked
														@endif
														>
														<label for="{{$title}}">
															{{ $dataFeature[$title] }}
														</label>
													</div>
												</li>
												@endisset
											@endforeach
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col d-flex justify-content-end">
										<button type="submit" class="btn card-list__btn d-flex justify-content-center">
											Сохранить
										</button>
									</div>
								</div>
							</form>
						</li>
					@endif --}}

					@if (!$step_11 && $step_7)
						<li class="checkout-list card__verification d-block">
							<div class="w-100 mb-4">
									<h2 class="card-list__title card__photo__title mt-4">{{trans('cars.show_confirmation')}}</h2>
							</div>
							<div class="container-confirm-phone checkout__block d-block">
									<div class="container-form justify-content-around">
											<form class="mr-4" method="GET" action="{{route('sendValidateSms')}}" id="sendValidateSms" class="validateForm">
													@csrf
													<div class="login-form__pass">
													<input id="#" type="text" name="phone" class="login-form__input phone" id="phone" placeholder="Введите номер" 	required value="{{$user['phone']}}">
													</div>
													<p></p>
													<button type="submit" class="checkout-btn btn">{{ __('{{trans('cars.show_confirmation_send')}}') }}</button>
											</form>
											<form method="POST" id="validatePhone" action="{{route('validatePhone')}}" class="validateForm">
													@csrf
													<div class="login-form__pass">
															<input id="#" type="number" name="code" class="login-form__input" id="phone_code" placeholder="Введите код" required>
													</div>
													<p></p>
													<button type="submit" class="checkout-btn btn">{{ __('{{trans('cars.show_confirmation_check')}}') }}</button>
											</form>
											<p class="confirm-text d-none">{{trans('cars.show_confirmation_complete')}}</p>
									</div>
							</div>
						</li>
					@endif

					@if ($step_11 && $step_7)
						<div class="row next w-100 mt-4 d-flex">
							<form id="redirect" method="POST" class="col d-flex justify-content-center" action="{{route('car.redirect', $data['id'])}}">
								@csrf
								<div class="checkbox w-50">
									<input id="policy" type="checkbox" name="policy" value="1" class="check"
									>
									<label for="policy">
										{{trans('cars.show_agreement')}}
										<a href="/agentskiy-dogovor-soauto.pdf">{{trans('cars.show_agreement_link')}}</a>

									</label>
									<div class="error d-none">
										<p>{{trans('cars.show_agreement_request')}}</p>
									</div>
								</div>
								<button class="btn card-list__btn" href="">{{trans('cars.show_agreement_finish')}}</button>
							</form>
						</div>
					@endif


				</ul>
			</div>
		</div>
		@else
			<h1>{{trans('cars.show_error_double')}}</h1>

			<a href="/profile/mycars/{{$data['id']}}">{{trans('cars.show_error_finish')}}</a>
		@endif
	</div>
</section>


{{--<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">{{trans('cars.show_error')}}</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			</button>
		</div>
		<div class="modal-body">
			<p>{{trans('cars.show_body_title')}}</p>
			<ul>
				<li>{{trans('cars.show_body_document1')}}</li>
				<li>{{trans('cars.show_body_document2')}}</li>
				<li>{{trans('cars.show_body_document3')}}</li>
				<li>{{trans('cars.show_body_document4')}}</li>
			</ul>
			<p>{{trans('cars.show_body_text1')}}</p>
			<p>{{trans('cars.show_body_text2')}}</p>
			<p>{{trans('cars.show_body_text3')}}</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('cars.show_body_good')}}</button>
		</div>
		</div>
	</div>
</div>--}}
<div class="modal fade" id="photos-modal" tabindex="-1" role="dialog" aria-labelledby="photoModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="photoModal">{{trans('cars.show_modal')}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>{{trans('cars.show_photo')}}</p>
				<ul>
					<li>{{trans('cars.show_photo1')}}</li>
					<li>{{trans('cars.show_photo2')}}</li>
					<li>{{trans('cars.show_photo3')}}</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('cars.show_photo_good')}}</button>
			</div>
		</div>
	</div>
</div>


<script>

	var car_id = <?= $data['id'] ?>;

</script>
@endsection
@section('bottom_scripts')
	<script>
		@if ($step_7 == null && $step_5)
		$(document).ready(function () {
			$('#photos-modal').modal('show');
		});
		@endif
	</script>
@endsection
