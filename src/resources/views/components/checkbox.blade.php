<div class="col-md-3 form-group form-check form-check-inline">
    <label for="{{ $title }}" class="col-md-12 col-form-label text-md-right">{{ __('form.checkbox.'.$title) }}</label>
    <input name="{{ $title }}" class="form-check-label" type="checkbox"
    @if($check)
        checked
    @endif
    id="{{ $title }}">
</div>
