<div id="{{$name}}" class="@isset($show){{'d-none'}}@endisset">

    <div class="form-cars__wrap">
        <h2 class="form-cars__title">
            {{ $title }}
        </h2>
        <select id="{{ $name.'id' }}"
        type="checkbox"
        @isset($required) required @endisset
        class="form-cars__select"
        name="{{ $name }}" >
        @if(request()->has($name.'id'))
            <option  value="">Ничего не выбрано</option>
            @foreach ($items as $item)
                <option
                class="form-cars__option" value="{{ $item['id'] }}" @if(request()->get($name.'id') == $item['id']) selected @endif>
                    {{ $item['name'] }}
                </option>
            @endforeach
            
        @else
        <option disabled selected value="">Ничего не выбрано</option>
            @foreach ($items as $item)
                <option
                value="{{ $item['id'] }}">
                    {{ $item['name'] }}
                </option>
            @endforeach
        @endif
        </select>
    </div>

</div>
