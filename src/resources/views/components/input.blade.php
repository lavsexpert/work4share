<div class="form-group row">
    <label for="{{ $title }}" class="col-md-4 col-form-label text-md-right">{{ $title }}</label>
    <input type="number" min="0" for="{{ $title }}" name="{{ $title }}" class="col-md-6 col-form-label text-md-left" value="{{ $slot }}"></label>
</div>
