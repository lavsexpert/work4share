<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Сделаем автомобиль доступным и полезным для каждого."/>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MMCHQQX');</script>
	<!-- End Google Tag Manager -->

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>



	<!-- Styles -->
	<link href="{{ asset('css/home.css') }}" rel="stylesheet">

	@stack('scripts')
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(53583991, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53583991" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body class="@yield('body_class')">

	
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMCHQQX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<div id="menu" class="slideout-menu">
		<ul>
			<li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">Главная</a></li>
			<li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">О нас</a></li>
			<li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">Арендовать</a></li>
			<li class="{{ Request::routeIs('handOver') ? 'active' : '' }}"><a href="{{route('handOver')}}">Поделиться авто</a></li>
			<li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Контакты</a></li>
		</ul>
	</div>

	<div id="panel" data-slideout-ignore>
		<!--BEGIN header-->
		<header class="header">
			<div class="container">
				<div class="row align-items-center h-100 flex-row-reverse flex-sm-row">
					<div class="col-xl-9 col-md-8 col-6">
						<a href="/" class="logo"><img src="image/logo-white.svg" alt=""></a>
					</div>
					<div class="col-6 d-block d-sm-none">
						<div class="open-menu"><i class="icon icon-menu"></i></div>
					</div>
					<div class="col-xl-3 col-md-4 d-none d-sm-block">
						<nav class="header__menu">
							<ul>
								{{--<li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">Главная</a></li>--}}
								{{--<li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">О нас</a></li>--}}
								<li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">Арендовать</a></li>
								<li class="{{ Request::routeIs('handOver') ? 'active' : '' }}"><a href="{{route('handOver')}}">Поделиться авто</a></li>
								{{--<li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Контакты</a></li>--}}
								@if (Auth::check())
								<li class="menu">
										<a href="#"><i class="icon icon-user"></i></a>
										<div class="menu__hover">
											<a href="/profile">Профиль</a>
											<a href="/logout">Выйти</a>
										</div>
								</li>
								@else
								<li class="menu">
										<a href="#"><i class="icon icon-user"></i></a>
										<div class="menu__hover">
											<a href="{{route('login')}}">Войти</a>
											<a href="{{route('register')}}">Зарегистрироваться</a>
										</div>
								</li>
								@endif
							
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!--END header-->
		<main>
			@yield('content')
		</main>


		<!--BEGIN header-->
		<footer class="footer">
			<div class="footer__top">
				<div class="container">
					<div class="row">
						<div class="col-md-6 d-flex flex-column">
							<h2>О нас</h2>
							<p>Мы создаем современное сообщество, которое делает автомобиль доступным и полезным для каждого участника.</p>
							<p class="last">Москва, пр-д Хлебозаводский, д. 7, стр. 9</p>
						</div>
						<div class="col-md-4 offset-md-2 d-flex flex-wrap justify-content-between">
							<h2 class="w-100">Узнай больше</h2>
							<ul>
								<li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">Главная</a></li>
								<li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">О нас</a></li>
								<li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">Арендовать</a></li>
								<li class="{{ Request::routeIs('handOver') ? 'active' : '' }}"><a href="{{route('handOver')}}">Поделиться авто</a></li>
								<li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Контакты</a></li>
							</ul>
							<ul>
								<li><a href="{{ route('info') }}">Гайд для владельца</a></li>
								<li><a href="#">Гайд для арендатора</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer__bottom">
				<div class="container">
					<div class="row">
						<div class="col d-flex justify-content-between flex-column flex-sm-row">
							<div class="logo"><img src="/image/logo.svg" alt=""></div>
							<div class="coopyright">2019 (c) Россия, Москва</div><a href="/soauto-oferta.pdf">Пользовательское соглашение</a><a target="_blank" href="/politika-konf-soauto.pdf">Политика конфиденциальности</a>
							<div class="social"><a href="https://www.facebook.com/soautoru"><i class="icon icon-facebook"></i></a><a href="https://www.instagram.com/so.auto"><i class="icon icon-instagram"></i></a><a href="https://vk.com/soautoru"><i class="icon icon-vk"></i></a></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--END footer-->
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="{{ asset('js/home.js') }}" defer></script>

</body>

</html>
