@extends('layouts.app')
@section('content')
<div class="container mb-5 checkout" id="checkout">
	<div class="row">
		<div class="col-12 col-lg-7 order-2 order-lg-1" id="checkout-lists">


			@if (Auth::check())
			@if (
			($car['assessed_value'] < 1000000 && $user->getAgeAttribute() >= 21 && $user['driving_experience'] >= 2)
				||
				($car['assessed_value'] >= 1000000 && $user->getAgeAttribute() >= 25 && $user['driving_experience'] >=
				5) || $user['driving_experience'] == ''
				)
				@if (!$user->confirmed)

				@endif
				@if ($user->confirmed)
				<h2 class="about-car__title mt-3">
					Оплати часть поездки перед загрузкой документов
				</h2>
				<ul class="checkout-lists">
					<li class="checkout-list">
						<div class="alert-primary" style="padding: 20px; ">Номер телефона добавлен!</div>

						<div class="title-list" id="сhoose-extra-options">
							{{-- <span class="marker">&#8226;</span> --}}

						</div>
						<div class="container-сhoose-extra-options checkout__block d-block">

							<div class="container-form flex-column">
								@if (count($extras) > 0)
								<p style="font-family: 'Circe 300';">К этому автомобилю владелец предлагает
									дополнительные опции</p>
								<p style="font-family: 'Circe 300';">Посмотри, может тебе пригодятся?</p>
								<br>
								<br>
								<br>

								@foreach ($extras as $item)
										<div class="row">
												<div class="col-lg-12 col-12 mb-2"  style="font-family: 'Druk Wide Medium Cy 500'; font-size: 25px">
													{{$item->extra->name}}
													<p class="extra-desc">{{$item->extra->desc}}</p>
												</div>
											</div>
									<div class="extra_id d-none">{{$item->id}}</div>
<br>
									<div class="row">
										<div class="col-sm-6" data-cost="{{$item->extra_cost}}">
												<span class="conditions"><p style="font-family: 'Circe 300';font-size: 30px">{{$item->extra_cost}} руб@if ($item->time->name == '1 день')
											/сутки</p></span>
											<p id="time{{$item->id}}" style="display: none">1</p>
											@endif
											
										</div>
										<div class="col-sm-6" style="margin-bottom: 20px">
										<button type="submit" data-cost="{{$item->extra_cost}}" data-id="{{$item->id}}" class="checkout-btn extra-btn btn">{{ __('Добавить') }}</button>
										</div>
									</div>
<br>

								@endforeach
								@else
								<p style="font-family: 'Circe 300';">Для данного автомобиля нет дополнительных опций</p>
								<br>

								@endisset

								<h5 style="font-family: 'Circe 300';">Теперь тебе нужно оплатить часть стоимости
									поездки, чтобы забронировать за собой автомобиль в выбранные даты.</h5>

								<h5 style="font-family: 'Circe 300';">После оплаты необходимо будет загрузить фотографии
									документов для завершения бронирования.</h5>
								<br>
								<h5 style="font-family: 'Circe 300';">Приготовь:</h5>

								<h5 style="font-family: 'Circe 300';">- паспорт</h5>

								<h5 style="font-family: 'Circe 300';">- водительское удостоверение</h5>
							</div>
						</div>
					</li>
				</ul>
				@endif
				@else
				<h1 class="about-car__title">К сожалению, вы не можете арендовать этот автомобиль, мы не можем
					застраховать этот автомобиль!</h1>
				@endif
				@else
				<h2 class="about-car__title mt-3">
					Укажи свой номер телефона
				</h2>
				<ul class="checkout-lists first-step">
					<li class="checkout-list">
						{{-- <div class="alert-primary" style="padding: 20px; ">Email добавлен, письмо на почту с паролем должно было уже прийти!</div> --}}



						<div class="title-list" id="confirm-phone">



							<h5 style="font-family: 'Circe 300';">Чтобы продолжить, подтверди свой номер телефона.</h5>
						</div>

						<div class="container-confirm-phone checkout__block d-block">
							<div class="container-form">
								<form method="GET" action="{{route('sendValidateSms')}}" id="sendValidateSms"
									class="validateForm">
									@csrf
									<div class="login-form__pass">
										<input type="text" name="phone" class="login-form__input phone" id="phone"
											placeholder="Введите номер" required>
									</div>
									<p id="numberExist" style="display:none">Этот номер уже существует в системе</p>
									<button type="submit" class="checkout-btn btn">{{ __('Отправить код') }}</button>
								</form>
								<form method="POST" id="validatePhone" action="{{route('validatePhone')}}"
									class="validateForm disabled">
									@csrf
									<div class="login-form__pass">
										<input type="number" name="code" class="login-form__input" id="phone_code"
											placeholder="Введите код" required>
									</div>
									<p></p>
									<button type="submit" class="checkout-btn btn">{{ __('Проверить код') }}</button>
								</form>
								<p class="confirm-text d-none">Ваш номер телефона успешно подтвержден</p>
							</div>
						</div>

						<p style="font-family: 'Circe 300';">Ты выбрал отличный автомобиль!</p>

						<p style="font-family: 'Circe 300';">Заверши бронирование в четыре шага:</p>

						<p style="font-family: 'Circe 300';">1. Подтверди номер телефона</p>

						<p style="font-family: 'Circe 300';">2. Выбери опции и внеси предоплату</p>

						<p style="font-family: 'Circe 300';">3. Предоставь данные о себе для проверки</p>

						<p style="font-family: 'Circe 300';">4. Получи подтверждение брони</p>

						<p style="font-family: 'Circe 300';">И ожидай дня поездки, зная что отличный автомобиль будет
							ждать тебя чистый и заправленный.</p>
						<div class="row" style="margin-bottom: 10px">
							<div class="col-md-6">
								<p class="mt-5" style="margin-bottom:70px">Уже зарегистрирован?</p>
								<div class="checkout-lists d-flex justify-content-between">
									<button type="submit" class="btn login-button ">Войти</button>
									{{-- <a href="{{route('checkout.goToRegister')}}" onclick="ym(53583991, 'reachGoal',
									'register');"
									class="btn login-form__button">Зарегистрироваться</a>--}}
								</div>
							</div>

						</div>



						{{-- @else
						<p class="mb-4">Ваш номер телефона успешно подтвержден</p>
						@endif --}}
					</li>
				</ul>
				{{-- <h1 class="about-car__title mt-3">Чтобы забронировать автомобиль вам необходимо зарегистрироваться или
					войти</h1> --}}

				@endif
		</div>
		<div class="col-12 col-lg-5 order-lg-2">
			<form id="booking-payment" method="POST" action="{{ route('checkout.booking', $car->id) }}">
				<div class="token">
					@csrf
				</div>
				<input type="hidden" name="extras" id="extras">
				<input type="hidden" name="from" id="from"
					value="{{$info_order['DateFrom']}} {{$info_order['TimeFrom']}}">
				<input type="hidden" name="to" id="to" value="{{$info_order['DateTo']}} {{$info_order['TimeTo']}}">
				<input type="hidden" name="index" id="index" value="{{$info_order['index']}}">
				<input type="hidden" name="delivery_place" id="to" value="{{$info_order['delivery_place']}}">
				<div class="container-reservation-details">
					<div class="reservation-details-body">
						<img style="width: 100%; margin-top: 10%;"
							src="https://hb.bizmrg.com/soautomedia/{{ $CarImage }}">
						<h1 style="	font-family: 'Druk Wide Medium Cy 500';letter-spacing: 0.03em;margin-bottom:30px"
							class="mt-3">{{ $car->brand->name }} {{ $car->models_name }}
							{{ $car->year_of_issue }} </h1>
						<ul>
							@isset ($info_order)
							@isset ($car)
							<li>
								<div class="row">
									<div class="col-sm-5" style="text-align: center;font-size:14px"><span
											class="conditions">
											{{$info_order['DateFrom']}} {{$info_order['TimeFrom']}}
										</span>
									</div>
									<div class="col-sm-1" style="margin:auto;text-align: center;"><span
											class="conditions">-</span></div>
									<div class="col-sm-5" style="text-align: center;font-size:14px">
										<span class="conditions">
											{{$info_order['DateTo']}} {{$info_order['TimeTo']}}
										</span>
									</div>
								</div>
							</li>
							<li style="background-color: gainsboro;font-size: 20px;padding:5px">
								<span class="conditions">Аренда за сутки</span>
								<span class="value">{{ $info_order['CostOneDay'] }} ₽</span>
							</li>
							<br>
							{{-- <li>
								<span class="conditions">Кол-во дней</span>
								<span class="value">{{$info_order['number_days']}}</span>
							<input type="hidden" name="count_day" value="{{$info_order['number_days']}}">
							</li> --}}
							@if (isset($rentPlace->name) && $rentPlace !== [])
							<li style="display: flex; flex-direction: column;font-size: 20px;">
								<span class="conditions">Место получения и возврата:</span>
								<span class="value">{{$rentPlace->name}}</span>
							</li>
							@else
							<li style="display: flex; flex-direction: column;font-size: 16px;">
								<span class="conditions">Место получения и возврата:</span>
								<span class="value">{{$car->location}}</span>
							</li>
							@endif

							<li style="font-size: 16px;">
								<span class="conditions">Включенный пробег</span>
								<span class="value">{{$info_order['included_distance']}} км</span>
								<input type="hidden" name="mileage" value="{{$info_order['included_distance']}}">
							</li>
							<br>
							<li style="font-size: 18px;">
								<strong><span class="conditions">Общая стоимость поездки</span>
									<span class="value order-sum"><span
											id="rentSum">{{session('RentSum')}}</span>₽</span></strong>
							</li>
							<li style="font-size: 16px;">
								<span class="conditions">+ Возвращаемый залог</span>
								<span class="value" id="deposit">{{$info_order['deposit']}} ₽</span>
								<input type="hidden" name="deposit" value="{{$info_order['deposit']}}">
							</li>
							<li style="font-size: 14px;">
								<span>Количество суток</span>
								<span class="value order-sum"><span id="number_days">{{$info_order['number_days']}}</span>
									суток</span>
							</li>
							<li style="font-size: 14px;">
								<span>Аренда</span>
								<span class="value order-sum"><span
										id="cost_day">{{$info_order['cost_day']}}</span>₽/сутки</span>
							</li>

							@if ($info_order['kaskoCost'])
							<li style="font-size: 14px;">
								<span>КАСКО</span>
								<span class="value" id="deposit">{{$info_order['kaskoCostDay']}} ₽</span>
							</li>
							@endif

							<li style="font-size: 14px;">
								<span>Комиссия</span>
								<span class="value order-sum"><span>0 ₽</span></span>
							</li>
							@if ($info_order['delivery_cost'])
							<li style="font-size: 14px;">
								<span>Доставка</span>
								<span class="value" id="delivery_cost">{{$info_order['delivery_cost']}} ₽</span>
							</li>
							@endif


							{{-- <li>
								<span class="conditions">Доставка</span>
								@if (isset($info_order['delivery_cost']) && $info_order['delivery_cost'] !== [])
								<span class="value">{{$info_order['delivery_cost']}} ₽</span>
							<input type="hidden" name="delivery_cost" id="delivery_cost"
								value="{{session('deliveryCost')}}">
							@else
							<span class="value">0</span>
							@endif
							</li>

							<li>
								<span class="conditions">Стоимость КАСКО</span>
								<span class="value order-sum">{{session('kaskoCost')}} ₽</span>
							</li>

							<li>
								<strong><span class="conditions">Сумма итого</span>
									<span class="value order-sum"><span id="totalSum">{{session('sum')}}</span>
										₽</span></strong>
							</li> --}}
							<br>
							<li style="font-size: 20px;">
								<strong><span class="conditions">К оплате сейчас</span>
									<span class="value order-sum"><span id="bookingSum">{{session('bookingSum')}}</span>
										₽</span></strong>
							</li>

							<li style="font-size: 14px;">
								<strong><span class="conditions">При получении авто</span>
									<span class="value order-sum"><span
											id="КesidualAmount">{{session('КesidualAmount')}}</span> ₽</span></strong>
							</li>

							@endisset
							@endisset
						</ul>
						<br>
						<div>

							<img style="width: 40%" src="/image/renessans.png">
							<img style="width: 40%;float: right;" src="/image/alfa-bank.png">
						</div>

						<input id="bluetooth" type="checkbox" name="bluetooth" value="1" class="check" checked="">


						<div class="buttons btn-finish">
							@if (Auth::check() && $user->confirmed)
							@if (
							($car['assessed_value'] < 1000000 && $user->getAgeAttribute() >= 21 &&
								$user['driving_experience'] >= 2)
								||
								($car['assessed_value'] >= 1000000 && $user->getAgeAttribute() >= 25 &&
								$user['driving_experience'] >= 5) || $user['driving_experience'] == ''
								)

								<button type="submit" class="btn filter-btn btn-finish" id="ao-button-checkout">
									Оплатить
								</button>



								@endif
								@endif

						</div>

					</div>



				</div>
				<input type="text" name="car[]" value="{{ json_encode($car[0]) }}" hidden>
				<input type="text" name="info_order[]" value="{{ json_encode($info_order) }}" hidden>

			</form>

		</div>
	</div>
</div>



<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Авторизация</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" id="checkout-login" action="{{ route('login') }}" aria-label="{{ __('Login') }}"
					class="login-form">
					@csrf
					<div class="login-form__adress">
						<p class="login-form__text">{{ __('Введите адрес эл. почты') }}</p>
						<input id="#" type="email"
							class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
							placeholder="Введи E-mail" required value="{{ old('email') }}">

						@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif

					</div>
					<div class="login-form__pass">
						<p class="login-form__text">{{ __('Введите пароль') }}</p>
						<input id="password" type="password"
							class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
							minlength="6" placeholder="Введите пароль" required>
						<div id="show-pass" onclick="showPass('password')"></div>
						@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif

						<div class="checkbox-login">
							<input id="check" type="checkbox" name="remember" value="check"
								{{ old('remember') ? 'checked' : '' }} class="check">
							<label for="check">{{ __('запомнить меня') }}</label>

							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif

						</div>
					</div>
					<button type="submit" class="btn login-form__button">{{ __('Войти') }}</button>
					<a href="{{ route('password.request') }}" class="login-form__link">{{ __('Забыл пароль') }}</a>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Cоглашение</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Пожалуйста, подтвердите соглашение на снятие денежных средств</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0 yes"
					data-dismiss="modal">Согласен(а)</button>
				<button type="button" class="btn btn-close creating-cars-btn mt-0 no" data-dismiss="modal">Не,
					согласен(а)</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="not" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="loading"><img src="/image/loading.gif" alt=""></div>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ошибка</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>К сожалению, вы не можете арендовать этот автомобиль, мы не можешь его застраховать Вас и этот
					автомобиль!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close creating-cars-btn mt-0"
					data-dismiss="modal">Понял(а)</button>
			</div>
		</div>
	</div>
</div>
<script>
	function inputFileUpload(nameInput) {

			var fileInput  = document.querySelector(".input-file-" + nameInput),  
					button     = document.querySelector(".input-file-trigger-" + nameInput),
					the_return = document.querySelector(".file-return-" + nameInput);
					
			the_return.innerHTML = fileInput.value.split('fakepath\\')[1]; // Разделить по 'fakepath\', взять вторую часть

	}

$(document).ready(function() {

	document.querySelector("html").classList.add('js');

	var car_id = '<?php echo $car->id; ?>';
	let isCode = false;

	$('body').on('click', '.checkout-title', function(){
			$(this).closest('.checkout-list').find('.checkout__block').toggleClass('d-block');
	});
});
</script>
@endsection