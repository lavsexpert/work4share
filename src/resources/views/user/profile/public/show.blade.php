@extends('layouts.home')

@section('content')
    <section class="user">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="user-photo">
                                <img src="{{ url('samples/user-ava.png') }}" alt="User Avatar" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6"></div>
            </div>
        </div>
    </section>
@endsection
