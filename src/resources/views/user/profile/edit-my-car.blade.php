@extends('layouts.app')
@section('content')

<section class="about-car edit-car">
  <div class="container">
    <div class="row">
      <div class="col">
          <a href="/profile/mycars/{{$car->id}}" class="content-card1__link back_to_cars">
            К авто
          </a>
          <h1 class="about-car__title">
            Редактирование {{$car->brand->name}} {{$car->models_name}}
          </h1>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">Общая информация</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Фото</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Условия доставки</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="deposit-tab" data-toggle="tab" href="#deposit" role="tab" aria-controls="deposit" aria-selected="false">Условия аренды</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="type-tab" data-toggle="tab" href="#type" role="tab" aria-controls="type" aria-selected="false">Тип подключения</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="false">Услуги</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="insurance-tab" data-toggle="tab" href="#insurance" role="tab"
                 aria-controls="insurance" aria-selected="false">ОСАГО и КАСКО</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="ban-tab" data-toggle="tab" href="#ban" role="tab" aria-controls="ban" aria-selected="false">Запрещено делать в авто</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade all show active" id="all" role="tabpanel" aria-labelledby="all-tab">
              {{--<form id="editBrand" method="POST" action="{{ route('car.brand', $car->id) }}">--}}
              {{--@csrf--}}
              {{--<input type="hidden" name="cars_id" value="{{$car->id}}">--}}
              {{--<div class="d-flex justify-content-between">--}}
              {{--<div class="form-production-date description_text">--}}
              {{--<h2 class="form-production-date__title">--}}
              {{--Брэнд авто--}}
              {{--</h2>--}}
              {{--<select id="brand_id" name="brand_id" class="form-cars__select">--}}
              {{--<option disabled selected>Ничего не выбрано</option>--}}
              {{--@foreach (\App\Models\BrandsCar::all() as $item)--}}
              {{--<option--}}
              {{--@if ($car['brand_id'] == $item['id'])--}}
              {{--selected--}}
              {{--@endif--}}
              {{--value="{{ $item['id'] }}">--}}
              {{--{{ $item['name'] }}--}}
              {{--</option>--}}
              {{--@endforeach--}}
              {{--</select>--}}
              {{--</div>--}}
              {{--<button type="submit" class="btn card-list__btn">--}}
              {{--Сохранить--}}
              {{--</button>--}}
              {{--</div>--}}
              {{--</form>--}}
              <form id="editModel" method="POST" action="{{ route('car.model', $car->id) }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Модель авто
                    </h2>
                    <select id="model_id" name="model_id" class="form-cars__select">
                      <option disabled selected>Ничего не выбрано</option>
                      @foreach (App\Models\ModelsCar::where('brand_id',$car['brand_id'])->get() as $item)

                        <option data-other="{{$item->is_other}}"
                                @if ($car['model_id'] == $item['id'])
                                selected
                                @endif
                                value="{{ $item['id'] }}">
                          {{ $item['name'] }}

                        </option>
                      @endforeach
                    </select>
                    @php
                      $model = \App\Models\ModelsCar::find($car['model_id'])
                    @endphp
                    <div class="row mt-3" id="new-model-name"
                         style="@if(!$model or !$model->is_other) display: none; @endif">
                      <div class="col-sm-12">

                        <h2 class="form-production-date__title">
                          Другое название модели
                        </h2>
                        <input class="login-form__input" name="model_name" value="{{$car->model_name ?? 'Другая'}}"
                               required>
                      </div>

                    </div>

                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editGos" method="POST" action="{{ route('car.editGos', $car->id) }}">
                  @csrf
                  <input type="hidden" name="cars_id" value="{{$car->id}}">
                  <div class="d-flex justify-content-between">
                    <div class="form-production-date description_text">
                      <h2 class="form-production-date__title">
                        Гос.номер
                      </h2>
                      <textarea type="number" class="login-form__input" name="gos_nomer" value="{{$car->gos_nomer}}" required>{{$car->gos_nomer}}</textarea>
                    </div>
                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>
                  </div>
                </form>
              <form id="editDescription" method="POST" action="{{ route('car.description') }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Описание авто
                    </h2>
                    <textarea type="number" class="login-form__input" name="description" value="{{$car->description['description']}}" required>{{$car->description['description']}}</textarea>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editCity" method="POST" action="{{ route('car.editCity', $car->id) }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Редактировать город авто
                    </h2>
                    <select name="city" id="city" class="form-cars__select">
                    @foreach ($city as $item)
                      <option 
                        @if ($car['city'] == $item['id'])
                          selected
                        @endif
                      value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                  </select>   
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editCarType" method="POST" action="{{ route('car.editCarType', $car->id) }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Редактировать тип авто
                    </h2>
                    <select name="cartype" id="cartype" class="form-cars__select">
                    @foreach ($carType as $item)
                      <option 
                        @if ($car['car_type'] == $item['id'])
                          selected
                        @endif
                      value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                  </select>   
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editCostDay" method="POST" action="{{ route('car.editCostDay', $car->id) }}">
                @csrf
                <div class="d-flex justify-content-between">
                  <div class="form-production-date">
                    <h2 class="form-production-date__title">
                      Цена за сутки
                    </h2>
                    <input type="number" class="login-form__input" name="cost_day" value="{{$car->cost_day}}" required>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editGear" method="POST" action="{{ route('car.gear', $car->id) }}">
                  @csrf
                  <input type="hidden" name="cars_id" value="{{$car->id}}">
                  <div class="d-flex justify-content-between">
                    <div class="form-production-date description_text">
                      <h2 class="form-production-date__title">
                        Тип коробки передач
                      </h2>
                      <select id="gear_id" name="gear_id" class="form-cars__select">
                        <option disabled selected>Ничего не выбрано</option>
                          @foreach ($gear as $item)
                              <option
                              @if ($car['gear_id'] == $item['id'])
                                  selected
                              @endif
                              value="{{ $item['id'] }}">
                                  {{ $item['name'] }}
                              </option>
                          @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>
                  </div>
                </form>
              <form id="editTransmission" method="POST" action="{{ route('car.transmission', $car->id) }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Тип привода
                    </h2>
                    <select id="transmission_id" name="transmission_id" class="form-cars__select">
                      <option disabled selected>Ничего не выбрано</option>
                      @foreach (\App\Models\CarTransmission::all() as $item)
                        <option
                                @if ($car['transmission_id'] == $item['id'])
                                selected
                                @endif
                                value="{{ $item['id'] }}">
                          {{ $item['name'] }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editTransmission" method="POST" action="{{ route('car.fuel', $car->id) }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{$car->id}}">
                <div class="d-flex justify-content-between">
                  <div class="form-production-date description_text">
                    <h2 class="form-production-date__title">
                      Тип топлива
                    </h2>
                    <select id="transmission_id" name="fuel_type_id" class="form-cars__select">
                      <option disabled selected>Ничего не выбрано</option>
                      @foreach (\App\Models\FuelType::all() as $item)
                        <option
                                @if ($car['fuel_type_id'] == $item['id'])
                                selected
                                @endif
                                value="{{ $item['id'] }}">
                          {{ $item['name'] }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
                <form id="editPower" method="POST" action="{{ route('car.editPower', $car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                        <div class="form-production-date">
                            <h2 class="form-production-date__title">
                                Мощность двигателя (л/с)
                            </h2>
                            <input type="number" class="login-form__input" name="power" value="{{$car->power}}"
                                   required>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                            Сохранить
                        </button>
                    </div>
                </form>
              <form id="editAssessedValue" method="POST" action="{{ route('car.editCarPrice', $car->id) }}">
                @csrf
                <div class="d-flex justify-content-between">
                  <div class="form-production-date">
                    <h2 class="form-production-date__title">
                      Рыночная стоимость автомобиля
                    </h2>
                    <input type="number" class="login-form__input" name="assessed_value" value="{{$car->assessed_value}}"
                           required>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
              <form id="editSeats" method="POST" action="{{ route('car.editSeats', $car->id) }}">
                @csrf
                <div class="d-flex justify-content-between">
                  <div class="form-production-date">
                    <h2 class="form-production-date__title">
                      Количество пассажиров
                    </h2>
                    <input type="number" class="login-form__input" name="seat" value="{{$car->seat}}"
                           required>
                  </div>
                  <button type="submit" class="btn card-list__btn">
                    Сохранить
                  </button>
                </div>
              </form>
                <form id="editVolume" method="POST" action="{{ route('car.editVolume', $car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                        <div class="form-production-date">
                            <h2 class="form-production-date__title">
                                Объем двигателя (л)
                            </h2>
                            <input type="number" class="login-form__input" step="0.1" name="volume" value="{{$car->engine_volume}}"
                                   required>
                        </div>
                        <button type="submit" class="btn card-list__btn">
                            Сохранить
                        </button>
                    </div>
                </form>
              <form id="editMileageInclusive" method="POST" action="{{ route('car.editMileageInclusive', $car->id) }}">
                  @csrf
                  <div class="d-flex justify-content-between">
                    <div class="number-dor">
                      <h2 class="form-production-date__title">
                        Укажите включенный пробег в сутки:
                      </h2>
                      <input type="number" class="login-form__input" name="day" placeholder="км" value="{{$car->mileageInclusive['day']}}" required>
                    </div>
                    <div class="number-dor">
                      <h2 class="form-production-date__title">
                        Укажите включенный пробег в неделю:
                      </h2>
                      <input type="number" class="login-form__input" name="week" placeholder="км" value="{{$car->mileageInclusive['week']}}" required>
                    </div>
                    <div class="number-dor">
                      <h2 class="form-production-date__title">
                        Укажите включенный пробег в месяц:
                      </h2>
                      <input type="number" class="login-form__input" name="month" placeholder="км" value="{{$car->mileageInclusive['month']}}"  required>
                    </div>
                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>
                  </div>
              </form>
              <form id="editRentTime" method="POST" action="{{ route('car.editRentTime', $car->id) }}">
                  @csrf
                  <div class="d-flex justify-content-between">
                    <div class="number-dor">
                      <h2 class="form-production-date__title">
                        Минимальное время аренды
                      </h2>
                      <input type="number" class="login-form__input" name="min_rent_time" placeholder="укажите время в днях" value="{{$car->rentTime['min_rent_time'] / 86400}}"  required>
                    </div>
                    <div class="number-dor">
                      <h2 class="form-production-date__title">
                        Максимально время аренды
                      </h2>
                      <input type="number" class="login-form__input" name="max_rent_time" placeholder="укажите время в днях" value="{{$car->rentTime['max_rent_time'] / 86400}}"  required>
                    </div>
                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>
                  </div>
              </form>
              <div class="row">
                <div class="col-sm-12">
                  <form id="editCoordinates" method="POST" action="{{ route("car.editCoordinates",$car->id) }}">
                    @csrf
                    <div class="d-flex justify-content-between">
                      <div class="form-production-date">
                        <h2 class="form-production-date__title">
                          Местоположение машины
                        </h2>
                        <input type="hidden" id="car_lat" name="lat" value="{{$car->lat}}">
                        <input type="hidden" id="car_lng" name="lng" value="{{$car->lng}}">
                      </div>
                      <button type="submit" class="btn card-list__btn">
                        Сохранить
                      </button>
                    </div>
                  </form>
                </div>
                <div class="col-sm-12" id="map" style="height: 300px">

                </div>
              </div>


            </div>
            <div class="tab-pane fade show" id="home" role="tabpanel" aria-labelledby="home-tab">
              <ul class="about-car__list">
                  @foreach ($images as $image)
                  @if ($image->avatar == 1)
                    <li class="hover">
                      <img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" alt="">
                      
                      <form method="POST" action="{{ route('profile.deleteImage', $car->id) }}" class="delete deletePhoto">
                        @csrf
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">
                            <img src="/image/delete.svg" alt="">
                        </button>
                      </form>
                      {{-- <form method="POST" action="{{ route('profile.setHead', $car->id) }}" class="primary saveHeadImageId">
                        @csrf
                        <input type="hidden" name="car_id" value="{{$car->id}}">
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">Сделать главной</button>
                      </form> --}}
                    </li>
                    @break
                    @endif
                  @endforeach
      
                  @foreach ($images as $image)
                    @if ($image->avatar == 1)
                      @continue
                    @endif
                    <li>
                      <img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" alt="">
                      <form method="POST" action="{{ route('profile.deleteImage', $car->id) }}" class="delete deletePhoto">
                        @csrf
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">
                            <img src="/image/delete.svg" alt="">
                        </button>
                      </form>
                      <form method="POST" action="{{ route('profile.setHead', $car->id) }}" class="primary saveHeadImageId">
                        @csrf
                        <input type="hidden" name="car_id" value="{{$car->id}}">
                        <input type="hidden" name="image_id" value="{{$image->id}}">
                        <button type="submit">Сделать главной</button>
                      </form>
                    </li>
                  
                @endforeach
                
              </ul>
      
              <input type="file" id="editPhoto" name="photo" multiple>
            </div>
            <div class="tab-pane fade conditionDelivery" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              @foreach ($delivery_cars as $item)
              <div class="card card-default active">
                <div class="card-header">
                  <h2>Новое условие доставки</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editConditionDelivery', $car->id) }}" class="editConditionsDelivery d-flex justify-content-between">
                    @csrf
                    <input type="hidden" name="car_id" value="{{$car->id}}">
                    <input type="hidden" class="data_id" name="data_id" value="{{$item->id}}">
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        Укажите место доставки
                      </h2>
                      <select name="place" class="form-number-persons__select" required>
                        <option class="form-number-persons__option" value="" selected disabled>
                          Ничего не выбрано
                        </option>
                        @foreach ($rent_place as $place)
                        <option class="form-number-persons__option" value="{{$place->id}}"
                          @if ($item->place == $place->id)
                          selected
                          @endif
                          >
                          {{$place->name}}
                        </option>
                        @endforeach
                        
                      </select>
                    </div>
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        Укажите цену доставки
                      </h2>
                      <input type="number" class="login-form__input" name="cost" placeholder="Введите цену" required value="{{$item->cost}}">
                    </div>
                    
                    <button type="submit" class="btn card-list__btn">
                      Редактировать
                    </button>
                  </form>
                </div>
              </div>
              @endforeach
              <div class="card card-default">
                <div class="card-header">
                  <h2>Новое условие доставки</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editConditionDelivery', $car->id) }}" class="editConditionsDelivery d-flex justify-content-between">
                    @csrf
                    <input type="hidden" name="car_id" value="{{$car->id}}">
                    <input type="hidden" class="data_id" name="data_id" value="0">
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        Укажите место доставки
                      </h2>
                      <div class="response_place">
                      </div>
                      <select name="place" class="form-number-persons__select" required>
                        <option class="form-number-persons__option" value="" selected disabled>
                          Ничего не выбрано
                        </option>
                        @foreach ($rent_place as $place)
                        <option class="form-number-persons__option" value="{{$place->id}}">
                          {{$place->name}}
                        </option>
                        @endforeach
                        
                      </select>
                    </div>
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        Укажите цену доставки
                      </h2>
                      <div class="response_cost">
                      </div>
                      <input type="number" class="login-form__input" name="cost" placeholder="Введите цену" required>
                    </div>
                    
                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>
                    
                  </form>
                </div>
              </div>
             
            </div>
            <div class="tab-pane fade" id="deposit" role="tabpanel" aria-labelledby="deposit-tab">
              <div class="card card-default">
                <div class="card-header">
                  <h2>Депозит</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('car.editDeposit', $car->id) }}" id="editDeposit" class="d-flex justify-content-between">
                    @csrf
                    <div class="form-production-date">
                      <h2 class="form-production-date__title">
                        Укажите размер депозита
                      </h2>
                      <div class="response_cost">
                      </div>
                      <input type="number" class="login-form__input" name="price" value="{{ $car->carsDeposit->price ?? 0 }}" placeholder="Введите цену" required>
                    </div>

                    <button type="submit" class="btn card-list__btn">
                      Сохранить
                    </button>

                  </form>
                </div>
              </div>

            </div>
            <div class="tab-pane fade" id="type" role="tabpanel" aria-labelledby="type-tab">
              <form id="editType" class="d-flex justify-content-between align-items-center" method="POST" action="{{ route('car.type', $car->id) }}">
                @csrf
                <div class="group-ridio" id="connection_type">
                  @foreach ($unlock_type as $type)
                      <input name="type" class="form-check-label" type="radio" value="{{ $type->id }}"
                      @if($type->check) checked @endif
                      id="{{ $type->id }}">
                      <label for="{{ $type->id }}">{{ $type->name }}</label>
                    @endforeach
                </div>
                <button type="submit" class="btn card-list__btn">
                  Сохранить
                </button>
              </form>
            </div>
            <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services-tab">
              <div class="table">
                  <div class="tr">
                    <div class="table_tr">Название услуги</div>
                    <div class="table_tr">На какое время</div>
                    <div class="table_tr">Стоимость</div>
                    <div class="table_tr"></div>
                  </div>
                  @foreach ($extra as $item)
                  
                  <form method="POST" id="editExtra-{{$item->id}}" class="tr editExtra" action="{{ route('car.extra') }}">
                    @csrf
                    @php
                     $car_extra = null;   
                    @endphp
                    @php
                      foreach ($extra_cars as $item_cars) {
                        if ($item->id == $item_cars['extra_id']) {
                          $car_extra = $item_cars;
                        }
                      }
                    @endphp
                    


                    
                    <input type="hidden" name="active" value="1">
                    <input type="hidden" name="cars_id" value="{{$car->id}}">
                    <input type="hidden" name="extra_id" value="{{$item->id}}">
                    <input type="hidden" name="extra_cars" value="{{$car_extra['id']}}">

                    <div class="th" scope="row">{{$item->name}}</div>
                    
                      <div class="td">
                        <select name="extra_time_id" class="form-number-persons__select" required>
                          <option class="form-number-persons__option" value="">
                            Ничего не выбрано
                          </option>
                          @foreach($extra_time as $item_time)
                            
                            <option value="{{$item_time->id}}" 
                                @if ($item_time->id == $car_extra['extra_time_id'])
                                    selected
                                @endif
                            > 
                              {{$item_time->name}}
                            </option>
                          @endforeach
                        </select>
                      </div>
                      <div class="td">
                        <input type="text" class="login-form__input" name="extra_cost" placeholder="укажите цену" required value="{{$car_extra['extra_cost']}}">
                      </div>
                    <div class="td">
                      @if ($car_extra['active'])
                        <button type="submit" class="btn card-list__btn">
                          Отменить
                        </button>
                      @else
                        <button type="submit" class="btn card-list__btn">
                          Добавить
                        </button>
                      @endif
                      
                    </div>
                  </form>
                  @endforeach
                  
                  
              </div>
            </div>
            <div class="tab-pane fade" id="insurance" role="tabpanel" aria-labelledby="insurance-tab">
              <div class="card card-default">
                <div class="card-header">
                  <h2>ОСАГО И КАСКО</h2>
                </div>
                <div class="card-body">

                  <ul class="card-list">
                    <li class="card__osago card__osago_id card-list__item d-flex">
              
                      <h2 class="changing-card-title">Дата окончания полиса ОСАГО</h2>
                      <form method="POST" action="{{route('car.editOsagoKaskoDate', $car->id)}}" class="editOsagoKaskoDate flex w-100 mb-4 align-items-center">
                        @csrf
                        <input type="date" name="osago_date" class="input" value="{{$car->osago_date}}">
                        <button class="btn card-list__btn ml-4"  type="submit">сохранить</button>
                      </form>
                      <h2 class="changing-card-title">Фотография полиса ОСАГО</h2>
                      @if ($osagoImage)
                        <img src="{{$osagoImage}}" alt="" style="height: 300px">
                      @else
                        <div class="card__osago__block">
                          <div class="number-dor">
                            <h2 class="number-dor__title">
                              Количество водителей в ОСАГО?
                            </h2>
                            <div class="group-ridio">
                              <form method="POST" action="{{route('car.osago', $car->id)}}">
                                @csrf
                                <input type="radio" @if(!$car->osago_has) checked @endif id="osago_has_1" name="osago_has" value="0" required="">
                                <label for="osago_has_1">Ограничено</label>
                                <input type="radio" @if($car->osago_has) checked @endif  id="osago_has_2" name="osago_has" value="1">
                                <label for="osago_has_2">Не ограничено</label>
                                <button type="submit" class="btn card-list__btn d-none">
                                  Сохранить
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="card__osago__block osago__need" style="opacity: 0">
                          <div class="card-list__Upload d-none">
                            <input type="file" id="osago" name="photo">
                          </div>
                        </div>
                      @endif
                      
                    </li>

                    <li class="card__kasko card__osago card-list__item d-flex">
                      <h2 class="changing-card-title">Дата окончания полиса КАСКО</h2>
                      <form method="POST" action="{{route('car.editOsagoKaskoDate', $car->id)}}" class="editOsagoKaskoDate flex w-100 mb-4 align-items-center">
                        @csrf
                        <input type="date" name="kasko_date" class="input" value="{{$car->kasko_date}}">
                        <button class="btn card-list__btn ml-4"  type="submit">сохранить</button>
                      </form>
                      <h2 class="changing-card-title">Фотография полиса КАСКО</h2>
                      @if ($kaskoImage)
                        <img src="{{$kaskoImage}}" alt="" style="height: 300px">
                      @else
                        <div class="card__osago__block">
                          <div class="number-dor">
                            <h2 class="number-dor__title">
                              Есть ли у Вас полис КАСКО?
                            </h2>
                            <div class="group-ridio">
                              <form method="POST" action="{{route('car.kasko', $car->id)}}">
                                @csrf
                                <input type="radio" id="kasko_has_1" @if(!$car->kasko_has) checked @endif name="kasko_has" value="1">
                                <label for="kasko_has_1">Да</label>
                                <input type="radio" id="kasko_has_2" @if($car->kasko_has) checked @endif name="kasko_has" value="0">
                                <label for="kasko_has_2">Нет</label>
                                <button type="submit" class="btn card-list__btn d-none">
                                  Сохранить
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="card__osago__block kasko__need" style="opacity: 0">
                          <div class="card-list__Upload">
                            <input type="file" id="kasko" name="photo">
                          </div>
                        </div>
                      @endif
                    </li>
                  </ul>

                </div>
              </div>

            </div>
            <div class="tab-pane fade" id="ban" role="tabpanel" aria-labelledby="ban-tab">
              <form id="editBan" class="ban" method="POST" action="{{ route('car.ban') }}">
                @csrf
                <input type="hidden" name="cars_id" value="{{ $car->id }}">

                @foreach ($cars_bans as $item)
                  
                  <div class="checkbox">
                    <input id="{{$item->id}}" type="checkbox" name="{{$item->id}}" value="1" class="check"
                    @foreach ($cars_bans as $item_cars)
                      @if ($item->active)
                        checked
                      @endif    
                    @endforeach
                    
                    >
                    <label for="{{$item->id}}">
                      {{$item->bans['name']}}
                    </label>
                  </div>    
                @endforeach
                <button class="btn card-list__btn" type="submit">отправить</button>
              </form>
            </div>

          </div>
        
        
      </div>
    </div>
  </div>

</section>

<script>
  var car_id = parseInt("{{$car->id}}");
</script>


@endsection

@section('bottom_scripts')
  <script>
    let map, center, marker;
    let $carLat = $('#car_lat');
    let $carLng = $('#car_lng');
    center = {lat: {{$lat}}, lng: {{$lng}} };
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: center,
        zoom: 18
      });
      marker = new google.maps.Marker({position: center, map: map, draggable: true});
      marker.addListener('dragend', (event) => {
        $carLat.val(event.latLng.lat());
        $carLng.val(event.latLng.lng());
      });
      @if($empty)
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          console.log(position);
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $carLat.val(position.coords.latitude);
          $carLng.val(position.coords.longitude);

          map.setCenter(pos);
          var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          marker.setPosition(latlng);
          console.log('your address  found')
        }, function () {
          console.log('your address not found')
        });
      } else {
        console.log('you dont navigator')
      }
      @endif
    }

  </script>
  <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQP9BGaoXYrBjGiDpzMd_wxEkX-CyLA-o&callback=initMap">
  </script>
@endsection