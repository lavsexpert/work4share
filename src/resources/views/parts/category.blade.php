<section class="category">
    <div class="container">
        <div class="row">
            <div class="col d-flex flex-wrap">
                <a href="{{route('car.catalog.index')}}" class="category__block black">
                    <h3>#поделам</h3>
                    <p>бизнес и премиум<br>{{trans('car')}}</p><img src="/image/category/black.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block yellow">
                    <h3>#просто</h3>
                    <p>удобные <br>и экономичные</p><img src="/image/category/yellow.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block white">
                    <h3>#длягруза</h3>
                    <p>вместительные</p><img src="/image/category/white.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block red">
                    <h3>#настиле</h3>
                    <p>спорт кары <br>и лакшери авто</p><img src="/image/category/red.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block blue">
                    <h3>#наотдых</h3>
                    <p>отправиться <br>в путешествие</p><img src="/image/category/blue.png" alt="">
                </a>
                <a href="{{route('car.catalog.index')}}" class="category__block grey">
                    <h3>#сдрузьями</h3>
                    <p>от 6 мест</p><img src="/image/category/grey.png" alt="">
                </a>
            </div>
        </div>
    </div>
</section>