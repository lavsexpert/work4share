import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    url: `${window.location.origin}/api`,
    cars: [],
    loading: false
  },
  mutations: {
    setCars: (state, payload) => {
      state.cars = payload;
    },
    setLoading: (state, payload) => {
      state.loading = payload;
    },
  },
  actions: {
    setCars: async (context, payload) => {
      context.commit('setCars', payload);
    },
    getCars: async (context, payload) => {
      context.commit('setCars', payload);
    },
    setLoading: async (context, payload) => {
      context.commit('setLoading', payload);
    },
    
  },
  getters: {
    getUrl: state => {
      return state.url
    },
    getCars: state => {
      return state.cars
    },
    getLoading: state => {
      return state.loading
    }
  }
})