$('body').on('submit','#filter',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    $('.car').load(`.car > *`);
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    
  }).fail(function(err) {   
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};
if ($('#dateTo').length) {
  let dateFrom = $('#dateFrom').datepicker({
    minDate: new Date(),
    onSelect: function (formattedDate, date, inst) {
      dateTo.update('minDate', date.addDays(1))
    }
  }).data('datepicker');
}
if ($('#dateTo').length) {
  let dateTo = $('#dateTo').datepicker({
    minDate: dateFrom.lastSelectedDate.addDays(1),
    onSelect: function (formattedDate, date, inst) {
      console.log(formattedDate)
    }
  }).data('datepicker');
}

// let dateFrom = $('#dateFrom').datepicker({
//   minDate: new Date(),
//   onSelect: function (formattedDate, date, inst) {
//     dateTo.update('minDate', date.addDays(1))
//   }
// }).data('datepicker');

// let dateTo = $('#dateTo').datepicker({
//   minDate: dateFrom.lastSelectedDate.addDays(1),
//   onSelect: function (formattedDate, date, inst) {
//     console.log(formattedDate)
//   }
// }).data('datepicker');

