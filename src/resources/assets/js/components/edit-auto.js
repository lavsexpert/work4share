$('body').on('submit','#editType',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit','#editLocation',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    document.location.reload(true);
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit','#editBan',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    alert('Ваши данные сохранены');
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit', '#editDeposit', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function (res) {
        console.log(res);
        setTimeout(function () {
            loading.removeClass('active')
        }, 1000);
        alert('Ваши данные сохранены');
    }).fail(function (err) {
        loading.removeClass('active');
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
});



$('body').on('submit','.editExtra',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    form.attr('id')
    form.load(` #${form.prop('id')} > *`)
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    
  }).fail(function(err) {   
    console.log(err.responseJSON);
    
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit','#editDescription',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    form.attr('id')
    form.load(` #${form.prop('id')} > *`)
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
    
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});


$('body').on('submit', '.deletePhoto', function(event){
  console.log(event);
  
  event.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    $('.about-car__list').load(` .about-car__list > *`)
    loading.removeClass('active')
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit', '#editRentCar', function(e){
  
  e.preventDefault();
  const form = $(this);
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    alert(res.message);
    document.location.reload(true);
    
  }).fail(function(err) {   
    loading.removeClass('active')
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});


$('body').on('submit', '#deleteRentCar', function(e){
  e.preventDefault();
  const form = $(this);
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);
    alert(res.message);
    document.location.reload(true);
    
  }).fail(function(err) { 
    loading.removeClass('active')  
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

//Общая информация
  $('#editCostDay').submit(function(e){
    e.preventDefault();
    const form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
      console.log(res);
      alert('Вы успешно изменили цену за день');
    }).fail(function(err) { 
      loading.removeClass('active')  
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
  });


  $('#editMileageInclusive').submit(function(e){
    e.preventDefault();
    const form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
      console.log(res);
      alert('Вы успешно изменили данные');
    }).fail(function(err) {  
      loading.removeClass('active') 
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
  });

  $('#editRentTime').submit(function(e){
    e.preventDefault();
    const form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
      console.log(res);
      alert('Вы успешно изменили данные');
    }).fail(function(err) {   
      loading.removeClass('active')
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
  });

$('body').on('submit','.editConditionsDelivery',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
      console.log(res);

      form.attr('id')
      $('.conditionDelivery').load(` .conditionDelivery > *`)
      setTimeout(function(){
        loading.removeClass('active')
      }, 1000)
    }).fail(function(err) {  
      loading.removeClass('active') 
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
});

$('body').on('change','#model_id',function(){
    console.log('changed model');
    let $newModelName = $('#new-model-name');
    let isOther = $(this).children(':selected').data('other');
    if (isOther) {
        console.log('is other');
        $newModelName.show();
    } else {
        console.log('not is other');
        $newModelName.hide();
    }
});


$('body').on('submit','#editCarType',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);

    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  }).fail(function(err) {   
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});

$('body').on('submit','#editCity',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);

    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  }).fail(function(err) {   
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});

$('body').on('submit','#editGear',function(e){
  e.preventDefault();
  const form = $(this);
  const loading = $('.loading');
  loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    console.log(res);

    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  }).fail(function(err) {   
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    setTimeout(function(){
      loading.removeClass('active')
    }, 1000)
  });
});

$('body').on('submit','#editBrand',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('body').on('submit','#editModel',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('body').on('submit','#editSeats',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('body').on('submit','#editTransmission',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});


$('body').on('submit','#editCoordinates',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});


$('body').on('submit','#editPower',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('body').on('submit','#editAssessedValue',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('body').on('submit','#editVolume',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);

        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});


$('body').on('submit', '#mileageInput', function (e) {
  e.preventDefault();
  var form = $(this);
  var loading = $('.loading');
  loading.addClass('active');
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize()
  }).done(function (res) {
    console.log(res);
    form.attr('id');
    form.load(' #' + form.prop('id') + ' > *');
    setTimeout(function () {
      loading.removeClass('active');
    }, 1000);
  }).fail(function (err) {
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit', '#mileageInputMyCar', function (e) {
  e.preventDefault();
  var form = $(this);
  var loading = $('.loading');
  loading.addClass('active');
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize()
  }).done(function (res) {
    console.log(res);
    form.attr('id');
    form.load(' #' + form.prop('id') + ' > *');
    setTimeout(function () {
      loading.removeClass('active');
    }, 1000);
  }).fail(function (err) {
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit', '#form-rescission', function (e) {
  e.preventDefault();
  var form = $(this);
  var loading = $('.loading');
  loading.addClass('active');
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize()
  }).done(function (res) {
    console.log(res);
    form.attr('id');
    form.load(' #' + form.prop('id') + ' > *');
    setTimeout(function () {
      loading.removeClass('active');
    }, 1000);
  }).fail(function (err) {
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});


if ($('#editPhoto').length) {
  //Загрузка авто для фото на странцие редактирования
  FilePond.create(
      document.querySelector('#editPhoto'),
      {
          name: 'photo',
          allowMultiple: true,
          server: {
              url: `/car/${car_id}/image`,
              process: {
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  onload: function(res){

                  },
                  onerror: function(res){
                    console.log(res);
                    
                    alert('Вы загрузили максимально допустимое количество фото')
                  }
              }
          }
      }
  );

}

$('body').on('submit', '.editOsagoKaskoDate', function (e) {
  e.preventDefault();
  var form = $(this);
  var loading = $('.loading');
  loading.addClass('active');
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize()
  }).done(function (res) {
    setTimeout(function () {
      loading.removeClass('active');
    }, 1000);
  }).fail(function (err) {
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit', '#editGos', function (e) {
  e.preventDefault();
  var form = $(this);
  var loading = $('.loading');
  loading.addClass('active');
  $.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize()
  }).done(function (res) {
    setTimeout(function () {
      loading.removeClass('active');
    }, 1000);
  }).fail(function (err) {
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});