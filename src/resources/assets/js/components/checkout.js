import { from } from "rxjs";

$(window).load(function () {





  $('body').on('click', '#ao-button-checkout', function (e) {
    e.preventDefault();
    ym(53583991, 'reachGoal', 'pay-button-click');
    $('#booking-payment').submit();

    //    $('#policy').modal('show');
  });

  $('body').on('click', '.yes', function () {

    $('#policy').modal('hide');
  });
  $('body').on('click', '.no', function () {
    $('#policy').modal('hide');
  });

  //Если есть блок показываем модалку(для случаев когда нельзя забронировать авто)
  if ($('.not').length) {
    $('#not').modal('toggle');
  }



  $('body').on('submit', '#sendValidateSms', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $("#numberExist").hide();

    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
    }).done(function (res) {
      // console.log(res);
      if (res != 0) {
        form.find('p').text(res.message);
        $('#validatePhone').removeClass('disabled');
        setTimeout(function () {
          loading.removeClass('active')
        }, 1000)
      } else {
        loading.removeClass('active')
        $("#numberExist").show();
      }
      
    }).fail(function (err) {
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
      setTimeout(function () {
        loading.removeClass('active')
      }, 1000)
    });
  });

  $('body').on('submit', '#validatePhone', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
    }).done(function (res) {
      // console.log(res);
      setTimeout(function () {
        loading.removeClass('active')
      }, 1000)
      if (res.message == 'Код введен не верно.') {
        form.find('p').text(res.message);
      } else {
        form.find('p').text(res.message);
        $('.container-confirm-phone form').addClass('d-none');
        $('.confirm-text').removeClass('d-none')
        $('#checkout-lists').load(` #checkout-lists > *`).fadeIn();
        $('.buttons').load(` .buttons > *`);
        if ($('.next').length) {
          $('.next').removeClass('d-none').addClass('d-flex');
        }
        ym(53583991, 'reachGoal', 'phone-verify');
      }
    }).fail(function (err) {
      alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
      setTimeout(function () {
        loading.removeClass('active')
      }, 1000)
    });
  });




  let extras = [];

  $('body').on('click', '.extra-btn', function () {

    const div = $(this).closest('.card__photo');
    const sum = $('#totalSum').text();
    let rentSum = $('#rentSum').text();
    const deposit = $('#deposit').text();
    const bookingSum = $('#bookingSum').text();
    const procent = $('#index').val();
    const number_days = $('#number_days').text();



    let cost = $(this).attr("data-cost");
    let id = $(this).attr("data-id");
    let totalSum;

  //  let id = div.find('.extra_id').text();
    let input = div.find('.extra_input').val();
    const time = $('#time' + id).text();

    console.log(number_days + " " + time + " " + cost);


    if (input > 0) {
      cost = cost * input
    }

    if ($(this).text() == 'Добавить') {
      extras.push({
        'id': id,
        'input': input,
        'cost': cost
      })

      if (time == 1) {
        cost = cost * parseInt(number_days, 10);
        totalSum = parseInt(rentSum, 10) + parseInt(cost, 10);
      } else {
        totalSum = parseInt(rentSum, 10) + parseInt(cost, 10);
      }

      

    } else {
      extras = $.grep(extras, function (e) {
        return e.id != id;
      })
      if (time == 1) {
        cost = cost * parseInt(number_days, 10);
        totalSum = parseInt(rentSum, 10) - parseInt(cost, 10);
      } else {
      totalSum = parseInt(rentSum, 10) - parseInt(cost, 10);
      }
    }
    
    let bookingSumEnd = parseInt(totalSum, 10) / 100 * procent;

  //  console.log(bookingSumEnd + " " + totalSum + " " + procent);

    totalSum = Math.round(totalSum);
    bookingSumEnd = Math.round(bookingSumEnd);

    $('#rentSum').text(totalSum);
    $('#bookingSum').text(bookingSumEnd);



    totalSum = parseInt(totalSum, 10) + parseInt(deposit, 10);
    $('#КesidualAmount').text(totalSum);


    var text = $(this).text();
    $(this).text(text == 'Добавить' ? 'Убрать' : 'Добавить');

    $('#extras').val(JSON.stringify(extras));


  });


  $('.login-button').click(function () {
    $('#login').modal('show');
  });


  $('body').on('submit', '#checkout-login', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
    })
      .done(function (res) {
        console.log(res);
        setTimeout(function () {
          loading.removeClass('active')
        }, 1000)
        $('#login').modal('hide');
        $('#checkout-lists').load(` #checkout-lists > *`).fadeIn();
        $('.buttons').load(` .buttons > *`);
        $('.token').load(` .token > *`);
        $('.header').load(` .header > *`).fadeIn();
      }).fail(function (err) {
        alert(err.responseJSON.errors.email);
        setTimeout(function () {
          loading.removeClass('active')
        }, 1000)
      });
  });


});
  