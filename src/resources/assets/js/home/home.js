import './search';
import './info';
import Swiper from 'swiper';
import Slideout from 'slideout';
import 'air-datepicker';
import ScrollMagic from 'scrollmagic';
import 'animation.gsap'
import 'debug.addIndicators'
import dayjs from 'dayjs'

var controller = new ScrollMagic.Controller();
if ($('#road').length) {
if ($(window).width() >= 1200) {
  
  // create a scene
  new ScrollMagic.Scene({triggerElement: '#road', duration: 890, offset: 200})
    .setTween('.car', 0.1, {y: 890}) // trigger a TweenMax.to tween
    .addTo(controller);

  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1000})
    .setTween('.car', 0.1, {rotation: 90}) // trigger a TweenMax.to tween
    .addTo(controller);

  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1100})
    .setTween('.car', 0.1, {x: -160}) // trigger a TweenMax.to tween
    .addTo(controller);

  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1200})
    .setTween('.car', 0.1, {rotation: 0}) // trigger a TweenMax.to tween
    .addTo(controller);

  new ScrollMagic.Scene({triggerElement: '#road', duration: 500, offset: 1200})
    .setTween('.car', 0.1, {y: 1520}) // trigger a TweenMax.to tween
    .addTo(controller);
  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1700})
    .setTween('.car', 0.1, {rotation: -90}) // trigger a TweenMax.to tween
    .addTo(controller);
  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1700})
    .setTween('.car', 0.1, {x: 0}) // trigger a TweenMax.to tween
    .addTo(controller);
  new ScrollMagic.Scene({triggerElement: '#road', duration: 100, offset: 1800})
    .setTween('.car', 0.1, {rotation: 0}) // trigger a TweenMax.to tween
    .addTo(controller);
  new ScrollMagic.Scene({triggerElement: '#road', duration: 500, offset: 1900})
    .setTween('.car', 0.1, {y: 1950}) // trigger a TweenMax.to tween
    .addTo(controller);
}

if ($(window).width() >= 991 && $(window).width() < 1200) {  
  new ScrollMagic.Scene({triggerElement: '#road', duration: 3000})
    .setTween('.car', 0.1, {y: 2000}) // trigger a TweenMax.to tween
    .addTo(controller);
}
}
//Мобильное меню
let slideMenu = new Slideout({
  'panel': document.getElementById('panel'),
  'menu': document.getElementById('menu'),
  'padding': 256,
  'tolerance': 70
});

// Открытие мобильного меню
$('.open-menu').click(function() {
  slideMenu.toggle();
});

// let $coordinatesInput = $('#coordinates');
// $('#search_ul li').click(function () {
//   if ($(this).hasClass('location')) {
//     if (navigator.geolocation) {
//       navigator.geolocation.getCurrentPosition(function (position) {
//         console.log(position);
//         $coordinatesInput.val(`${position.coords.latitude},${position.coords.longitude}`);
//       }, function () {
//         console.log('your address not found')
//       });
//     } else {
//       console.log('you dont navigator')
//     }
//   } else {
//     $coordinatesInput.val('');
//   }
// });

//Выбор даты
let picker = $('#date').datepicker({
  minDate: new Date(),
  onSelect: function(formattedDate, date, inst) {    
    let arr = formattedDate.split('-');
    $('.form__date .label').hide();
    if (date.length === 1) {
      $('.form__date .from').text(newDate(0));
      $('.form__date span').fadeIn();
      
      $('#from').val(arr[0]);
    }
    if (date.length === 2) {
      $('.form__date .from').text(newDate(0));
      $('.form__date .to').text(newDate(1));
      $('#from').val(arr[0]);
      $('#to').val(arr[1]);
      picker.hide();
    }

    function newDate(number) {
      let day = date[number].getDate();
      let time = date[number].getHours() + ':' + (date[number].getMinutes()<10?'0':'') + date[number].getMinutes();
      let month = date[number].toLocaleString('ru', { month: 'short' });
      return `${day} ${month} ${time}`;
    }

  }
}).data('datepicker');
picker.selectDate([dayjs().add(1, 'day').toDate(), dayjs().add(7, 'day').toDate()])


//Слайдер
new Swiper ('.swiper-container', {
  loop: true,
  slidesPerView: 3,
  spaceBetween: 30,
  speed: 1000,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    // when window width is <= 320px
    767: {
      slidesPerView: 1,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
    },
    991: {
      slidesPerView: 2,
    },
  }
});
