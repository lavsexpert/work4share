let modelSelect = $('#model-select');
$('#brandid').change(function () {
    let brandId = $(this).val();
    $.get(`/staff/getModels/${brandId}`, function (data) {
        console.log(data);
        if (data.length) {
            let models = `<option value="">Ничего не выбрано</option>`;
            data.forEach(function (item) {
                models += `<option
                                class="form-cars__option" value="${item['id']}">
                            ${item['name']}
                        </option>`;
            });
            modelSelect.html(models);
            modelSelect.show();
        }
    });
});


$('body').on('submit','.staff-auto form',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    
    if (form.find('select').val().length == 0) {
        alert('Вы ничего не выбрали')
        loading.removeClass('active')
        return true;
    } else{
        $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
        }).done(function(res) {
            console.log(res);
            
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
        }).fail(function(err) {
            console.log(err);
            
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
        });
    }
  });