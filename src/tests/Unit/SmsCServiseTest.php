<?php

namespace Tests\Unit;

use Tests\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use Psr\Http\Message\StreamInterface;
use App\Services\SMSService\Sms;
use App\Services\SMSService\SmsCClient;
use App\Services\SMSService\SmsCService;
use App\Services\SMSService\AbstractSms;
use App\Services\SMSService\SmsException;
use App\Services\SMSService\InterfaceSmsService;

class SmsCServiceTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();

        $sms = $this->createMock(AbstractSms::class);
        $sms->method('getDataArray')
             ->willReturn(['phones' => 84464726304, 'mes' => 'Hello']);
        $this->sms = $sms;

        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')
            ->willReturn('{"id": 1, "cnt": 1}');
        $this->body = $body;

        $response = $this->createMock(Response::class);
        $response->method('getBody')
            ->willReturn($body);
        $this->response = $response;

        $client = $this->createMock(SmsCClient::class);
        $client->method('send')
            ->willReturn($this->response);
        $this->client = $client;

        $this->app->singleton(SmsCClient::class, function() use ($client) {
            return $client;
        });
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetStatus()
    {
        $smso = new Sms;
        $smso->setPhone(89176411436);
        $smso->setMessage('test');

        $sms_service = resolve(SmsCService::class);
        $sms_service->sendSms($smso);
        $this->assertTrue(isset($sms_service->getStatus()['id']));
        $this->assertTrue($sms_service->getStatus()['cnt'] === 1);
        $this->assertTrue($sms_service->getStatus()['id'] === 1);

        $this->assertTrue(is_array($sms_service->getResponse()));


    }

    /**
     * @expectedException     App\Services\SMSService\SmsException
     * @expectedExceptionCode 1
    */
    public function testInvalidService()
    {
        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')
            ->willReturn('{"error_code": 1, "error": "Любая ошибка"}');

        $response = $this->createMock(Response::class);
        $response->method('getBody')
            ->willReturn($body);

        $client = $this->createMock(SmsCClient::class);
        $client->method('send')
            ->willReturn($response);

        $service = new SmsCService(
            $client,
            $this->app['config']->get('sms.services.smsc')
        );

        $service->sendSms(new Sms);
    }

    public function testResolve()
    {
        $this->assertInstanceOf(InterfaceSmsService::class, resolve(SmsCService::class));
    }

    public function testSendSms()
    {
        /** @var SmsCService $service */
        $service = resolve(SmsCService::class);
        $this->assertTrue($service->sendSms($this->sms));
    }

    public function testSms()
    {
        $smso = new Sms;
        $smso->setPhone(89176411436);
        $smso->setMessage('rgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrgrgrehuheiuheihvvhifvrgrehuheiuheihvvhifvrg');

        $this->assertStringMatchesFormat('%d', $smso->getPhone());
        $this->assertTrue(1000 === mb_strlen($smso->getMessage()));
        $this->assertTrue('89176411436' === $smso->getPhone());
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
