<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UnloadHotTest extends TestCase
{
    use WithoutMiddleware, DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $user = factory(\App\User::class)->make([
            'id' => 1,
            'name' => 'Test',
            'email' => 'test@test.tu'
        ]);

        factory(\App\Models\Car::class)->create();

        Storage::fake('hot');
        Storage::fake('ice');

        Hash::shouldReceive('make')
            ->once()
            ->andReturn('value');

        Auth::shouldReceive('user')
            ->once()
            ->andReturn($user);
    }

    /**
     * Тест загрузки фото авто
     *
     * @return void
     */
    public function testSaveHotBacket()
    {
        $this->assertDatabaseMissing('cars_images', ['car_id' => 1, 'name' => 'value']);

        $response = $this->json(
            'POST',
            '/api/cars/add/photo',
            [
                'CarId' => 1,
                'photo' => UploadedFile::fake()->image('test.jpg')
            ]
        );

        $response->assertStatus(200);
        $response->assertJson(['carId' => 1, 'result' => true]);

        $this->assertDatabaseHas('cars_images', ['car_id' => 1, 'name' => 'value']);

        Storage::disk('hot')->assertExists('value');
        Storage::disk('ice')->assertMissing('value');
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
