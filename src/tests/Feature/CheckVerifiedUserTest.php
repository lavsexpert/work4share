<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CheckVerifiedUserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        factory(\App\Models\Car::class)->create();

        Storage::fake('hot');
        Storage::fake('ice');
    }

    /**
     * Тест бронирования авто не верефецированным пользователем
     *
     * @return void
     */
    public function testReserveCarRedirect()
    {

        $user = factory(\App\User::class)->make([
            'id' => 2,
            'name' => 'Test',
            'email' => 'test@test1.tu',
            'phone' => '+79000000000'
        ]);

        $response = $this->actingAs($user)->json(
            'POST',
            'carcard/1/reserve',
            []
        );

        $response->assertRedirect(route('user.profile'));
    }

    /**
     * Тест бронирования авто верефецированным пользователем
     *
     * @return void
     */
    public function testReserveCar()
    {

        $user = factory(\App\User::class)->make([
            'id' => 2,
            'name' => 'Test',
            'email' => 'test@test1.tu',
            'validate' => true,
            'phone' => '+79000000000'
        ]);

        $response = $this->actingAs($user)->json(
            'POST',
            'carcard/1/reserve',
            []
        );

        $response->assertStatus(200);
        $response->assertSee('заглушка');
    }

    /**
     * Тест бронирования авто владельцем
     *
     * @return void
     */
    public function testReserveCarInvalid()
    {

        $user = factory(\App\User::class)->make([
            'id' => 1,
            'name' => 'Test',
            'email' => 'test@test1.tu',
            'validate' => true,
            'phone' => '+79000000000'
        ]);

        $response = $this->actingAs($user)->json(
            'POST',
            'carcard/1/reserve',
            []
        );

        $response->assertForbidden();
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
