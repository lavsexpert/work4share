<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class NotificationsEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $subject;

    public $textMessage;
    
    public $buttonText;

    public $url;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $textMessage, $url = '', $buttonText = '')
    {
        $this->subject = $subject;

        $this->textMessage = $textMessage;

        $this->url = $url;
        
        $this->buttonText = $buttonText;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@soauto.ru')
                    ->subject($this->subject)
                    ->view('emails.notifications',[
                        'textMessage' => $this->textMessage,
                        'url' => $this->url,
                        'buttonText' => $this->buttonText
                    ]);
    }
}