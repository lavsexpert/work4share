<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Car as CarModel;

/**
 *
 */
class Car extends JsonResource
{
    /**
     * The resource instance.
     *
     * @var CarModel
     */
    public $resource;

    public function __construct(CarModel $resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'available' => $this->available,
            'image' => $this->carImages()->first()['name'],
            'brand' => $this->brand->name,
            'model' => $this->model->name,
            'year_of_issue' => $this->year_of_issue,
            'seat' => $this->seat,
            'doors' => $this->doors,
            'cost_day' => $this->cost_day,
            'car_type' => $this->cartype->name,
            'gear' => $this->gear->name,
            'free_days' => DateRentCars::collection($this->resource->dateRentCars)
        ];
    }
}
