<?php

namespace App\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 */
class Feature extends JsonResource
{

    public function toArray($request)
    {
        return [
            'air' => $request->has('air'),
            'bluetooth' => $request->has('bluetooth'),
            'audio_input' => $request->has('audio_input'),
            'usb_charger' => $request->has('usb_charger'),
            'child_seat' => $request->has('child_seat'),
            'cruise_control' => $request->has('cruise_control'),
            'gps' => $request->has('gps')
        ];
    }
}
