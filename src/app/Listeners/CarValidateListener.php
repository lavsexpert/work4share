<?php

namespace App\Listeners;

use App\Events\CarValidateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DateRentCars;
use Carbon\Carbon;
class CarValidateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CarValidateEvent  $event
     * @return void
     */
    public function handle(CarValidateEvent $event)
    {
        $from = Carbon::now();
        $to = Carbon::now()->addMonths(3);

        DateRentCars::create([
            'car_id' => $event->car->id,
            'active' => 1,
            'from' => $from,
            'to' => $to
        ]);
    }
}
