<?php

namespace App\Services\SMSService;

use App\Services\SMSService\InterfaceSMS;

/**
 *
 */
abstract class AbstractSms implements InterfaceSMS
{

    abstract public function setPhone(string $phone);

    abstract public function getPhone(): string;

    abstract public function setMessage(string $message);

    abstract public function getMessage(): string;

    public function getDataArray(): array
    {
        return collect(
            get_object_vars($this)
        )->toArray();
    }
}
