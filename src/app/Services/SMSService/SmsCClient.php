<?php

namespace App\Services\SMSService;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\ClientInterface;

/**
 *
 */
class SmsCClient
{
    private $client;
    private $url;
    private $urn;
    private $login;
    private $psw;
    private $charset;
    private $fmt;
    private $sender;

    public function __construct(ClientInterface $client, array $config)
    {
        $this->client = $client;
        $this->url = $config['host'];
        $this->urn = $config['method'];
        $this->login = $config['login'];
        $this->psw = $config['password'];
        $this->charset = $config['charset'] ?? 'utf-8';
        $this->fmt = $config['response_format'] ?? 1;
        $this->sender = $config['sender'] ?? '';
    }

    public function send(InterfaceSMS $sms)
    {
        return $this->client->request(
            'POST',
            $this->getUri(),
            [
                'query' => $this->getParams() + $sms->getDataArray()
            ]
        );
    }

    private function getParams(): array
    {
        return [
            'login'     => $this->login,
            'psw'       => $this->psw,
            'charset'   => $this->charset,
            'fmt'       => $this->fmt,
            'sender'    => $this->sender
        ];
    }

    private function getUri()
    {
        return $this->url . $this->getUrn();
    }

    private function getUrn(): string
    {
        return (mb_strpos($this->urn, '/') === 0) ? $this->urn : '/' . $this->urn;
    }
}
