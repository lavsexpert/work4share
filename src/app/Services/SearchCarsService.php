<?php


namespace App\Services;


use App\Models\Car;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class SearchCarsService
{
    public function search(Collection $params): Builder
    {
        $diff_time = Carbon::now()->diffInMinutes(Carbon::parse($params->get('from', Carbon::now())), false);
        $from_cost_day = null == $params->get('from_cost_day') ? '0' : $params->get('from_cost_day'); // Цена от
        $to_cost_day   = null == $params->get('to_cost_day ')  ? Car::max('cost_day') : $params->get('to_cost_day '); // Цена до
        return Car::where('available', '1')
            ->orderBy('cost_day', 'desc') // Вывод авто по умолчанию от самых дорогих
            ->whereBetween('cost_day', [$from_cost_day, $to_cost_day])  // Фильтрация по цене
            ->where('validate', '1')
            ->when(
                $params->get('brand_id'),
                function (Builder $query, $value) {
                    return $query->where('brand_id', $value);
                })
            ->when(
                $params->get('model_id'),
                function (Builder $query, $value) {
                    return $query->where('model_id', $value);
                })
            ->whereHas(
                'feature',
                function (Builder $query) use ($params) {
                    foreach ($params->get('feature', []) as $column => $value) {
                        $query->where($column, $value);
                    }
                })
            ->whereHas(
                'dateRentCars',
                function (Builder $query) use ($params) {
                    $query->when(
                        Carbon::parse($params->get('from', Carbon::now())),
                        function (Builder $query, $value) {
                            return $query->where('from', '<=', $value)
                                ->where('to', '>=', $value);
                        })
                    ->when(
                        Carbon::parse($params->get('to')),
                        function (Builder $query, $value) {
                            return $query->where('to', '>=', $value)
                                ->where('from', '<=', $value);
                        });
                })
            ->whereHas(
                'deliveryCars',
                function (Builder $query) use ($params) {
                    $query->when(
                        $params->get('seat'),
                        function (Builder $query, $value) {
                            return $query->where('place', '=', $value);
                        });
                })
            ->whereHas(
                'timeAlertOwnRent',
                function ($query) use ($diff_time) {
                    $query->where('time', '<', $diff_time);
                }
            );
    }
}