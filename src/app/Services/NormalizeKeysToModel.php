<?php

namespace App\Services;

/**
 * Класс нормализации ключей в формат модели
 *
 * Class NormalizeKeysToModel
 */
class NormalizeKeysToModel
{
    const CAR_KEYS = [
        'brandid' => 'brand_id',
        'modelid' => 'model_id',
        'yearOfIssue' => 'year_of_issue',
        'gearid' => 'gear_id',
        'carType' => 'car_type',
        'carTypeid' => 'car_type'
    ];

    const CAR_FEATURE_KEY = [
        'audioInput' => 'audio_input',
        'usbCharger' => 'usb_charger',
        'childSeat' => 'child_seat',
        'cruiseControl' => 'cruise_control'
    ];

    public static function normalizeToCar(string $key) : string
    {
        return collect(self::CAR_KEYS)->get($key) ?: $key;
    }

    public static function normalizeToCarFeature(string $key) : string
    {
        return collect(self::CAR_FEATURE_KEY)->get($key) ?: $key;
    }
}
