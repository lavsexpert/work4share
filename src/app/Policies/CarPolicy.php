<?php
namespace App\Policies;

use App\User;
use App\Models\Car;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarPolicy
{
    use HandlesAuthorization;

    /**
    * Determine whether the user can view the Car.
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function view(User $user, Car $car)
    {
        return true;
    }

    /**
    * Determine whether the user can create Car.
    *
    * @param  \App\User  $user
    * @return bool
    */
    public function create(User $user)
    {
        return $user->id > 0;
    }

    /**
    * Determine whether the user can update the Car.
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function update(User $user, Car $car)
    {
        return $user->id == $car->user_id;
    }

    /**
    * Determine whether the user can delete the Car.
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function delete(User $user, Car $car)
    {
        return $user->id == $car->user_id;
    }

    /**
    * Determine whether the user can reserve the Car.
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function reserve(User $user, Car $car)
    {
        return $user->id > 0 && (bool) $user->validate;
    }

    /**
    * Determine whether the user can reserve the Car.
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function isNoOwner(User $user, Car $car)
    {
        return $user->id != $car->user_id;
    }

    /**
    * Determine whether the user is valid pasport
    *
    * @param  \App\User  $user
    * @param  \App\Models\Car  $car
    * @return bool
    */
    public function validate(User $user)
    {
        return (bool) $user->validate;
    }
}
