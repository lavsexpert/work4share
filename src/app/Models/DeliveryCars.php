<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DeliveryCars
 *
 * @property int $id
 * @property int|null $car_id
 * @property int|null $alert_time
 * @property int|null $cost
 * @property int|null $place
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Car|null $car
 * @property-read \App\Models\RentPlace|null $rentPlace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereAlertTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCars whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeliveryCars extends Model
{
    protected $table = 'delivery_cars';

    protected $fillable = [
        'car_id',
        'cost',
        'place'
    ];

    public function rentPlace()
    {
        return $this->belongsTo('App\Models\RentPlace', 'place', 'id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
