<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function car()
    {
        return $this->hasOne('App\Models\Car', 'car_type', 'car_type');
    }
}
