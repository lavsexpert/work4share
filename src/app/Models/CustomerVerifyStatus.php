<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerVerifyStatus
 *
 * @package App\Models
 * @property boolean
 * @property int $id
 * @property string $stage_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $personal_user_id
 * @property-read \App\Models\PersonalUser|null $personalUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus wherePersonalUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus whereStageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerVerifyStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CustomerVerifyStatus extends Model
{
    protected $table = 'customer_verify_status';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stage_name',
        'personal_user_id'
    ];

    public function personalUser()
    {
        return $this->belongsTo('App\Models\PersonalUser', 'personal_user_id', 'id');
    }
}
