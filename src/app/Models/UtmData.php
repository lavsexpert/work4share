<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UtmData
 *
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereUtmCampaign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereUtmMedium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UtmData whereUtmSource($value)
 * @mixin \Eloquent
 */
class UtmData extends Model
{
    protected $table = 'users_utm';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
