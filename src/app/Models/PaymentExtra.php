<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentExtra
 *
 * @property int $id
 * @property int|null $booking_id
 * @property int|null $extra_id
 * @property int|null $count
 * @property int|null $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExtra whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentExtra extends Model
{
    protected $fillable = [
        'booking_id',
        'extra_id',
        'count',
        'amount'
    ];
}
