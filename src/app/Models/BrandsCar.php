<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BrandsCar
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ModelsCar[] $models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BrandsCar whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BrandsCar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function models()
    {
        return $this->hasMany('App\Models\ModelsCar', 'brand_id');
    }
}
