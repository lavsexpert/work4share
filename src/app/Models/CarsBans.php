<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarsBans
 *
 * @property int $id
 * @property int|null $cars_id
 * @property int|null $bans_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CarBans|null $bans
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereBansId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsBans whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarsBans extends Model
{
    protected $fillable = [
        'cars_id',
        'bans_id',
        'active'
    ];

    public function bans()
    {
       return $this->belongsTo(CarBans::class);
    }
}
