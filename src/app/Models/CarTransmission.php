<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarTransmission
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarTransmission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarTransmission extends Model
{
    //
}
