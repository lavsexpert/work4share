<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingPhoto
 *
 * @property int $id
 * @property int|null $booking_id
 * @property string|null $name
 * @property int|null $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingPhoto whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingPhoto extends Model
{
    protected $table = 'booking_photos';

    protected $fillable = [
        'booking_id',
        'name',
        'state'
    ];
}
