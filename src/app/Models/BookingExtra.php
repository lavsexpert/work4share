<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingExtra
 *
 * @property int $id
 * @property int|null $booking_id
 * @property int|null $extra_id
 * @property int|null $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingExtra whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingExtra extends Model
{
    protected $fillable = [
      'booking_id',
      'extra_id',
      'cost'
    ];
}
