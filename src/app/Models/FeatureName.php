<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FeatureName
 *
 * @property int $id
 * @property string $en_name
 * @property string $ru_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName whereEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName whereRuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FeatureName whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FeatureName extends Model
{
    protected $table = 'feature_name';
}
