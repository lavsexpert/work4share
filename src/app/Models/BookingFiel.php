<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingFiel
 *
 * @property int $id
 * @property int|null $booking_id
 * @property string|null $fiel_photo_before
 * @property string|null $fiel_photo_after
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereFielPhotoAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereFielPhotoBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingFiel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingFiel extends Model
{
    protected $table = 'booking_fiels';

    protected $fillable = [
        'booking_id',
        'fiel_photo_before',
        'fiel_photo_after'
    ];
}
