<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class CarsDeposit
 *
 * @package App\Models
 * @property integer $price
 * @property int $id
 * @property int $car_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsDeposit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarsDeposit extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }

}
