<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

/**
 * App\Models\BookingDate
 *
 * @property int $id
 * @property string $user_id
 * @property string $car_id
 * @property string $datefrom
 * @property string $dateto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $ownGaveKayBeforeRent
 * @property int|null $usrTookKayBeforeRent
 * @property int|null $usrGaveKayAfterRent
 * @property int|null $ownTookKayBeforeRent
 * @property int|null $status
 * @property int|null $action
 * @property int|null $own_booking_confirm
 * @property int|null $count_day
 * @property string|null $mileage
 * @property string|null $deposit
 * @property int|null $delivery_place
 * @property-read \App\Models\Car $cars
 * @property-read \App\Models\PaymentBooking $payment
 * @property-read \App\Models\BookingStatus|null $statusBooking
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate getBookingsDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereCountDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereDatefrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereDateto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereDeliveryPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereDeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereOwnBookingConfirm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereOwnGaveKayBeforeRent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereOwnTookKayBeforeRent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereUsrGaveKayAfterRent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingDate whereUsrTookKayBeforeRent($value)
 * @mixin \Eloquent
 */
class BookingDate extends Model
{
    protected $table = 'booking_date';

    protected $fillable = [
        'user_id',
        'car_id',
        'datefrom',
        'dateto',
        'status',
        'action',
        'count_day',
        'mileage',
        'deposit',
        'delivery_place',
        'kasko'
    ];

    public function cars()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }

    public function statusBooking()
    {
        return $this->belongsTo(BookingStatus::class, 'status', 'id');
    }
    public function payment()
    {
        return $this->hasOne(PaymentBooking::class, 'booking_id', 'id');
    }
    public function scopegetBookingsDate()
    {
        return BookingDate::where('user_id', Auth::id())->with('payment')->get();
    }
    public function delivery()
    {
        return $this->belongsTo(BookingDelivery::class, 'id', 'booking_id');
    }
}
