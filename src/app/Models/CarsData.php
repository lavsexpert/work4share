<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarsData
 *
 * @property int $id
 * @property string|null $pts_image_part_one
 * @property string|null $pts_image_part_two
 * @property string|null $sts_image_part_one
 * @property string|null $sts_image_part_two
 * @property string|null $osago_image
 * @property string|null $kasko_image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereKaskoImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereOsagoImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData wherePtsImagePartOne($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData wherePtsImagePartTwo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereStsImagePartOne($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereStsImagePartTwo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsData whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarsData extends Model
{
    protected $table = 'cars_data';
    protected $fillable = [
        'id',
        'car_id',
        'pts_image_part_one',
        'pts_image_part_two',
        'sts_image_part_one',
        'sts_image_part_two',
        'osago_image',
        'kasko_image'
    ];
}
