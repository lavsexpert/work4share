<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExtraTime
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraTime whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExtraTime extends Model
{
    protected $table = 'extra_time';
}
