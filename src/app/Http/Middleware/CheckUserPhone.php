<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Web\UserController;

class CheckUserPhone
{
    /**
     * Проверка
     * @param  [type]  $request [description]
     * @param  Closure $next    [description]
     * @param  [type]  $ability [description]
     * @return [type]           [description]
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        if (!$user->phone) {
            return redirect(action('Web\UserController@editPhone'))->withInput(
                $request->flashOnly(['name', 'email', 'phone'])
            );
        }

        return $next($request);
    }
}
