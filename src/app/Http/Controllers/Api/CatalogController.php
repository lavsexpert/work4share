<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiFilterRequest;
use App\Models\Car;
use App\Models\CarsGear;
use App\Models\CarType;
use App\Models\Extra;
use App\Models\FeatureName;
use App\Models\RentPlace;
use App\Models\City;
use Illuminate\Support\Facades\Log;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index(Request $request)
    {
        $result = [
            'cars' => $this->selectAndUpdateLinks(
                Car::getCatalogCars()
                    ->orderByDesc('cost_day'))
        ];
                
        $this->setMinAndMax($result);
		
        return response()->json($result);
    }

    public function SimilarAutos(Request $request)
    {
        $result = [
            'cars' => $this->selectAndUpdateLinks(
                Car::find(99))
        ];
        $this->setMinAndMax($result);

        return response()->json($result);
    }

    public function updateLink($car)
    {

        $avatars = $car->carImages->filter(function ($value, $key) {
            return $value->avatar === 1;
        });
        if ($avatars->count()) {
            $car->image = env('AWS_URL', 'https://hb.bizmrg.com') . "/" . env('AWS_BUCKET_HOT', 'soautomedia') . "/" . $avatars->first()->name;
        }

        return $car;
    }

    /**
     * @param  $query
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function selectAndUpdateLinks($query)
    {
        return $query
            ->select(Car::getSelectParameters())
            ->get()
            ->map([\Helpers::class, 'updateLink']);
    }

    /**
     * @param $result
     */
    public function setMinAndMax(&$result)
    {
        $result['minPrice'] = $result['cars']->min('cost_day') ?? 0;
        $result['maxPrice'] = $result['cars']->max('cost_day') ?? 0;
    }

    public function filter(ApiFilterRequest $request)
    {
        $query = Car::getCars(null, $request->from, $request->to);
        $functions = Car::getFilterFunctions();
        foreach ($request->keys() as $key) {
            if (isset($functions[$key]) and $request->$key) {
                $function = "App\Models\Car::$functions[$key]";
                $query = $function($query, $request->$key);
            }
        }

        $result = [
            'cars' => $this->selectAndUpdateLinks($query)
        ];
        $this->setMinAndMax($result);

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGearTypes()
    {
        return response()->json(CarsGear::all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExtraData()
    {
        return response()->json([
            "gears" => CarsGear::all(),
            "locations" => RentPlace::all(),
            "options" => FeatureName::all(),
            "extras" => Extra::all(),
            "car_types" => CarType::all()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocations()
    {
        return response()->json(City::all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOptions()
    {
        return response()->json(FeatureName::all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExtraOptions()
    {
        return response()->json(Extra::all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCarTypes()
    {
        return response()->json(CarType::all());
    }





}
