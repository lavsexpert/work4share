<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Validator;
use App\Models\{Car, StsImage, CarImage, PersonalUser, CarsData};
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

use App\User;

/**
 * Контроллер работы с фото авто
 */
class ImageController extends Controller
{

    /**
     * Сохраняем файл из запроса
     *
     * @param  Car        $car     Модель авто
     * @param  Request    $request запрос
     * @return Collection          результат сохранения
     */
    public static function savePtsImage(Car $car, Request $request, $type): Collection
    {   
        $car = Auth::user()->cars()->findOrFail($car->id);
        $hash = static::generateHashSts($car, Auth::user());
        $result = collect(['result' => $type]);
        try {
            Storage::disk('ice')->put(
                $hash,
                $request->file('photo')->get()
            );

            // проверяем что файл действительно есть на шаре
            if (Storage::disk('ice')->exists($hash)) {
                if ($car->car_data_id == 0) {
                    $image = CarsData::create([$type => $hash]);
                    $car->update([
                        'car_data_id' => $image->id
                    ]);
                } else{
                    $image = CarsData::where('id', $car->car_data_id)->update([$type => $hash]);
                }
            } else {
                $result->put('code', "2");
            }

        } catch (QueryException $e) {
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }


    

    /**
     * Сохраняем файл из запроса
     *
     * @param  Car        $car     Модель авто
     * @param  Request    $request запрос
     * @return Collection          результат сохранения
     */
    protected function saveCarImage(Car $car, Request $request)
    {
        if (count(CarImage::where('car_id', $car->id)->get()) > 10) {
            abort(500, 'Something went wrong');
        }
        $hash = static::generatePhoto($car, Auth::user());

        $result = collect(['result' => false]);
        try {
            Storage::disk('hot')->put(
                $hash,
                $request->file('photo')->get(),
                array('ACL'=>'public-read')
            );

            // проверяем что файл действительно есть на шаре
            if (Storage::disk('hot')->exists($hash)) {
                $car->carImages()->saveMany([
                    new CarImage(['name' => $hash, 'avatar' => 0])
                ]);

                $result->put(
                    'result',
                    $car->save()
                )->put(
                    'carId',
                    $car->id
                );

                $query = DB::table('cars_images')
                    ->select('id')
                    ->where('car_id', $car->id)
                    ->orderBy('name', 'asc')
                    ->limit(1)
                    ->get();

                DB::table('cars_images')
                    ->where('id', $query[0]->id)
                    ->update(['avatar' => 1]);
            } else {
                $result->put('code', "2")->put('test', $query);
            }

        } catch (QueryException $e) {
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }

    /**
     * Сохраняем файл из запроса
     *
     * @param  Car        $car     Модель авто
     * @param  Request    $request запрос
     * @return Collection          результат сохранения
     */
    protected function savePassport(Car $car, Request $request, $type)
    {
        $user = Auth::user(); 
        $hash = static::generatePhoto($car, Auth::user());
        $result = collect(['result' => false]);
        try {
            
            Storage::disk('ice')->put(
                $hash,
                $request->file('photo')->get(),
                array('ACL'=>'public-read')
            );
            
            // проверяем что файл действительно есть на шаре
            if (Storage::disk('ice')->exists($hash)) {
                $personal = null;
                $result = $user->personal_user_id;
                if ($user->personal_user_id == null) {
                    $personal = PersonalUser::create([$type => $hash]);
                    $user->update(['personal_user_id' => $personal->id]);
                } else{
                    $personal = PersonalUser::where('id', $user->personal_user_id)->update([$type => $hash]);
                }
                
                


            } else {
                $result->put('code', "2")->put('test', $query);
            }

        } catch (QueryException $e) {
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }

    protected function saveOsagoKaskoImage(Car $car, Request $request, $type): Collection
    {   
        $car = Auth::user()->cars()->findOrFail($car->id);
        $hash = static::generateHashSts($car, Auth::user());
        $result = collect(['result' => $type]);
        try {
            Storage::disk('ice')->put(
                $hash,
                $request->file('photo')->get()
            );

            // проверяем что файл действительно есть на шаре
            if (Storage::disk('ice')->exists($hash)) {
                if ($car->car_data_id == 0) {
                    $image = CarsData::create([$type => $hash]);
                    $car->update([
                        'car_data_id' => $image->id
                    ]);
                } else{
                    $image = CarsData::where('id', $car->car_data_id)->update([$type => $hash]);
                }
            } else {
                $result->put('code', "2");
            }

        } catch (QueryException $e) {
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }


    /**
     * Генерируем уникальный хеш для Sts
     *
     * @param  Car             $car  Модель авто для которой генерируем имя файла
     * @param  Authenticatable $user Модель юзера
     * @return string                сгенерированный хеш
     */
    protected static function generateHashSts(Car $car, Authenticatable $user): string
    {
        do {
            $hash = static::createName($car, $user);
        } while (!static::validateNameStsImage($hash));

        return $hash;
    }

    /**
     * Генерируем уникальный хеш для Photo
     *
     * @param  Car             $car  Модель авто для которой генерируем имя файла
     * @param  Authenticatable $user Модель юзера
     * @return string                сгенерированный хеш
     */
    protected static function generateHashPhoto(Car $car, Authenticatable $user): string
    {
        do {
            $hash = static::createName($car, $user);
        } while (!static::validateNameCarImage($hash));

        return $hash;
    }

    protected static function generatePhoto(Car $car, Authenticatable $user): string
    {

        $hash = static::createName($car, $user);
        return $hash;
    }

    protected static function createHashName(Car $car, Authenticatable $user): string
    {
        return Hash::make(md5(sprintf("%s%s", $car->id + $user->id, $user->email)));
    }

    protected static function createName (Car $car, Authenticatable $user): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 20; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return ($car->id + $user->id) . $randstring;
    }

    private static function validateNameStsImage($hash): bool
    {
        return Validator::make(
            compact($hash),
            ['hash' => 'required|unique:sts_image,name|max:255']
        )->fails();
    }

    private static function validateNameCarImage($hash): bool
    {
        return Validator::make(
            compact($hash),
            ['hash' => 'required|unique:cars_images,name|max:255']
        )->fails();
    }

    protected static function getValidateRolesCarImage(): array
    {
        return [
            'CarId' => 'required|numeric|exists:cars,id',
            'photo' => 'required|file|mimes:jpeg,bmp,png'
        ];
    }

    protected function generateImages(Request $request): array
    {
        $files = $request->files->all()['files'];

        $result = [];
        foreach($files as $file) {
            $result[$this->generateName()] = $file;
        }

        return $result;
    }

    private function generateName(): string
    {
        do {
            $hash = Str::random(40);
        } while (Validator::make(
            ['hash' => $hash],
            ['hash' => 'required|unique:cars_images,name|max:255']
        )->fails());

        return $hash;
    }

    public function addPhotosCar(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $validate = Validator::make(
            $request->all(),
            [
                'files.*' => 'file|mimes:jpeg,bmp,png'
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car_images = $this->generateImages($request);

        $result = collect(['result' => false]);

        foreach($car_images as $name => $file) {
            try {
                Storage::disk('hot')->put(
                    $name,
                    $file,
                    ['ACL'=>'public-read']
                );

                // проверяем что файл действительно есть на шаре
                if (Storage::disk('hot')->exists($name)) {
                    $car->carImages()->saveMany([
                        new CarImage(['name' => $name])
                    ]);

                    $result->put(
                        'result',
                        $car->save()
                    )->put(
                        'carId',
                        $car->id
                    );

                } else {
                    $result->put('code', "2");
                }

            } catch (QueryException $e) {
                $result->put('code', "1");
            }

            return $result;
        }
    }
}
