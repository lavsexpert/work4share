<?php

namespace App\Http\Controller\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function getCars(Request $request) {
            $query = DB::table('selectionCars')
            ->join('cars', 'selectionCars.car_id', '=', 'cars.id')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->select('cars.id', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear');
            


        if (!isset($request->brandid) && !isset($request->modelid)) {
            $selectionCars = $query
            ->where('cars.available', '1')
            ->where('cars.validate', '1')
            ->get();
        }

        

        $brandsCars = DB::table('brands_cars')
            ->select('brands_cars.id', 'brands_cars.name')
            ->orderBy('name')
            ->get();


        return view('viewCars.listCars', ['selectionCars' => $selectionCars,
                                          'brandsCars' => $brandsCars]);
    }
}
