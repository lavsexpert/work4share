<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Mail\NotificationsEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\{Car, PaymentBooking};
use Mail;
use Validator;

class CheckUserController extends Controller
{
    public function start() {

        $route = DB::table('staff_module')
            ->select('staff_module.route', 'staff_module.name')
            ->leftJoin('staff_access', 'staff_access.module_id', '=', 'staff_module.id')
            ->where('staff_access.user_id', Auth::id())
            ->get();

        $users = DB::table('users')
            ->select('users.id', 'users.NAME', 'users.email', 'users.email_verified_at', 'users.validate_refusal_id', 'users.phone', 'personal_users.passport_face', 'personal_users.passport_visa', 'personal_users.driver_lisense_first', 'personal_users.driver_license_second', 'personal_users.selfie', 'users.validate','users.birthday','users.driving_experience')
            ->leftJoin('personal_users', 'users.personal_user_id', '=', 'personal_users.id')
            ->whereNull('users.validate')
            ->get();

        $users_refusal = DB::table('users_validate_refusal')
            ->select('users_validate_refusal.id', 'users_validate_refusal.name')
            ->get();

        foreach($users as $item) {
            $item->passport_face = empty($item->passport_face) ? "" : Storage::disk('ice')->temporaryUrl($item->passport_face, now()->addMinutes(5));
            $item->passport_visa = empty($item->passport_visa) ? "" : Storage::disk('ice')->temporaryUrl($item->passport_visa, now()->addMinutes(5));
            $item->driver_lisense_first = empty($item->driver_lisense_first) ? "" : Storage::disk('ice')->temporaryUrl($item->driver_lisense_first, now()->addMinutes(5));
            $item->driver_license_second = empty($item->driver_license_second) ? "" : Storage::disk('ice')->temporaryUrl($item->driver_license_second, now()->addMinutes(5));
            $item->selfie = empty($item->selfie) ? "" : Storage::disk('ice')->temporaryUrl($item->selfie, now()->addMinutes(5));
        }

        return view('staff.check-user', ['route' => $route, 'users' => $users, 'users_refusal' => $users_refusal]);
    }

    public function setUserRefusalId(Request $request, $user_id) {

        

        $validate = Validator::make(
            $request->all(),
            [
                'user_refusal_id' => 'nullable|integer',
                'experience' => 'nullable|integer',
                'birthday' => 'nullable|date'
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        /* @var $user User */
        $user = User::where('id', $user_id)->first();
        if ($request->user_refusal_id){
            if ($request->user_refusal_id == 9) {
                $user->validate = 1;
            } else {
                $user->validate = $request->user_refusal_id;
            }
            $user->save();

            $refusal_name = DB::table('users_validate_refusal')
                ->where('id', $request->user_refusal_id)
                ->pluck('name')
                ->first();

            if ($user && $user->email_notif === 1) {
                $textMessage = '
            <p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000">' .
                    $refusal_name .
                    '</p>';
                $subject = $refusal_name;

                Mail::to($user->email)
                    ->send(new NotificationsEmail($subject, $textMessage));
            }

            if ($user && $user->sms_notif === 1) {
                $login = 'marussia_f1_team';
                $pass = 'SOAUTO_SMS';
                $endpoint = 'https://smsc.ru/sys/send.php';
                $client = new \GuzzleHttp\Client();

                $response = $client->request('GET', $endpoint, ['query' => [
                    'login' => $login,
                    'psw' => $pass,
                    'phones' => $user->phone,
                    'mes' => $refusal_name
                ]]);
            }

            $kaskoOver21 = $user->age >= 21 and $user->driving_experience >= 2;
            $kaskoOver25 = $user->age >= 25 and $user->driving_experience >= 5;
            $firstTime = true;


            //Получаем записи бронирования со статусом 22(Ожидание подтверждение владельца)
            $bookings = $user->bookingDates->where('status', 22);
            
            foreach ($bookings as $booking) {
                $car = Car::where('id', $booking->car_id)->first();
                $paymentBooking = PaymentBooking::where('booking_id', $booking->id);
                if (($car->assessed_value > 1000000 and $kaskoOver25) or ($car->assessed_value <= 1000000 and $kaskoOver21)) {
                    Log::info("Пользователь $user->id подвержден");
                    if ($paymentBooking['result']) {
                        $booking->update(['status' => 6]);
                    } else{
                        $booking->update(['status' => 1]);
                    }
                    
                    // if ($user && $user->email_notif === 1) {
                    //     $textMessage = '
                    //     <p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000">' .
                    //         "Твой автомобиль забронирован,  <a href='" . env("APP_URL") . "/trips/card/".$booking->id."'>подтверди аренду</a>" .
                    //         '</p>';
                    //     $subject = "Подтверждение аренды";

                    //     Mail::to($user->email)
                    //         ->send(new NotificationsEmail($subject, $textMessage));
                    // }
                    // if ($user && $user->sms_notif === 1) {
                    //     $login = 'marussia_f1_team';
                    //     $pass = 'SOAUTO_SMS';
                    //     $endpoint = 'https://smsc.ru/sys/send.php';
                    //     $client = new \GuzzleHttp\Client();

                    //     $response = $client->request('GET', $endpoint, ['query' => [
                    //         'login' => $login,
                    //         'psw' => $pass,
                    //         'phones' => $user->phone,
                    //         'mes' => "Твой автомобиль забронирован, подтверди аренду на ". env("APP_URL") . "/profile/trips/card/".$booking->id
                    //     ]]);
                    // }

                } else {
                    Log::info("Пользователь $user->id не допущен");
                    $booking->update(['status' => 5]);
                    $textMessage = '
                        <p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000">' .
                        "Привет, ты допущен до управления авто, но к сожалению, 
                        <br> мы не можем сдать тебе в аренду этот автомобиль.
                        <br> Подбери себе <a href='" . env("APP_URL") . "/catalog/'>лучший вариант</a>" .
                        '</p>';
                    $subject = "Аренда авто";
                    if ($firstTime) {
                        Mail::to($user->email)
                            ->send(new NotificationsEmail($subject, $textMessage));
                        $firstTime = false;
                    }


                }
            }

        }
        if ($request->birthday){
            $user->birthday = $request->birthday;
            $user->save();
        }
        if ($request->experience){
            $user->driving_experience = $request->experience;
            $user->save();
        }


        return response()->json();
    }
}
