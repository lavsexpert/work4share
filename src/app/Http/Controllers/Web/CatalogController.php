<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;


class CatalogController extends Controller
{
    public function index()
    {
        return view('catalog.index');
    }

    public function filter(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'from' => 'required',
                'to' => 'required',
                'coordinates' => 'nullable'
            ]
        );

        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return redirect()->route('car.catalog.index');
        }


        return view('catalog.index', [
            'from' => date('Y-m-d h:m:s', strtotime($request->from)),
            'to' => date('Y-m-d h:m:s', strtotime($request->to)),
            'coordinates' => $request->coordinates
        ]);

    }
}
