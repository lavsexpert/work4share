<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Common\UserController as Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Token;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;
use \Illuminate\Http\Response;

/**
 * Контроллер работы с пользователями
 */
class UserController extends Controller
{

    public function index($id)
    {
        return view('user.profile', ['id' => $id]);
    }
    /**
     * Получаем профиль
     *
     * @return Response      Коллекция полей для заполнения [ModelsCar, BrandsCar, CarsGear]
     */
    protected function getProfile(Request $request)
    {
        $user = Auth::user();
        return view(
            'user.profile.show',
            [
                'user' => $user,
                'personal' => collect($user->personalUser)->only([
                    'passport_face',
                    'passport_visa',
                    'driver_lisense_first',
                    'driver_license_second'
                ])->map(function ($value) {
                    return boolval($value);
                })
            ]
        );
    }

    public function editPhone(Request $request)
    {
        $user = Auth::user();
        return view(
            'user.profile.phone',
            [
                'user' => $user,
            ]
        );
    }

    public function updatePhone(Request $request, Authenticatable $user)
    {
        $validator = Validator::make(
            $request->only(['phone', 'sms_code']),
            [
                'phone' => 'required|phone:RU',
                'sms_code' => 'required|string:'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $token = $user->tokens()->apply(
            PhoneNumber::make($request->get('phone'), 'RU')->formatE164()
        )->first();

        if ($token && Hash::check($request->get('sms_code'), $token->hash)) {
            $user->phone = $request->get('phone');
            $user->save();
            $token->used = true;

            return redirect()->route('home')->with('status', 'Телефон подтвержден');
        } elseif ($token) {
            // стоит ли удалять
            // $token->delete();
        }

        return response()->json(['result' => false, 'message' => 'Не верный код'], 400);
    }

    /**
     * подтверждение почты
     *
     * @param  Request         $request запрос подтверждения почты
     * @param  Authenticatable $user    авторизованный пользователь
     * @return Response                 результат сохранения
    */
    public function confirmedEmail(Request $request, Authenticatable $user): Response
    {
        $user->confirmed = true;
        $user->save();

        return response()->view('home', ['status' => 'Почтовый адрес подтвержден']);
    }
}
