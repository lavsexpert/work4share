<?php

namespace App\Http\Controllers\Web\UserProfile;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;
use App\Models\{BookingDate};
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;

class profileController extends Controller
{
    public function show() {
        $bookingDate = BookingDate::getBookingsDate();
        $paymentBooking = collect();
        foreach ($bookingDate as $key => $value) {
            if ($value->payment !== null) {
                $paymentBooking->push($value->payment);
            }
        }
        return view('user.profile.personal-page', [
            'user' => Auth::user(),
            'user_event_push_type' => DB::select('select * from user_event_push_type'),
            'paymentBooking' => $paymentBooking
        ]);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route( 'profile.index' )->with( [ 'response' => 'Что-то пошло не так' ] );
        }

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Current password does not match']]], 422);
        }

        $user->password = Hash::make($request->password);
        $user->save();


        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Вы успешно изменили свой пароль' ] );
    }
    
    public function saveNotifications(Request $request) {
        
        $user = Auth::user();

        if ($request->email) { $user->email_notif = 1; } else { $user->email_notif = null; }
        if ($request->sms) { $user->sms_notif     = 1; } else { $user->sms_notif   = null; }
        if ($request->app) { $user->app_notif     = 1; } else { $user->app_notif   = null; }

        $user->save();

        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function updatePreferredTypeKPP(Request $request) {
        
        $user = Auth::user();

        if ($request->preferred_type_KPP) { $user->preferred_type_KPP = $request->preferred_type_KPP; }

        $user->save();

        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function saveInfo(Request $request) {
        
        $user = Auth::user();

        // if ($request->phone) {
            // $user_controller = new UserController();
        //     $user_controller->sendSms($request, $user);
        // }

        $user->country = $request->country;
        $user->city = $request->city;
        $user->vk_link = $request->vk_link;
        $user->inst_link = $request->inst_link;
        $user->fb_link = $request->fb_link;

        $user->save();

        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function isActive() {
        
        $user = Auth::user();
        
        if ($user->isActive == null) { 
            $user->isActive = 1;
        } elseif ($user->isActive == 1) {
            $user->isActive = null;
        } 

        $user->save();

        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function updateBirthday(Request $request)
    {
        /* @var $user User */
        $user = Auth::user();
        $user->update([ 'birthday' => $request->birthday ]);
        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function updateExperience(Request $request)
    {
        /* @var $user User */
        $user = Auth::user();
        $user->update([ 'driving_experience' => $request->driving_experience ]);
        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Настройки сохранены' ] );
    }

    public function uploadAvatar(Request $request) {

        if ($request->hasFile('avatar')) {

            $hash = 'profile/avatar-' . Auth::user()->id;
        
            Storage::disk('hot')->put(
                $hash,
                $request->file('avatar')->get(),
                array('ACL'=>'public-read')
            );
            if (Storage::disk('hot')->exists($hash)) {
                $user = Auth::user();
                $user->avatar = env('AWS_URL', 'https://hb.bizmrg.com') . "/" . env('AWS_BUCKET_HOT', 'soautomedia') . "/" .  $hash;
                $user->save();
            }

        }

        return redirect()->route( 'profile.index' )->with( [ 'response' => 'Ваш аватар загружен' ] );
    }
}
