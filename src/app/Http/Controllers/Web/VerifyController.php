<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\{PersonalUser, PaymentBooking};
use App\User;
use Auth;
use Validator;


class VerifyController extends Controller
{
    public function index($id)
    {
        $user = User::find(Auth::id());
        $userPersonal = PersonalUser::find($user['personal_user_id']);
        $paymentBooking = PaymentBooking::find($id); //поиск ордера по id
        return view('verify.index', [
            'user' => $user,
            'userPersonal' => $userPersonal,
            'paymentBooking' => $paymentBooking
        ]);
    }

    public function passport(Request $request)
    {
        $user = User::find(Auth::id());
        $userPersonal = PersonalUser::find($user->personal_user_id);

        $hash_face = md5(uniqid(Auth::id(), true));
        $hash_visa = md5(uniqid(Auth::id(), true));


        $validate = Validator::make(
            $request->all(),
            [
                'passport_face' => 'required|file|mimes:jpeg,bmp,png',
                'passport_visa' => 'required|file|mimes:jpeg,bmp,png',
                // 'passport_seria' => 'required|string',
                // 'passport_nomer' => 'required|string',
                // 'passport_issued_by' => 'required|string',
                // 'passport_issued' => 'required|string',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        Storage::disk('ice')->put(
            $hash_face,
            $request->file('passport_face')->get()
        );
        Storage::disk('ice')->put(
            $hash_face,
            $request->file('passport_visa')->get()
        );


        $userPersonal->update([
            'passport_face' => $hash_face,
            'passport_visa' => $hash_visa,
            // 'passport_seria' => $request->passport_seria,
            // 'passport_nomer' => $request->passport_nomer,
            // 'passport_issued_by' => $request->passport_issued_by,
            // 'passport_issued' => $request->passport_issued,
        ]);


        return response()->json($userPersonal, 200);
    }

    public function driving(Request $request)
    {
        $user = User::find(Auth::id());
        $userPersonal = PersonalUser::find($user->personal_user_id);
        $hash_front = md5(uniqid(Auth::id(), true));
        $hash_back = md5(uniqid(Auth::id(), true));

        $validate = Validator::make(
            $request->all(),
            [
                'driving_front' => 'required|file|mimes:jpeg,bmp,png',
                'driving_back' => 'required|file|mimes:jpeg,bmp,png',
                // 'driver_licence_num' => 'required|string',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $message = Storage::disk('ice')->put(
            $hash_front,
            $request->file('driving_front')->get()
        );
        Storage::disk('ice')->put(
            $hash_back,
            $request->file('driving_back')->get()
        );


        $userPersonal->update([
            'driver_lisense_first' => $hash_front,
            'driver_license_second' => $hash_back,
            // 'driver_licence_num' => $request->driver_licence_num,
        ]);

        return response()->json($userPersonal, 200);
    }
    
    public function selfie(Request $request)
    {

        $validate = Validator::make(
            $request->all(),
            [
                'selfie' => 'required|file|mimes:jpeg,bmp,png',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $user = User::find(Auth::id());
        $userPersonal = PersonalUser::find($user->personal_user_id);
        $hash_selfie = md5(uniqid(Auth::id(), true));
        
        Storage::disk('ice')->put(
            $hash_selfie,
            $request->file('selfie')->get()
        );
 
        $userPersonal->update([
            'selfie' => $hash_selfie,
        ]);
        
        return $userPersonal;
    }

    public function Email(Request $request) {
        $user = User::find(Auth::id());

        if ($user->update([
            'email' => $request->email,
        ])) {
            return 1;
        } else {
            return 0;
        }
    }

    public function Name(Request $request) {
        $user = User::find(Auth::id());

        if ($user->update([
            'name' => $request->name,
        ])) {
            return 1;
        } else {
            return 0;
        }
    }
}
