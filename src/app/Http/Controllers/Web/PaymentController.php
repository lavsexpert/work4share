<?php

namespace App\Http\Controllers\Web;

use App\Mail\NotificationsEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Models\{
    PaymentBooking,
    BookingDate,
    Car
};
use App\User;
use Mail;

class PaymentController extends Controller
{
    private $login = 'soauto';
    private $pass1 = 'g2PJMMdra8oF1X8s7wih';
    

    public function payment()
    {
        $paymentBooking = Session::get('paymentBooking');

        // Настройки платежа
        $out_summ = strval($paymentBooking['total_amount_booking']); //сумма платежа
        $inv_id = strval($paymentBooking['id']); //уникальный id платежа
        $crc = md5("$this->login:$out_summ:$inv_id:$this->pass1"); //генерация подписи для оплаты
        return redirect("https://merchant.roboxchange.com/Index.aspx?MrchLogin=$this->login&SignatureValue=$crc&OutSum=$out_summ&InvId=$inv_id"); //редирект на страницу оплаты с данными
        
    }


    public function success(Request $request)
    {
        $paymentBooking = PaymentBooking::find($request->InvId); //поиск ордера по id


        $out_summ = $request->OutSum; //сумма платежа
        $inv_id = $request->InvId; //уникальный id платежа
        $crc = strtoupper($request->SignatureValue);
        $my_crc = strtoupper(md5("$out_summ:$inv_id:$this->pass1"));


        // проверка корректности подписи
        if ($my_crc != $crc)
        {
            return redirect()->route('fail');
        }

        $paymentBooking->update(['result' => 1]);

        $booking = BookingDate::find($paymentBooking->booking_id);
        $booking->update(['status' => 6]);
        $car = Car::find($booking->car_id);
        $user = User::find($car->user_id);


        Mail::to('oleynikova.leyla22@icloud.com')->cc('victoria.nedelko@soauto.ru')
            ->send(new NotificationsEmail('Новая бронь SoAuto', 'Новая бронь на SoAuto'));


        if ($user && $user->email_notif === 1) {
            $textMessage = '<h3 style="color: #000">'. $car->brand_name . ' ' . $car->model_name .'</h3>
            <p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000"> 
            <br> Забронирован с <br>' . $booking->datefrom . '<br> по <br> ' . $booking->dateto .
            '</p>';
            $buttonText = 'Смотреть подробнее';
            $url = \Config::get('app.url') . '/profile/mycars/' . $car->id;
            $subject = 'Ваш авто забронирован';

            Mail::to($user->email)
                ->send(new NotificationsEmail($subject, $textMessage, $url, $buttonText));
                
        }

        if ($user && $user->sms_notif === 1) {
            $login = 'marussia_f1_team';
            $pass = 'SOAUTO_SMS';
            $endpoint = 'https://smsc.ru/sys/send.php';
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', $endpoint, ['query' => [
                'login' => $login, 
                'psw' => $pass,
                'phones' => $user->phone,
                'mes' => 'Ваш авто забронирован. Для подробностей зайдите в личный кабинет'
            ]]);
        }

        return redirect()->route('verify.index', $paymentBooking['id']);

    }


    public function result()
    {
    }

    
    public function fail(Request $request)
    {
        PaymentBooking::find($request->InvId)->update(['result' => 0]);
        return redirect()->action('Web\UserProfile\MyTripsController@index')->with('result', '0');
    }
}
