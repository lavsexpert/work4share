<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RentCarController extends Controller
{
    public function bookingCar() {
        $user_id = $_GET['user_id'];
        $car_id = $_GET['car_id'];
        $from = $_GET['from'];
        $to = $_GET['to'];

        $query = DB::table('cars')
            ->where('cars.id', $car_id)
            ->select('cars.datefrom', 'cars.dateto')
            ->get();

        if ($query[0]->datefrom < $from) {
            if ($query[0]->dateto > $to) {

            } else {
                return response()->json([
                    'error' => 'start_booking',
                    'message' => 'Дата окончания аренды больше возможной даты'
                ]);
            }
        } else {
            return response()->json([
                'error' => 'start_booking',
                'message' => 'Дата начала аренды меньше возможной даты'
            ]);
        }
        
    }

    public function rentCarUser () {

    }

    public function validateRentCarOwner() {

    }
}
