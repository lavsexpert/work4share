<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{RentPlace, City, Car, CarImage};

class HomePageController extends Controller
{
    public function index()
    {
      $cars = Car::find([113, 88, 141, 140, 127]);
      $images = Carimage::all();
      return view('home', [
        'rentPlace' => RentPlace::all(),
        'city' => City::orderBy('name', 'asc')->get(),
        'cars' => $cars,
        'images' => $images
        ]
      );
    }
}


