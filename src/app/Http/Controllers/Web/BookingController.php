<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class BookingController extends Controller
{
    public function BookingPage() {
        $carId = $_GET['carId'];
        $from = $_GET['from'];
        $to = $_GET['to'];

        $query = DB::table('cars')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->where('cars.id', $carId)
            ->select('cars.id', 'cars.available', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear');
           
            $selectionCars = $query
                ->get();

        $images = DB::table('cars_images')
            ->select('cars_images.name as img_url')
            ->where('cars_images.car_id', $carId)
            ->get();

            $from = strtotime($from);
            $to = strtotime($to);

            $days = $to - $from;

            $days = $days / 86400;

            if ($days > 3) {
                $delivery = 0;
            } else {
                $delivery = $selectionCars[0]->cost_day / 2;
            }

            $fullCostDays = $days * $selectionCars[0]->cost_day;
            $fullCost = $fullCostDays + $delivery;


	



        return view('cars.booking.booking', [
                                        'selectionCars' => $selectionCars, 
                                        'images' => $images, 
                                        'bookingDetals' => array(
                                            'days' => $days, 
                                            'from' => $from, 
                                            'to' => $to, 
                                            'delivery' => $delivery, 
                                            'fullCostDays' => $fullCostDays, 
                                            'fullCost' => $fullCost
                                            )]);


    }

    public function BookingSuccess () {
        
        $id = Auth::user()->id;

        $from = date_create($_GET['from']);
        $datefrom = date_format($from, 'Y-m-d');

        $to = date_create($_GET['to']);
        $dateto = date_format($to, 'Y-m-d');

        DB::table('booking_date')->insert(
            [
                'car_id' => $_GET['car_id'],
                'user_id' => $id,
                'datefrom' => $datefrom,
                'dateto' => $dateto,
                'status' => 1,

            ]
        );

        DB::table('cars')
            ->where('id', $_GET['car_id'])
            ->update(['available' => '2']
        );

        return view('cars.booking.booking-success');

    }
}
