<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\{PersonalUser, UtmData};
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Mail;
use Auth;
use App\Mail\NotificationsEmail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected function redirectTo()
    {
        $car_id = Cookie::get('car_id');
        if (Cookie::get('checkout')) {
            return "/checkout/$car_id";
        }
        return '/';
    }



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $personal_user = PersonalUser::create();
        $newUser = new User([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'personal_user_id' => $personal_user->id,
            'app_notif' => 1,
            'email_notif' => 1,
            'sms_notif' => 1,
        ]);
        $newUser->save();

        /*Проверяем есть ли в сессии сохраненные utm метки,
        если есть создаем новый UtmData
        и прикрепляем к созданному пользователю*/

        $utm = session('utm');
        if ($utm) {
            $userUtmData = new UtmData([
                'user_id' => $newUser->id,
                'utm_source' => $utm['source'],
                'utm_medium' => $utm['medium'],
                'utm_campaign' => $utm['campaign'],
            ]);
            $userUtmData->save();
        }
        return $newUser;
    }
    protected function quick(Request $request)
    {
        if (User::where('email', $request->email)->first()) {
            return 0;
        }

        $car_id = Cookie::get('car_id');
        Cookie::queue('checkout', true, 10);
        $passwd = static::createPasswd();
        $personal_user = PersonalUser::create();
        $newUser = new User([
            'name' => static::createName(),
            'email' => $request->email,
            'password' => Hash::make($passwd),
            'personal_user_id' => $personal_user->id,
            'app_notif' => 1,
            'email_notif' => 1,
            'sms_notif' => 1,
        ]);
        $newUser->save();



        $textMessage = '<h3 style="color: #000">Логин: ' . $request->email .'</h3><br><h3 style="color: #000">Пароль: ' . $passwd .'</h3>';
        $subject = 'Успешная регистрация';
        Mail::to($newUser->email)
            ->send(new NotificationsEmail($subject, $textMessage));

        /*Проверяем есть ли в сессии сохраненные utm метки,
        если есть создаем новый UtmData
        и прикрепляем к созданному пользователю*/

        $utm = session('utm');
        if ($utm) {
            $userUtmData = new UtmData([
                'user_id' => $newUser->id,
                'utm_source' => $utm['source'],
                'utm_medium' => $utm['medium'],
                'utm_campaign' => $utm['campaign'],
            ]);
            $userUtmData->save();
        }

        Auth::loginUsingId($newUser->id);
        return redirect("/checkout/$car_id");
    }

    protected static function createName (): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randstring;
    }


    protected static function createPasswd (): string
    {
        $characters = '!@#$%^&*()_+=-*/0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randstring;
    }

}
