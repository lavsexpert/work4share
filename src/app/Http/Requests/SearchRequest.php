<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class SearchRequest extends FormRequest
{
    protected $redirect = '/carslist';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brandid' => 'integer',
            'modelid' => 'integer',
            'air' => 'sometimes',
            'bluetooth' => 'sometimes',
            'audio_input' => 'sometimes',
            'usb_charger' => 'sometimes',
            'child_seat' => 'sometimes',
            'cruise_control' => 'sometimes',
            'gps' => 'sometimes',
            'seat' => 'integer',
            'from_cost_day' => 'integer|nullable',
            'to_cost_day' => 'integer|nullable',
            'from' => 'sometimes|nullable|date',
            'to' => 'sometimes|nullable|date'
        ];
    }
    public function messages()
    {
        return [
            'brandid.integer' => ':attribute должен быть числом',
            'modelid.integer' => ':attribute должен быть числом'
        ];
    }

    public function attributes()
    {
        return [
            'brandid' => 'Бренд',
            'modelid' => 'Модель'
        ];
    }
}
