<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'nullable|date',
            'to' => 'nullable|date',
            'minPrice' => 'nullable|integer',
            'maxPrice' => 'nullable|integer',
            'minPlaces' => 'nullable|integer',
            'selectedOptions' => 'nullable|array',
            'selectedExtraOptions' => 'nullable|array',
            'brand_id' => 'nullable|integer',
            'model_id' => 'nullable|integer',
        ];
    }
}
