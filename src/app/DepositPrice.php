<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\DepositPrice
 *
 * @property int $id
 * @property int $car_price
 * @property int $deposit_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice whereCarPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice whereDepositPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DepositPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DepositPrice extends Model
{
    protected $guarded = [];
}
