<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->text('description')->nullable();
                $table->string('preferred_type_KPP')->nullable();
                $table->string('country')->nullable();
                $table->string('city')->nullable();
                $table->string('vk_link')->nullable();
                $table->string('fb_link')->nullable();
                $table->string('inst_link')->nullable();
                $table->boolean('isActive')->nullable();
            });
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
