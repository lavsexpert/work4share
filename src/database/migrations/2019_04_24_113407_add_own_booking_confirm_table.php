<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnBookingConfirmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('booking_date')) {
            Schema::table('booking_date', function ($table) {
                $table->boolean('own_booking_confirm')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_date', function (Blueprint $table) {
            $table->dropColumn('own_booking_confirm');
        });
    }
}
