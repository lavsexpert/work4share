<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCars extends Migration
{
    /**
     * Добавляем внешние ключи на модель и бренд по id
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'brand_id') && Schema::hasColumn('brands_cars', 'id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('brand_id')->references('id')->on('brands_cars');
                });
            }
        }

        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'model_id') && Schema::hasColumn('models_cars', 'id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('model_id')->references('id')->on('models_cars');
                });
            }
        }

        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'gear_id') && Schema::hasColumn('cars_gears', 'id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('gear_id')->unsigned()->nullable();
                });
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('gear_id')->references('id')->on('cars_gears');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'model_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropForeign(['model_id']);
                });
            }
        }

        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'brand_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropForeign(['brand_id']);
                });
            }
        }

        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'gear_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropForeign(['gear_id']);
                    $table->dropColumn('gear_id');
                });
            }
        }
    }
}
