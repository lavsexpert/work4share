<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCars extends Migration
{
    /**
     * Добавляем поле владельца авто
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'user_id') && Schema::hasColumn('users', 'id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('user_id')->unsigned()->nullable();
                });
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('user_id')->references('id')->on('users');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
