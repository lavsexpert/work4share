<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDoorsCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'doors')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->unsignedTinyInteger('doors')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'doors')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropColumn('doors');
                });
            }
        }
    }
}
