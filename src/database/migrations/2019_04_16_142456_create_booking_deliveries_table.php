<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->integer('delivery_cars_id')->unsigned()->nullable();
            $table->integer('count')->nullable();
            $table->integer('cost')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('booking_deliveries', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('booking_date');
            $table->foreign('delivery_cars_id')->references('id')->on('delivery_cars');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_deliveries');
    }
}
