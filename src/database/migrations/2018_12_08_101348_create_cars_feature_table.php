<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsFeatureTable extends Migration
{
    /**
     * Добавляем таблицу дополнительных свойств авто
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cars_feature')) {
            Schema::create('cars_feature', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('air')->default(false);
                $table->boolean('bluetooth')->default(false);
                $table->boolean('audio_input')->default(false);
                $table->boolean('usb_charger')->default(false);
                $table->boolean('child_seat')->default(false);
                $table->boolean('cruise_control')->default(false);
                $table->boolean('gps')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_feature');
    }
}
