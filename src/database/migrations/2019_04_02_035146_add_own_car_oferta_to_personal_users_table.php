<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddOwnCarOfertaToPersonalUsersTable
 *
 * 1. В сущность personal_users добавить атрибуты:
 * ownCarOferta (bool)
 */
class AddOwnCarOfertaToPersonalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_users', function (Blueprint $table) {
            $table->boolean('own_car_oferta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('personal_users') && Schema::hasColumn('personal_users', 'own_car_oferta')) {
            Schema::table('personal_users', function (Blueprint $table) {
                $table->dropColumn(['own_car_oferta']);
            });
        }
    }
}
