<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUnlockTypeIdCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (!Schema::hasColumn('cars', 'unlock_type_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->tinyInteger('unlock_type_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'unlock_type_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropColumn('unlock_type_id');
                });
            }
        }
    }
}
