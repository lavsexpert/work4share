<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('brand_id')->unsigned();
            $table->Integer('model_id')->unsigned();
            $table->Integer('year');
            $table->Integer('pts_image_id');
            $table->Integer('sts_image_id');
            $table->Integer('cost_day');
            $table->Integer('cost_hour');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
