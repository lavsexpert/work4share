<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->integer('extra_id')->unsigned()->nullable();
            $table->integer('count')->nullable();
            $table->integer('amount')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('payment_extras', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('booking_date');
            $table->foreign('extra_id')->references('id')->on('extra_cars');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_extras');
    }
}
