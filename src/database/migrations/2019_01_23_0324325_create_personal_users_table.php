<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('personal_users')) {
            Schema::create('personal_users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('passport_face')->nullable();
                $table->string('passport_visa')->nullable();
                $table->string('driver_lisense_first')->nullable();
                $table->string('driver_license_second')->nullable();
                $table->string('selfie')->nullable();
                $table->timestamps();
            });
        }

        if (!Schema::hasColumn('users', 'personal_user_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->unsignedInteger('personal_user_id')->nullable();
            });

            Schema::disableForeignKeyConstraints();
            Schema::table('users', function (Blueprint $table) {
                $table->foreign('personal_user_id')->references('id')->on('personal_users');
            });
            Schema::enableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'personal_user_id')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropForeign(['personal_user_id']);
                    $table->dropColumn('personal_user_id');
                });
            }
        }

        Schema::dropIfExists('personal_users');
    }
}
