<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsImagesTable extends Migration
{
    /**
     * Добавляем таблицу фото автомобилей
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cars_images')) {
            Schema::create('cars_images', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('car_id')->unsigned();
                $table->string('name');
                $table->timestamps();
            });
        }

        if (Schema::hasTable('cars') && Schema::hasColumn('cars', 'id')) {
            Schema::table('cars_images', function (Blueprint $table) {
                $table->foreign('car_id')->references('id')->on('cars');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars_images') && Schema::hasColumn('cars_images', 'car_id')) {
            Schema::table('cars_images', function (Blueprint $table) {
                $table->dropForeign(['car_id']);
            });
        }

        Schema::dropIfExists('cars_images');
    }
}
