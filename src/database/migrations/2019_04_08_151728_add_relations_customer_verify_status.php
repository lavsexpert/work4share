<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationsCustomerVerifyStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('customer_verify_status')) {
            Schema::table('customer_verify_status', function (Blueprint $table) {
                $table->integer('personal_user_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_verify_status', function (Blueprint $table) {
            $table->dropColumn('personal_user_id');
        });
    }
}
