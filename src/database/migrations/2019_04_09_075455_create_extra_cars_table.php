<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned()->nullable();
            $table->integer('extra_id')->unsigned()->nullable();
            $table->integer('extra_time_id')->unsigned()->nullable();
            $table->integer('extra_cost')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('extra_cars', function (Blueprint $table) {
            $table->foreign('cars_id')->references('id')->on('cars');
            $table->foreign('extra_id')->references('id')->on('extra');
            $table->foreign('extra_time_id')->references('id')->on('extra_time');
            
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('extra_cars');
    }
}
