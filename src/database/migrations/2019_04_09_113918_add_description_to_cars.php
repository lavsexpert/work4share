<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('description_id')->unsigned()->nullable();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('cars', function (Blueprint $table) {
            $table->foreign('description_id')->references('id')->on('cars_descriptions');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['description_id']);
            $table->dropColumn('description_id');
        });
    }
}
