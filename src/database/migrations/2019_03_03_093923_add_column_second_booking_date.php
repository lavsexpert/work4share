<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSecondBookingDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('booking_date')) {
            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('status')->nullable();
            });

            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('action')->nullable();
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
