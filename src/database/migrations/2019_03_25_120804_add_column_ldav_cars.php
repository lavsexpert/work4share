<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLdavCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('like')->nullable();
            $table->integer('dislike')->nullable();
            $table->integer('available')->nullable();
            $table->integer('validate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('like');
            $table->dropColumn('dislike');
            $table->dropColumn('available');
            $table->dropColumn('validate');
        });
    }
}
