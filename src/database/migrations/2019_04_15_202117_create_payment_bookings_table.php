<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->integer('amount_booking')->nullable();
            $table->integer('total_amount_booking')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('payment_bookings', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('booking_date');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_bookings');
    }
}
