<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryPlaceToBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_date', function (Blueprint $table) {
            $table->unsignedInteger('delivery_place')->nullable();
            $table->foreign('delivery_place')->references('id')->on('rent_place');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_date', function (Blueprint $table) {
            //
        });
    }
}
