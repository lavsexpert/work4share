<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingFielsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_fiels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned()->nullable();
            $table->string('fiel_photo_before')->nullable();
            $table->string('fiel_photo_after')->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('booking_fiels', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('booking_date');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_fiels');
    }
}
