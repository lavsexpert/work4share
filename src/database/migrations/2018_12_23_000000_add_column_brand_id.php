<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBrandId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('brands_cars') && Schema::hasTable('models_cars')) {
            if (!Schema::hasColumn('models_cars', 'brand_id')) {
                Schema::table('models_cars', function (Blueprint $table) {
                    $table->integer('brand_id')->unsigned()->nullable();
                });
            }

            if (Schema::hasTable('models_cars') && Schema::hasColumn('brands_cars', 'id')) {
                Schema::table('models_cars', function (Blueprint $table) {
                    $table->foreign('brand_id')->references('id')->on('brands_cars');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('models_cars')) {
            if (Schema::hasColumn('models_cars', 'brand_id')) {
                Schema::table('models_cars', function (Blueprint $table) {
                    $table->dropForeign(['brand_id']);
                    $table->dropColumn('brand_id');
                });
            }
        }
    }
}
