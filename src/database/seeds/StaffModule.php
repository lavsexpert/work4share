<?php

use Illuminate\Database\Seeder;

class StaffModule extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staff_module')->insert([
            'name' => 'Админ',
            'route' => '/staff/admin',
        ]);

        DB::table('staff_module')->insert([
            'name' => 'Проверка авто',
            'route' => '/staff/checkauto',
        ]);

        DB::table('staff_module')->insert([
            'name' => 'Проверка арендатора',
            'route' => '/staff/checkuser',
        ]);

        DB::table('staff_module')->insert([
            'name' => 'Права',
            'route' => '/staff/role',
        ]);

        DB::table('staff_module')->insert([
            'name' => 'Брони',
            'route' => '/staff/bookings',
        ]);

        DB::table('staff_module')->insert([
            'name' => 'Проверенные авто',
            'route' => '/staff/validated-auto',
        ]);


        DB::table('staff_module')->insert([
            'name' => 'Размеры депозитов',
            'route' => '/staff/deposit-amounts',
        ]);


    }
}
