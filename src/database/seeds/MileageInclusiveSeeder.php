<?php

use Illuminate\Database\Seeder;
use App\Models\MileageInclusive;

class MileageInclusiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 100; $i <= 5000; $i+=50) {
            MileageInclusive::insert([
                'name' => $i,
            ]);
        }
    }
}
