<?php

use Illuminate\Database\Seeder;
use App\Models\CarBans;
class CarBansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CarBans::insert([
            'name' => 'Курить',
        ]);

        CarBans::insert([
            'name' => 'С животными',
        ]);
    }
}
