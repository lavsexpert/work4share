<?php

use Illuminate\Database\Seeder;
use App\Models\ModelsCar;

class ModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'aaa'],
            ['name' => 'bbb'],
        ];

        foreach ($items as $item) {
            ModelsCar::insert([
                'name' => $item['name']
            ]);
        }
    }
}
