<?php

use Illuminate\Database\Seeder;

class AddLatLongToCar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Car::all() as $car) {
            $car->lat = (55700 + rand(11, 99) + rand(1, 9)) / 1000;
            $car->lng = (37600 + rand(11, 99) + rand(1, 9)) / 1000;
            $car->save();
        }
    }
}
