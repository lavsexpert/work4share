<?php

use Illuminate\Database\Seeder;

class AddTransmissionIdToCarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Car::all() as $car) {
            $car->transmission_id = rand(1, 4);
            $car->save();
        }
    }
}
