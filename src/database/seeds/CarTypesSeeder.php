<?php

use Illuminate\Database\Seeder;
use App\Models\CarType;

class CarTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Седан'],
            ['name' => 'Хетчбек'],
            ['name' => 'Внедорожник'],
            ['name' => 'Универсал'],
            ['name' => 'Купе'],
            ['name' => 'Минивен'],
            ['name' => 'Пикап'],
            ['name' => 'Фургон'],
            ['name' => 'Кабриолет']
        ];

        foreach ($items as $item) {
            CarType::insert([
                'name' => $item['name']
            ]);
        }
    }
}
