<?php

use Illuminate\Database\Seeder;
use App\Models\CarsColor;
class CarsColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Красный'],
        ];

        foreach ($items as $item) {
            CarsColor::insert([
                'name' => $item['name']
            ]);
        }
    }
}
