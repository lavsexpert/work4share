<?php

use Illuminate\Database\Seeder;

class ReRunPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \App\ReRunPrice::insert([
            'car_price' => 500000,
            'coef' => 0.00001 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 1000000,
            'coef' => 0.000005 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 1500000,
            'coef' => 0.000004 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 2000000,
            'coef' => 0.000003 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 2500000,
            'coef' => 0.0000025 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 3000000,
            'coef' => 0.0000023 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 3500000,
            'coef' => 0.0000023 ,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 4000000 ,
            'coef' => 0.000002,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 4500000 ,
            'coef' => 0.000002,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 5000000,
            'coef' => 0.0000018,
        ]);

        \App\ReRunPrice::insert([
            'car_price' => 1000000000,
            'coef' => 0.0000018,
        ]);
    }
}
