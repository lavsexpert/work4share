<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarsRefusalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['name' => 'Паспорт подтвержден'],
            ['name' => 'Паспорт не читаем'],
            ['name' => 'Паспорт не действителен'],
            ['name' => 'СТС подтверждено'],
            ['name' => 'СТС не читаемо'],
            ['name' => 'СТС не действительно'],
            ['name' => 'СТС отклонено, не владелец'],
            ['name' => 'СТС отклонено, в розыске'],
            ['name' => 'ПТС подтвержден'],
            ['name' => 'ПТС не читаем'],
            ['name' => 'ОСАГО подтверждено'],
            ['name' => 'ОСАГО не читаемо'],
            ['name' => 'ОСАГО не действительно'],
            ['name' => 'ОСАГО отклонено, необходимо расширенное'],
            ['name' => 'Автомобиль подтверждён'],
            ['name' => 'Автомобиль не подтвержден']
        ];

        foreach ($items as $item) {
            DB::table('cars_validate_refusal')->insert($item);
        }
    }
}
