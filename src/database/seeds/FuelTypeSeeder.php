<?php

use Illuminate\Database\Seeder;

class FuelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fuel_types')->insert([
            'name' => 'Бензин',
            'slug' => 'benzin'
        ]);

        DB::table('fuel_types')->insert([
            'name' => 'Дизель',
            'slug' => 'dizel'
        ]);

        DB::table('fuel_types')->insert([
            'name' => 'Газ',
            'slug' => 'gas'
        ]);

        DB::table('fuel_types')->insert([
            'name' => 'Другое',
            'slug' => 'else'
        ]);
    }
}
